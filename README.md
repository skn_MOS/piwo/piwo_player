# P.I.W.O. project

## Introduction
The P.I.W.O project is the open-source player for light shows performed of different kinds of buildings.
Historically the project used to make the shows on student dormitories, but there are no limits.
To make the show you need some extra components, but more about it in the project wiki.

## Our CI
You can find piwo\_player binaries for both: linux and windows on our CI build pipelines.
- The build-linux stage contains binary for linux called **Player**.
- The build-win stage contains binary for windows called **Player.exe**.


## Dependencies
To build the project, you need to download all dependecies by yourself.
The required dependecies are:
 - [fmt library](https://github.com/fmtlib/fmt)
 - [RapidJSON library](https://github.com/Tencent/rapidjson)
 - [spdlog library](https://github.com/gabime/spdlog)
 - [mipc library](https://gitlab.com/skn_MOS/piwo/libraries/mipc)
 - [piwo library](https://gitlab.com/skn_MOS/piwo/libraries/libpiwo)
 - [RtMidi library](https://github.com/thestk/rtmidi)
 - [BASS library](https://www.un4seen.com)

You can check scripts directory to see how to build them for Windows and Linux.

## Build
### Windows
Probably the simplest way of building the project is to open it with qt-creator.
Mind you, that this hasn't been tested. We also have managed to build the whole
project using MSYS, natively on Windows, but this is not supported and user has
to prepare the environment and dependencies him/herself.

### Linux
* Install necessary packages
```bash
apt install qt5-default libqt5opengl5-dev qml-module-qtquick-extras \
            qml-module-qtquick-controls2 qml-module-qtquick-window2 \
	    qml-module-qt-labs-qmlmodels qtbase5-dev
```

## Dev notes
### Running project with clang-analyzer
Project has to be configured with scan-build like (user can also pass other usual defines/options):
```sh
scan-build cmake ..
```

### Running clang-tidy checkers over project
Project has to provide `RUN_CLANG_TIDY=ON` option. So the configuration line would be:
```sh
cmake -DRUN_CLANG_TIDY=ON ..
```

## Deployment [Windows only]
When you want to run the cross-compiled Player for windows you need to deliver additional dependencies:
 - Dynamic libraries
 - Qt Plugins
 - Qml Imports  

The easiest thing is to look for them inside our docker image.  
For dynamic libraries you can use depwalk.py which will try to copy deps to current directory.  
Qt Plugins and Qml imports can be found inside the docker image or you can use the [ones](https://drive.google.com/file/d/1dzIqOZqmNni33p1YP6rdx3KqrZXYK34K/view?usp=sharing) that were prepared
for the commit 4e531b48841d9bd9a43eeeeb5290aa1713f244ec.
The few enviromental variables should be exported before we can run the Player on windows:
  - QT\_PLUGIN\_PATH- absolute path to Qt Plugins (C:\\Uesers\\...\\player\_plugins)
  - QT\_QPA\_PLATFORM\_PLUGIN\_PATH - absolute path to Qt Plugin platforms (C:\\Uesers\\...\\player\_plugins\\platforms)
  - QML\_IMPORT\_PATH - absolute path to Qt Qml imports (C:\\Uesers\\...\\player\_qml\\qml)
  - QML2\_IMPORT\_PATH - absolute path to Qt Qml imports (C:\\Uesers\\...\\player\_qml\\qml)

## Additional Links
[Launchpad protocol](https://docs.google.com/document/d/1KvSllQo9GsYYoJ09QXKexN2dBp_79TAcYuCTSapX41o)
[UDP protocol](https://docs.google.com/document/d/1Id8atthziZ64V6xowBAZt2FVeHfraQnJGya-9A8t3F8)
[TCP protocol](https://docs.google.com/document/d/1qdNjDpIlg-bFNPkFcjgMsGNjFXdrhj7mQ_jBvjID3Zw)
[PIWO protocol](https://docs.google.com/document/d/1Id8atthziZ64V6xowBAZt2FVeHfraQnJGya-9A8t3F8)

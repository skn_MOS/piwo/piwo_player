#include "event_loop.hpp"
#include "config.hpp"
#include "frame_provider.hpp"

#include <piwo/common_intersection_operators.h>
#include <piwo/frames_intersection.h>

template<class... Ts>
struct overloaded : Ts...
{
  using Ts::operator()...;
};
template<class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

void
event_loop::send_event(event_variant event)
{
  this->_events.locked_scope([&](queue_t& ev) { ev.push(event); });

  this->_cv.notify_all();
}

event_loop::event_loop()
{
  this->_th = std::thread(&event_loop::process_events, std::ref(*this));
}

event_loop::~event_loop()
{
  this->_events.locked_scope(
    [&](queue_t& ev)
    {
      ev = queue_t();
      ev.emplace(finish_loop());
    });

  this->_cv.notify_all();

  if (this->_th.joinable())
    this->_th.join();
}

event_loop::event_variant
event_loop::pop_event()
{
  auto [lock, events] = this->_events.get_guarded();

  this->_cv.wait(lock, [&] { return events.size() > 0; });

  event_variant ev = events.front();
  events.pop();
  return ev;
}

void
event_loop::process_events()
{
  bool is_alive = true;
  while (is_alive)
  {
    event_variant ev = this->pop_event();
    std::visit(
      overloaded{
        [this](resize_data& data)
        {
          auto [width, height] = data;
          handle_resize(width, height);
        },
        [this](eth_conn_data& data)
        {
          auto [ip, tcp_port, udp_port] = data;
          handle_tx_connect(ip, tcp_port, udp_port);
        },
        [this](eth_disconn_data&) { handle_tx_disconnect(); },
        [&is_alive](finish_loop&) { is_alive = false; },
      },
      ev);
  }
}

void
event_loop::handle_resize(size_t width, size_t height)
{
  // TODO take application state mutex
  auto [locks, providers] = global_config.acquire_providers();

  std::apply([&](auto&&... provider)
             { ((provider.resize(width, height)), ...); },
             providers);
}

void
event_loop::handle_tx_connect(const std::string& ip,
                              const std::string& tcp_port,
                              const std::string& udp_port)
{
  global_config.tx_connect(ip, tcp_port, udp_port);
}

void
event_loop::handle_tx_disconnect()
{
  global_config.tx_disconnect();
}

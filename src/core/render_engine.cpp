#include "render_engine.hpp"

#include "ui_iface.hpp"
#include <spdlog/spdlog.h>

using state_t = render_engine::state_t;

render_engine::render_engine(size_t rows, size_t cols)
  : frame_provider(true, provider_id::RENDER)
{
  auto [lock, frame] = this->_frame.get_guarded();

  frame = piwo::alloc_frame(rows, cols);
}

render_engine::~render_engine()
{
  this->stop();
}

void
render_engine::stop()
{
  this->_tim.stop();
  if (this->_th.joinable())
    this->_th.join();
}
void
render_engine::start()
{
  this->start(this->_sec, this->_mili);
}
void
render_engine::start(const std::chrono::seconds& sec,
                     const std::chrono::milliseconds& mili)
{
  this->_ctx.locked_scope(
    [&](render_ctx& ctx)
    {
      if (ctx._render_active)
        return;

      ctx._render_active = true;
    });

  this->_th = std::thread(&render_engine::render, this);
  this->_sec = sec;
  this->_mili = mili;
  this->_tim.start(sec, mili);
}

state_t
render_engine::blocking_add(const render_engine_step& config)
{
  this->stop();
  return this->add_step(config);
}

bool
render_engine::is_active()
{
  return this->_tim.is_active();
}

state_t
render_engine::blocking_refresh(step_id id)
{
  this->stop();
  return this->refresh_step(id);
}

state_t
render_engine::blocking_remove(step_id id)
{
  this->stop();
  return this->remove_step(id);
}

void
render_engine::serialize(serialization_writer& writer)
{
  std::optional lck = this->_ctx.try_lock();

  if (!lck.has_value())
  {
    spdlog::warn("Render engine serialization failed. Render engine busy");
    return;
  }

  auto& [lock, ctx] = lck.value();

  writer.StartObject();
  writer.Key("steps");
  writer.StartArray();
  for (auto s : ctx._steps)
  {
    ::serialize(s, writer);
  }
  writer.EndArray();
  writer.EndObject();
}

state_t
render_engine::add_step(const render_engine_step& config)
{
  std::optional lck = this->_ctx.try_lock();
  if (!lck.has_value())
  {
    spdlog::warn("Adding step failed. Render engine busy");
    return state_t::re_busy;
  }

  auto& [lock, ctx] = lck.value();

  if (ctx._render_active == true)
  {
    spdlog::warn("Adding step failed. Render engine busy");
    return state_t::re_busy;
  }

  ctx._steps.push_back(config);
  return state_t::re_ok;
}

state_t
render_engine::remove_step(step_id id)
{
  std::optional lck = this->_ctx.try_lock();
  if (!lck.has_value())
  {
    spdlog::warn("Step removal failed. Render engine busy");
    return state_t::re_busy;
  }

  auto& [lock, ctx] = lck.value();

  if (ctx._render_active == true)
  {
    spdlog::warn("Step removal failed. Render engine busy");
    return state_t::re_busy;
  }

  auto step = std::find(ctx._steps.begin(), ctx._steps.end(), id);
  if (step == ctx._steps.end())
  {
    spdlog::warn(
      "Step removal failed. Configuration with specified id does not exist");
    return state_t::re_failed;
  }

  ctx._steps.erase(step);
  render_engine_step_removed(id);
  return state_t::re_ok;
}

void
render_engine::clear_steps()
{
  std::optional lck = this->_ctx.try_lock();
  if (!lck.has_value())
  {
    spdlog::warn("Clearing steps failed. Render engine busy");
    return;
  }

  auto& [lock, ctx] = lck.value();

  if (ctx._render_active == true)
  {
    spdlog::warn("Clearing steps failed. Render engine busy");
    return;
  }

  while (ctx._steps.size())
  {
    step_id id = ctx._steps.back().id;
    auto removed = this->remove_step(id);
    if (removed != state_t::re_ok)
    {
      spdlog::warn("Step removing failed");
      continue;
    }
  }
}

state_t
render_engine::refresh_step(step_id id)
{
  std::optional lck = this->_ctx.try_lock();

  if (!lck.has_value())
  {
    spdlog::warn("Refreshing step failed. Render engine busy");
    return state_t::re_busy;
  }

  auto& [lock, ctx] = lck.value();

  if (ctx._render_active == true)
  {
    spdlog::warn("Refreshing step failed. Render engine busy");
    return state_t::re_busy;
  }

  auto step = std::find(ctx._steps.begin(), ctx._steps.end(), id);
  if (step == ctx._steps.end())
  {
    spdlog::warn(
      "Step removal failed. Configuration with specified id does not exist");
    return state_t::re_failed;
  }

  step->refresh();

  return state_t::re_ok;
}

state_t
render_engine::set_step_state(step_id id, bool state)
{
  std::optional lck = this->_ctx.try_lock();

  if (!lck.has_value())
  {
    spdlog::warn("Setting step state failed. Render engine busy");
    return state_t::re_busy;
  }

  auto& [lock, ctx] = lck.value();

  auto step = std::find(ctx._steps.begin(), ctx._steps.end(), id);
  if (step == ctx._steps.end())
  {
    spdlog::warn(
      "Step removal failed. Configuration with specified id does not exist");
    return state_t::re_failed;
  }

  step->active = state;

  return state_t::re_ok;
}

void
render_engine::render()
{
  while (true)
  {
    if (!this->_tim.wait())
    {
      this->_ctx.locked_scope(
        [&](render_ctx& ctx)
        {
          ctx._render_active = false;
          this->_cv.notify_all();
        });

      return;
    }

    this->_ctx.locked_scope(
      [&](render_ctx& ctx)
      {
        for (const auto& step : ctx._steps)
        {
          if (!step.active)
            continue;
          std::invoke(step.effect, step._intersection);
        }

        ctx._rendered = true;
        this->_cv.notify_all();
      });
  }
}

piwo::frames_intersection::frame_t
render_engine::get_frame_()
{
  auto [lock, frame] = this->_frame.get_guarded();
  return frame;
}

void
render_engine::resize_(size_t width, size_t height)
{
  spdlog::info("Changing frame resolution of\
      render engine to width->{} height->{}",
               width,
               height);

  auto [lock, frame] = this->_frame.get_guarded();

  frame = piwo::alloc_frame_shared(width, height);
}

void
render_engine::wait()
{
  auto [lock, ctx] = this->_ctx.get_guarded();

  this->_cv.wait(lock, [&] { return ctx._rendered || !ctx._render_active; });
  ctx._rendered = false;
}

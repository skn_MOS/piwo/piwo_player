#include "network.hpp"

#include <boost/asio.hpp>
#include <boost/asio/ip/address.hpp>
#include <fmt/core.h>

#include <piwo/proto.h>
#include <piwo/protodef.h>
#include <string>

#include "config.hpp"

template<typename T>
static void
log_bytes(const T& arr, spdlog::level::level_enum level)
{
  constexpr size_t line_width = 16;

  spdlog::log(level, "Dump of {} bytes", arr.size());

  size_t i = 0;

  for (;;)
  {
    const auto beg = i;
    const auto end = std::min(i + line_width, arr.size());

    spdlog::log(
      level,
      "{:04X}: {:02X}",
      i,
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      fmt::join(std::begin(arr) + beg, std::begin(arr) + end, " "));

    i += line_width;

    if (end == arr.size())
      break;
  }
}

void
tx_eth_connection::start_io_workers()
{
  spdlog::debug("Starting eth IO workers");

  this->_udp_io_context_runner.resume_and_wait();
  this->_tcp_io_context_runner.resume_and_wait();
}

tx_eth_connection::~tx_eth_connection()
{
  try
  {
    this->close();
  }
  catch (...)
  {
    fmt::print("An expection occured in tx_eth_connection destructor");
  }
}

bool
tx_eth_connection::open(const char* host,
                        const char* tcp_port,
                        const char* udp_port)
{
  using boost::asio::ip::tcp;
  using boost::asio::ip::udp;

  bool ret = false;

  try
  {
    tcp::resolver tcp_resolver(this->_tcp_io_context);
    boost::asio::connect(this->_tcp_socket,
                         tcp_resolver.resolve(host, tcp_port));
    ret = this->_tcp_socket.is_open();
    if (ret)
    {
      udp::resolver udp_resolver(this->_udp_io_context);

      auto [lock, udp_sock] = this->_udp_socket.get_guarded();

      this->_udp_endpoint =
        *udp_resolver.resolve(udp::v4(), host, udp_port).begin();

      udp_sock.open(boost::asio::ip::udp::v4());
    }
  }
  catch (const boost::system::system_error& ex)
  {
    spdlog::error("Connecting to TX failed: \"{}\"", ex.what());
    ret = false;
  }
  catch (...)
  {
    spdlog::error("Connecting to TX failed with unexpected error.");
    ret = false;
  }

  if (ret)
    this->start_io_workers();

  return ret;
}

void
tx_eth_connection::close()
{
  spdlog::debug("Stopping eth IO workers");

  this->_udp_io_context_runner.stop();
  this->_tcp_io_context_runner.stop();

  this->_udp_io_context.stop();
  this->_tcp_io_context.stop();

  // make sure that runners are stopped
  this->_udp_io_context_runner.stop_and_wait();
  this->_tcp_io_context_runner.stop_and_wait();

  this->_tcp_socket.close();

  auto [lock, udp_sock] = this->_udp_socket.get_guarded();
  udp_sock.close();
}

void
network_packet_write_handler::operator()(boost::system::error_code error,
                                         std::size_t bytes_written)
{
  if (error)
  {
    spdlog::debug("Packet write handler @ {}: Failed to send bytes, with: {}",
                  (void*)this,
                  error.message());
  }
  else
  {
    spdlog::debug("Packet write handler @ {}: Successfully send {} bytes",
                  (void*)this,
                  bytes_written);
  }

  auto ptr = this->queue.lock();
  if (ptr.get() != nullptr)
    ptr->remove_pending(this->packet_node);
  else
    spdlog::error("This should never happen! Function: {}", __func__);
}

network_packet_queue::buffer_t
network_packet_queue::make_network_packet_buffer(size_t size)
{
  return network_packet_queue::buffer_t(size, static_cast<std::byte>(0));
}

network_packet_queue::iterator_t
network_packet_queue::add(const packet_t& packet)
{
  auto [lock, packet_queue] = this->queue.get_guarded();

  packet_queue.emplace_back(make_network_packet_buffer(packet.size()));
  std::copy(
    std::begin(packet), std::end(packet), std::begin(packet_queue.back()));
  return --packet_queue.end();
}

network_packet_write_handler
network_packet_write_handler::make_write_packet_handler(
  network_packet_queue::iterator_t node_it,
  const std::shared_ptr<network_packet_queue>& queue)
{
  return network_packet_write_handler{ .queue = std::weak_ptr(queue),
                                       .packet_node = node_it };
}

bool
tx_eth_connection::tcp_send(packet_t packet)
{

  if (!this->is_opened())
    return false;

  boost::asio::async_write(
    this->_tcp_socket,
    boost::asio::buffer(packet.data(), packet.size()),
    [](const boost::system::error_code& error, std::size_t bytes_written)
    {
      if (error)
      {
        spdlog::debug("TCP write failed. An error occured {}", error.message());
      }
      else
      {
        spdlog::debug("Bytes {} written via TCP", bytes_written);
      }
    });

  return true;
}

void
tx_eth_connection::enqueue(packet_t packet)
{
  network_packet_queue::iterator_t packet_it =
    this->_udp_queue.get()->add(packet);

  network_packet_write_handler handler =
    network_packet_write_handler::make_write_packet_handler(packet_it,
                                                            this->_udp_queue);
  spdlog::debug("Created write handler for packet @ {}", (void*)&(*packet_it));

  spdlog::debug("Enqueued packet:");
  log_bytes(packet, spdlog::level::level_enum::debug);

  auto [lock, udp_sock] = this->_udp_socket.get_guarded();

  udp_sock.async_send_to(
    boost::asio::buffer(packet_it->data(), packet_it->size()),
    this->_udp_endpoint,
    handler);
}

template<typename Iter_>
constexpr static bool
invalidates_iterators_on_remove()
{
  // TODO: Find a better way to check if removing element from container
  //       that contains given iterator invalidates other iterators on removal.
  using Iter = std::remove_cv_t<std::remove_reference_t<Iter_>>;
  return std::is_same_v<typename Iter::iterator_category,
                        std::random_access_iterator_tag>;
}

void
network_packet_queue::remove_pending(network_packet_queue::iterator_t packet_it)
{
  static_assert(!invalidates_iterators_on_remove<decltype(packet_it)>());

  auto [lock, packet_queue] = this->queue.get_guarded();

  packet_queue.erase(packet_it);
}

void
ls_sending_task::stop()
{
  this->_ls_send_task.stop_and_wait();
}

bool
ls_sending_task::stop_and_configure(std::shared_ptr<frame_provider> p,
                                    ls_config_array configs)
{
  this->stop();
  if (!p)
    return false;

  this->_provider = std::move(p);
  this->_configs = std::move(configs);
  return true;
}

static void
wait_for_next_frame(frame_provider* fp, ls_sending_task::ms_t frame_duration)
{
  if (fp->waitable())
    fp->wait();
  else
    std::this_thread::sleep_for(frame_duration);
}

void
ls_sending_task::ls_send_loop()
{
  spdlog::debug("Lightshow send loop enter");
  unsigned int last_used_tx = lightshow_config::UNDEFINED_ID;
  for (auto& config : this->_configs)
  {
    std::optional tx_cont_opt = global_config.get_tx_context(config.id.tx);
    if (!tx_cont_opt.has_value())
      continue;

    auto& tx_cont = tx_cont_opt.value().get();

    tx_cont.add_or_send_lightshow(config);
    last_used_tx = config.id.tx;
  }

  std::optional tx_cont_opt = global_config.get_tx_context(last_used_tx);
  if (!tx_cont_opt.has_value())
    return;

  auto& tx_cont = tx_cont_opt.value().get();
  tx_cont.flush();

  wait_for_next_frame(this->_provider.get(), this->_frame_duration);
  spdlog::debug("Lightshow send loop exit");
}

void
rssi_sending_task::stop()
{
  this->_rssi_send_task.stop_and_wait();
}

void
rssi_sending_task::stop_and_configure(rssi_config config)
{
  this->stop();
  // NOLINTNEXTLINE(performance-move-const-arg)
  this->_config = std::move(config);
}

void
rssi_sending_task::rssi_send_loop()
{
  spdlog::debug("RSSI send loop enter");

  std::optional tx_cont_opt = global_config.get_tx_context(_config.tx);
  if (!tx_cont_opt.has_value())
  {
    spdlog::error("Wrong TX context {}", _config.tx);
    return;
  }

  auto& tx_cont = tx_cont_opt.value().get();

  tx_cont.send_rssi(this->_config);

  std::this_thread::sleep_for(ms_t{ this->_config.interval });
}

#include "playlist.hpp"

#include <piwo/color.h>
#include <piwo/common_intersection_operators.h>
#include <spdlog/spdlog.h>

#include "iterator.hpp"
#include "ui_iface.hpp"

#include <piwo/frame.h>

constexpr size_t PLAYLIST_FPS = 20;
constexpr piwo::color BLACK_PIXEL = { 0, 0, 0 };
constexpr size_t FRAME_DURATION = 50;
;

constexpr static std::chrono::milliseconds
playlist_frame_rate(size_t fps)
{
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
  return std::chrono::milliseconds(1000 / fps);
}

playlist_t::playlist_t(resolution_t<size_t> resolution)
  : frame_provider(true, provider_id::PLAYLIST)
  , _frame_rate_thread(&playlist_t::framerate_thread_handler, this)
{
  if (resolution.width == 0 || resolution.height == 0)
  {
    this->_frame_buffer_ptr = piwo::alloc_frame_shared(1, 1);
  }
  else
  {
    this->_frame_buffer_ptr =
      piwo::alloc_frame_shared(resolution.width, resolution.height);
  }
}

void
playlist_t::reset_current_animation()
{
  auto [lock, anims] = this->_animations.get_guarded();

  if (anims.size())
  {
    this->_current_animation = 0;
  }
  else
  {
    this->_current_animation = PLAYLIST_INVALID_ANIMATION_INDEX;
  }
}

playlist_t::~playlist_t()
{
  this->_framesync.locked_scope(
    [this](framesync_data& framesync)
    {
      framesync._is_finished = true;
      this->_cv.notify_all();
    });

  this->_frame_buffer_ptr->fill(BLACK_PIXEL);
  internal_stop_thread_and_wait();
}

piwo::frames_intersection::frame_t
playlist_t::get_frame_()
{
  return this->_frame_buffer_ptr;
}

auto
playlist_t::get_current_animation_frame_count()
{
  auto [lock, anims] = this->_animations.get_guarded();
  return anims[this->_current_animation].anim.frame_count();
}

void
playlist_t::framerate_thread_handler()
{
  while (this->_frame_rate_timer.is_valid() && this->_frame_rate_timer.wait())
  {
    if (this->_current_frame >= this->get_current_animation_frame_count())
    {
      if (!this->next())
      {
        this->internal_reset_non_blocking();
      }
    }
    else
    {
      this->_framesync.locked_scope(
        [&](framesync_data& framesync)
        {
          framesync._rendered = false;
          const auto future_frame =
            this->_current_frame + 1 < this->get_current_animation_frame_count()
              ? this->_current_frame + 1
              : this->_current_frame;

          auto [lock, anims] = this->_animations.get_guarded();

          piwo::unsafe_intersection_apply(
            *this->_frame_buffer_ptr,
            anims[this->_current_animation].anim[this->_current_frame],
            0,
            0,
            piwo::frame_assign_op);

          piwo::unsafe_intersection_apply(
            *this->_future_frame_store._frame,
            anims[this->_current_animation].anim[future_frame],
            0,
            0,
            piwo::frame_assign_op);

          framesync._rendered = true;
          this->_cv.notify_all();
        });

      ++this->_current_frame;
    }
  }

  this->_framesync.locked_scope(
    [&](framesync_data& framesync)
    {
      framesync._is_finished = true;
      this->_cv.notify_all();
    });
}

bool
playlist_t::add_animation(named_animation anim)
{
  if (this->is_running())
    return false;

  anim.id = last_animation_id++;

  this->_animations.locked_scope(
    [&](anim_vector& anims)
    {
      this->buffer_resolution_check(anim);
      auto id_copy = anim.id;
      anims.emplace_back(std::move(anim));

      playlist_animation_added(id_copy);
    });

  return true;
}

std::optional<piwo::animation>
playlist_t::get_current_animation()
{
  if (this->_current_animation == PLAYLIST_INVALID_ANIMATION_INDEX)
    return std::nullopt;

  auto [lock, anims] = this->_animations.get_guarded();

  return anims[this->_current_animation].anim;
}

bool
playlist_t::next()
{
  auto [lock, anims] = this->_animations.get_guarded();

  if (this->_current_animation < 0 ||
      (static_cast<size_t>(this->_current_animation) + 1) >= anims.size())
    return false;

  this->_music_player.stop();
  this->_lasers.stop_lasers(this->_current_animation);
  ++this->_current_animation;
  this->_lasers.start_lasers(this->_current_animation);
  this->_current_frame = 0;

  if (this->load_music_for_animation(anims[this->_current_animation]))
  {
    this->_music_player.play();
  }

  display_animation_changed();

  return true;
}

bool
playlist_t::prev()
{
  if (this->_current_animation <= 0)
    return false;

  auto [lock, anims] = this->_animations.get_guarded();

  this->_lasers.stop_lasers(this->_current_animation);
  --this->_current_animation;
  this->_lasers.start_lasers(this->_current_animation);
  this->_current_frame = 0;
  this->_music_player.stop();
  if (this->load_music_for_animation(anims[this->_current_animation]))
  {
    this->_music_player.play();
  }

  display_animation_changed();

  return true;
}

bool
playlist_t::set_animation(size_t index)
{
  auto [lock, anims] = this->_animations.get_guarded();

  if (index >= anims.size())
    return false;

  std::optional narrowed_index_opt = narrow<int64_t>(index);

  if (!narrowed_index_opt.has_value())
  {
    spdlog::error("Unsupported animation index = {}", index);
    return false;
  }

  this->_lasers.stop_lasers(this->_current_animation);
  this->_current_animation = narrowed_index_opt.value();
  this->_lasers.start_lasers(this->_current_animation);
  this->_current_frame = 0;

  this->_music_player.stop();
  if (this->load_music_for_animation(anims[this->_current_animation]))
  {
    this->_music_player.play();
  }

  piwo::unsafe_intersection_apply(
    *this->_frame_buffer_ptr,
    anims[this->_current_animation].anim[this->_current_frame],
    0,
    0,
    piwo::frame_assign_op);

  internal_start_thread_if_not_running();
  display_animation_changed();
  animation_started();

  return true;
}

bool
playlist_t::set_frame_no(size_t frame_no)
{
  auto [lock, anims] = this->_animations.get_guarded();

  if (this->_current_animation == PLAYLIST_INVALID_ANIMATION_INDEX ||
      frame_no > anims[this->_current_animation].anim.frame_count())
  {
    return false;
  }

  this->_current_frame = frame_no;

  bool was_running = this->is_running();
  this->_music_player.pause();
  this->_music_player.set_music_position(this->_current_frame * FRAME_DURATION);

  if (was_running)
    this->_music_player.play();

  return true;
}

void
playlist_t::wait()
{
  auto [lock, framesync] = this->_framesync.get_guarded();

  this->_cv.wait(lock,
                 [&] { return framesync._rendered || framesync._is_finished; });
}

std::optional<size_t>
playlist_t::get_current_animation_index()
{
  if (this->_current_animation < 0)
  {
    return std::nullopt;
  }

  return static_cast<size_t>(this->_current_animation);
}

size_t
playlist_t::get_current_animation_frame_no()
{
  return this->_current_frame;
}

bool
playlist_t::load_music_for_animation(const named_animation& anim)
{
  // the proper mp3 file has to be in the same folder
  // as animation and file name until extension shall
  // be the same
  constexpr int piwo7_ext_strlen = std::size("piwo7");
  const auto anim_path_no_ext =
    anim.name.substr(0, anim.name.size() - piwo7_ext_strlen + 1);
  const auto anim_music = anim_path_no_ext + "mp3";
  return this->_music_player.load_music(anim_music.c_str());
}

bool
playlist_t::start()
{
  auto [lock, anims] = this->_animations.get_guarded();

  if (this->_current_animation < 0 && anims.size() > 0)
  {
    this->_current_animation = 0;
  }
  if (static_cast<size_t>(this->_current_animation) > anims.size())
  {
    return false;
  }
  if (this->_music_player.inactive())
  {
    if (this->load_music_for_animation(anims[this->_current_animation]))
    {
      this->_music_player.play();
      this->_lasers.start_lasers(this->_current_animation);
    }
  }
  else
  {
    this->_music_player.play();
  }
  internal_start_thread_if_not_running();

  spdlog::debug("Animation started");
  display_animation_changed();
  animation_started();

  return true;
}

void
playlist_t::pause()
{
  this->_music_player.pause();
  internal_stop_thread_and_wait();
  spdlog::debug("Animation paused");
  animation_paused();
}

void
playlist_t::stop()
{
  this->_music_player.stop();
  this->_lasers.stop_lasers(this->_current_animation);
  internal_stop_thread_and_wait();
  this->_frame_buffer_ptr->fill(BLACK_PIXEL);
  this->_future_frame_store._frame->fill(BLACK_PIXEL);
  this->_current_animation = PLAYLIST_INVALID_ANIMATION_INDEX;
  this->_current_frame = 0;
  spdlog::debug("Animation stopped");
  display_animation_changed();
  animation_stopped();
}

void
playlist_t::resize_(size_t width, size_t height)
{
  spdlog::trace(
    "{} width = {} height = {}", __PRETTY_FUNCTION__, width, height);

  this->_frame_buffer_ptr = piwo::alloc_frame_shared(width, height);
  this->_future_frame_store._frame = piwo::alloc_frame_shared(width, height);
}

void
playlist_t::buffer_resolution_check(const named_animation& animation)
{
  const auto anim_width = animation.anim.width();
  const auto anim_height = animation.anim.height();

  if (anim_width <= _frame_buffer_ptr->width &&
      anim_height <= _frame_buffer_ptr->height)
  {
    return;
  }

  const auto new_width = std::max(anim_width, _frame_buffer_ptr->width);
  const auto new_height = std::max(anim_height, _frame_buffer_ptr->height);

  resize_(new_width, new_height);
}

void
playlist_t::internal_start_thread_if_not_running()
{
  if (!this->is_running())
  {
    if (!this->_frame_rate_thread.is_stopped())
    {
      this->_frame_rate_thread.stop_and_wait();
    }
    this->_frame_rate_timer.start(
      std::chrono::seconds(0), playlist_frame_rate(PLAYLIST_FPS), false);

    this->_frame_rate_thread.resume();
  }
}

void
playlist_t::internal_stop_thread_and_wait()
{
  this->_frame_rate_timer.stop();
  if (!this->_frame_rate_thread.is_stopped())
  {
    this->_frame_rate_thread.stop_and_wait();
  }
}

void
playlist_t::internal_reset_non_blocking()
{
  this->_frame_rate_timer.stop();
  this->_music_player.stop();
  this->_frame_rate_thread.stop();
  this->_lasers.stop_lasers(this->_current_animation);
  this->reset_current_animation();
  this->_current_frame = 0;
  this->_frame_buffer_ptr->fill(BLACK_PIXEL);
  this->_future_frame_store._frame->fill(BLACK_PIXEL);
  animation_stopped();
  display_animation_changed();
}

void
playlist_t::connect_audio(int handle)
{
  if (!this->_music_player.init_stream(handle))
  {
    return;
  }

  auto [lock, anims] = this->_animations.get_guarded();

  // this sequence is an edge case
  // when sombody forgot to load the music
  // and don't stopped the animation
  if (!anims.empty() && this->_current_frame > 0)
  {
    bool was_running = this->is_running();
    this->pause();
    if (this->load_music_for_animation(anims[this->_current_animation]))
    {
      // need to start the music
      // before we first time set the
      // position
      this->_music_player.play();
      this->_music_player.pause();
      this->_music_player.set_music_position(this->_current_frame *
                                             FRAME_DURATION);
    }
    if (was_running)
      this->start();
  }

  audio_out_connection_status_changed(true);
}

void
playlist_t::change_animation_index(size_t from, size_t to)
{
  spdlog::trace("{} from = {} to = {}", __PRETTY_FUNCTION__, from, to);

  auto [lock, anims] = this->_animations.get_guarded();

  if (this->is_running())
  {
    spdlog::warn("Animation is running. Rejecting playlist modification");
    return;
  }

  if (from >= anims.size() || to > anims.size())
  {
    spdlog::warn("Wrong index. Rejecting playlist modification");
    return;
  }

  auto animation_to_be_moved = std::move(anims[from]);
  auto animation_to_be_moved_id = animation_to_be_moved.id;

  std::optional narrowed_index_from = narrow<long>(from);
  std::optional narrowed_index_to = narrow<long>(to);

  if (!narrowed_index_from.has_value()) [[unlikely]]
  {
    spdlog::warn("Got unsupported animation index == {}. Ignoring.", from);
    return;
  }

  if (!narrowed_index_to.has_value()) [[unlikely]]
  {
    spdlog::warn("Got unsupported animation index == {}. Ignoring.", to);
    return;
  }

  anims.erase(::next(anims.begin(), narrowed_index_from.value()));
  anims.insert(::next(anims.cbegin(), narrowed_index_to.value()),
               std::move(animation_to_be_moved));

  playlist_animation_index_changed(animation_to_be_moved_id);
}

void
playlist_t::remove_animation_by_index(size_t index)
{
  spdlog::trace("{} index = {}", __PRETTY_FUNCTION__, index);

  auto [lock, anims] = this->_animations.get_guarded();

  if (this->is_running())
  {
    spdlog::warn("Animation is running. Rejecting playlist modification");
    return;
  }

  if (index >= anims.size())
  {
    spdlog::warn("Wrong index. Rejecting playlist modification");
    return;
  }

  spdlog::info("Animation {} removed", anims[index].name);

  auto removed_id = anims[index].id;

  anims.erase(::next(anims.begin(), index));
  playlist_animation_removed(removed_id);
}

void
playlist_t::remove_animation_by_id(animation_id_t id)
{
  spdlog::trace("{} id = {}", __PRETTY_FUNCTION__, id);

  auto [lock, anims] = _animations.get_guarded();

  if (this->is_running())
  {
    spdlog::warn("Animation is running. Rejecting playlist modification");
    return;
  }

  std::optional index_opt = this->get_animation_index(id);

  if (!index_opt.has_value())
  {
    spdlog::warn("Wrong id. Rejecting playlist modification");
    return;
  }

  spdlog::info("Animation {} removed", anims[index_opt.value()].name);

  anims.erase(::next(anims.begin(), index_opt.value()));
  playlist_animation_removed(id);
}

std::optional<size_t>
playlist_t::get_animation_index(animation_id_t id)
{
  std::optional<size_t> index_opt;
  this->_animations.locked_scope(
    [&](anim_vector& anims)
    {
      for (size_t index = 0; index < anims.size(); index++)
      {
        if (anims[index].id == id)
        {
          index_opt = index;
          break;
        }
      }
    });

  return index_opt;
}

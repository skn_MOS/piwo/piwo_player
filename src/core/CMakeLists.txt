cmake_minimum_required(VERSION 3.13)

file(GLOB CORE_SRC "*.cpp")

if(NOT "${LIB_UI}" STREQUAL "")
  list(REMOVE_ITEM CORE_SRC ${CMAKE_CURRENT_SOURCE_DIR}/ui_iface.cpp)
endif()

add_library(player_core OBJECT ${CORE_SRC})
target_include_directories(player_core PUBLIC
  ${RapidJSON_INCLUDE_DIRS}
  ${libbass_DIR}
)

target_compile_options(player_core PUBLIC "${COMMON_DIAGNOSTIC_FLAGS}")

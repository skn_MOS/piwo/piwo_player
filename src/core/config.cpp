#include "config.hpp"
#include "effects.hpp"
#include "piwo_core.hpp"

#include <cstdint>
#include <functional>
#include <rapidjson/rapidjson.h>

static void
forward_packet_and_reset_builder(
  std::optional<std::reference_wrapper<tx_context_t>> tx_cont_opt,
  piwo::forward_w_cid_builder& forward_builder,
  piwo::raw_packet raw_packet)
{
  if (!tx_cont_opt.has_value())
  {
    // reset builder and exit
    std::optional forward_builder_opt =
      piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

    forward_builder = forward_builder_opt.value();
    return;
  }
  // forward packet
  auto& tx_cont = tx_cont_opt.value();
  auto forward = piwo::forward_w_cid(forward_builder);
  tx_cont.get().connection.tcp_send(
    piwo::raw_packet(forward.data(), forward.size()));

  // reset builder
  std::optional forward_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);

  forward_builder = forward_builder_opt.value();
}

std::weak_ptr<frame_provider>
config_t::weak_provider(frame_provider::provider_id id)
{
  switch (id)
  {
    case frame_provider::provider_id::RENDER:
    {
      auto [lck, re] = this->_render_engine.get_guarded();
      return re;
    }

    case frame_provider::provider_id::PLAYLIST:
    {
      auto [lck, playlists] = this->_playlists.get_guarded();
      return playlists._playlist;
    }

    case frame_provider::provider_id::FUTURE_PLAYLIST:
    {
      auto [lck, playlists] = this->_playlists.get_guarded();
      return playlists._future_playlist;
    }

    default:
      return {};
  };
}

bool
config_t::add_step(pipeline_config& config)
{
  std::optional guard_application = this->_application_state.try_lock();
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guard_application.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Render "
                 "engine configuration failed!");
    return false;
  }

  auto& [lock_application, application_state] = guard_application.value();
  auto& [lock_re, re] = guard_re.value();

  if (application_state != application_state_t::IDLE)
  {
    spdlog::warn(
      "Player is not in IDLE state. Render engine configuration failed!");
    return false;
  }

  const auto source_provider_id =
    lookup_provider(config.source_provider_name.c_str());

  const auto destination_provider_id =
    lookup_provider(config.destination_provider_name.c_str());

  auto source_provider = this->weak_provider(source_provider_id);

  auto destination_provider = this->weak_provider(destination_provider_id);

  auto step_opt =
    render_engine_step::make_step(source_provider,
                                  destination_provider,
                                  config.off_x,
                                  config.off_y,
                                  lookup_effect(config.effect.c_str()));
  if (!step_opt.has_value())
  {
    spdlog::warn(
      "Provider step cannot be created. Render engine configuration failed!");
    return false;
  }

  auto step = step_opt.value();
  auto status = re.get()->add_step(step);
  if (status != render_engine::state_t::re_ok)
  {
    config.pipeline_id = INVALID_STEP_ID;
    return false;
  }
  config.pipeline_id = step.id;

  // send back the configuration with updated status
  render_engine_configured(config);
  return true;
}

bool
config_t::remove_step(step_id id)
{
  std::optional guard_application = this->_application_state.try_lock();
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guard_application.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Render "
                 "engine step removing failed!");
    return false;
  }

  auto& [lock_application, application_state] = guard_application.value();
  auto& [lock_re, re] = guard_re.value();

  if (application_state != application_state_t::IDLE)
  {
    spdlog::warn(
      "Player is not in IDLE state. Render engine step removing failed!");
    return false;
  }

  auto status = re.get()->blocking_remove(id);
  if (status != render_engine::state_t::re_ok)
  {
    return false;
  }

  return true;
}

bool
config_t::refresh_step(step_id id)
{
  std::optional guard_application = this->_application_state.try_lock();
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guard_application.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Render "
                 "engine step removing failed!");
    return false;
  }

  auto& [lock_application, application_state] = guard_application.value();
  auto& [lock_re, re] = guard_re.value();

  if (application_state != application_state_t::IDLE)
  {
    spdlog::warn(
      "Player is not in IDLE state. Render engine step removing failed!");
    return false;
  }

  auto status = re.get()->blocking_refresh(id);
  if (status != render_engine::state_t::re_ok)
  {
    return false;
  }

  return true;
}

bool
config_t::change_step_state(step_id id, bool state)
{
  std::optional guard_application = this->_application_state.try_lock();
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guard_application.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Render "
                 "engine step removing failed!");
    return false;
  }

  auto& [lock_application, application_state] = guard_application.value();
  auto& [lock_re, re] = guard_re.value();

  if (application_state != application_state_t::IDLE)
  {
    spdlog::warn(
      "Player is not in IDLE state. Render engine step removing failed!");
    return false;
  }

  auto status = re.get()->set_step_state(id, state);

  if (status != render_engine::state_t::re_ok)
  {
    return false;
  }

  render_engine_step_state_changed(id, state);
  return true;
}

void
config_t::refresh_audio_out_list()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  BASS_DEVICEINFO info;
  audio_list devices;
  for (auto nr = 0ULL; BASS_GetDeviceInfo(nr, &info); nr++)
  {
    if (info.flags & BASS_DEVICE_ENABLED)
    {
      devices.emplace_back(info.name);
    }
    else
    {
      spdlog::error("Bass serach failed");
    }
  }
  audio_out_list_changed(devices);
}

void
config_t::refresh_midi_list()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  RtMidiOut midiout;
  unsigned int ports = midiout.getPortCount();
  midi_list devices;
  for (unsigned int i = 0; i < ports; i++)
  {
    try
    {
      devices.push_back(midiout.getPortName(i));
    }
    catch (RtMidiError& error)
    {
      error.printMessage();
      return;
    }
  }
  midi_list_changed(devices);
}

void
config_t::auto_configure_modules(
  [[maybe_unused]] const std::vector<piwo::uid>& uid_vec)
{
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting display auto "
                     "configuraction request!");
        return;
      }
      application_state = application_state_t::DISPLAY_AUTO_CONFIG;
      application_state_changed(application_state);

      state_handler_scope(
        [&]
        {
          spdlog::warn("Bump! auto_configure_modules uid not implemented");
          // TODO(all) Implement LSD
        },
        application_state);
    });
}

void
config_t::auto_configure_modules(
  [[maybe_unused]] const std::vector<ipos_t>& tiles_vec)
{
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting display auto "
                     "configuraction request!");
        return;
      }
      application_state = application_state_t::DISPLAY_AUTO_CONFIG;
      application_state_changed(application_state);

      state_handler_scope(
        [&]
        {
          spdlog::warn("Bump! auto_configure_modules ipos not implemented");
          // TODO(all) Implement LSD
        },
        application_state);
    });
}

application_state_t
config_t::get_application_state()
{
  auto [lock, application_state] = this->_application_state.get_guarded();
  return application_state;
}

void
config_t::set_resolution(uresolution_t::underlying_type width,
                         uresolution_t::underlying_type height)
{
  if (width > MAX_RESOLUTION_WIDTH || height > MAX_RESOLUTION_HEIGHT ||
      width < MIN_RESOLUTION_WIDTH || height < MIN_RESOLUTION_HEIGHT)
  {
    spdlog::warn(
      "Resolution value is out of bounds. Rejecting set resolution request!");
    return;
  }

  std::vector<piwo::uid> modules_out_of_range;

  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn(
          "Player is not in IDLE state. Rejecting resolution change!");
        return;
      }
      this->_modules.locked_scope(
        [&](modinfo_t& modules)
        {
          for (auto module : modules)
          {
            if (module.position.x >=
                  static_cast<ipos_t::underlying_type>(width) ||
                module.position.y >=
                  static_cast<ipos_t::underlying_type>(height))
            {
              modules_out_of_range.push_back(module.base.uid);
            }
          }
        });

      for (auto module_out_of_range : modules_out_of_range)
      {
        ipos_t unassigned_position{ .x = MODULE_INVALID_POS,
                                    .y = MODULE_INVALID_POS };
        this->assign_module_position(module_out_of_range, unassigned_position);
      }

      this->_resolution.locked_scope(
        [&](uresolution_t& resolution)
        {
          resolution.width = width;
          resolution.height = height;
        });

      spdlog::info("New resolution {}x{}", width, height);
      event_loop::resize_data resize_packet{ width, height };

      this->_event_loop.locked_scope([&resize_packet](event_loop& ev)
                                     { ev.send_event(resize_packet); });

      config_resolution_changed();
    });
}

void
config_t::assign_module_position(piwo::uid& uid, ipos_t& position)
{
  bool emit_data_changed_signal = false;
  if ((position.x < 0 && position.y >= 0) ||
      (position.y < 0 && position.x >= 0))
  {
    spdlog::warn("Inconsistent position x: {} y: {}. Rejecting assing module "
                 "position request!",
                 position.x,
                 position.y);
    return;
  }
  std::optional<piwo::uid> module_that_was_swapped;
  modinfo key{};
  key.base.uid = uid;

  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting assing module "
                     "position request!");
        return;
      }

      this->_modules.locked_scope(
        [&](modinfo_t& modules)
        {
          auto module_handle = modules.extract(key);

          if (!module_handle)
          {
            spdlog::error(
              "Failed to assign module position. UID[{:s}] not found.",
              uid.to_ascii());
            return;
          }

          if (position.x >= 0 && position.y >= 0)
          {
            for (auto module : modules)
            {
              if (module.position.x == position.x &&
                  module.position.y == position.y)
              {
                auto uid_module_to_swap_pos_with = module.base.uid;
                modinfo key_second{};
                key_second.base.uid = uid_module_to_swap_pos_with;
                auto module_handle_to_swap_pos_with =
                  modules.extract(key_second);
                if (!module_handle_to_swap_pos_with)
                {
                  spdlog::critical("This should never happen");
                  return;
                }

                module_handle_to_swap_pos_with.value().position =
                  module_handle.value().position;
                spdlog::info("Module uid[{}] assigned to pos x: {} y: {}",
                             module.base.uid.to_ascii(),
                             module_handle_to_swap_pos_with.value().position.x,
                             module_handle_to_swap_pos_with.value().position.y);
                modules.insert(std::move(module_handle_to_swap_pos_with));
                module_that_was_swapped = uid_module_to_swap_pos_with;
                break;
              }
            }
          }

          module_handle.value().position = position;
          modules.insert(std::move(module_handle));
          emit_data_changed_signal = true;
          spdlog::info("Module uid[{}] assigned to pos x: {} y: {}",
                       uid.to_ascii(),
                       position.x,
                       position.y);
        }); // locked scope
    });     // locked scope

  if (emit_data_changed_signal)
  {
    if (module_that_was_swapped.has_value())
    {
      config_module_data_changed(*module_that_was_swapped);
    }
    config_module_data_changed(uid);
  }
}

void
config_t::set_module_id(piwo::uid& uid, int64_t id)
{
  bool emit_data_changed_signal = false;
  modinfo key{};
  key.base.uid = uid;

  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn(
          "Player is not in IDLE state. Rejecting set module id request!");
        return;
      }

      this->_modules.locked_scope(
        [&](modinfo_t& modules)
        {
          for (auto module : modules)
          {
            if (module.base.id == id)
            {
              spdlog::warn("Module with ID {} already exists. Rejecting", id);
              return;
            }
          }

          auto module_handle = modules.extract(key);

          if (!module_handle)
          {
            spdlog::error("Failed to set module ID. UID[{:s}] not found.",
                          uid.to_ascii());
            return;
          }
          module_handle.value().base.id = id;
          modules.insert(std::move(module_handle));
          emit_data_changed_signal = true;
          spdlog::info("Module uid[{}] ID set to {}", uid.to_ascii(), id);
        }); // locked scope
    });     // locked scope

  if (emit_data_changed_signal)
  {
    config_module_data_changed(uid);
  }
}

void
config_t::set_module_logic_address(piwo::uid& uid,
                                   piwo::logic_address_t logic_address)
{
  bool emit_data_changed_signal = false;
  modinfo key{};
  key.base.uid = uid;

  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting set module "
                     "logic address request!");
        return;
      }

      if (!this->_ls_task.is_configured())
      {
        spdlog::warn("Lightshow not configured, first commit configuration. "
                     "Rejecting setting logic address!");
        return;
      }

      this->_modules.locked_scope(
        [&](modinfo_t& modules)
        {
          for (auto module : modules)
          {
            if (module.logic_address == logic_address)
            {
              spdlog::trace(
                "Module with logic address {} already exists. Swapping",
                logic_address);
              auto module_lhs_handle = modules.extract(key);

              if (!module_lhs_handle)
              {
                spdlog::error(
                  "Failed to swap module logic address. UID[{:s}] not found.",
                  key.base.uid.to_ascii());
                return;
              }

              auto module_rhs_handle = modules.extract(module);
              uint8_t logic_address_tmp =
                module_rhs_handle.value().logic_address;
              module_rhs_handle.value().logic_address =
                module_lhs_handle.value().logic_address;
              module_lhs_handle.value().logic_address = logic_address_tmp;
              auto uid_lhs = module_lhs_handle.value().base.uid;
              auto uid_rhs = module_rhs_handle.value().base.uid;
              modules.insert(std::move(module_lhs_handle));
              modules.insert(std::move(module_rhs_handle));
              spdlog::trace(
                "Module logic address with uid[{}] was swapped with "
                "module of uid[{}] ",
                uid_lhs.to_ascii(),
                uid_rhs.to_ascii());

              config_module_data_changed(uid_lhs);
              config_module_data_changed(uid_rhs);
              return;
            }
          }

          auto module_handle = modules.extract(key);

          if (!module_handle)
          {
            spdlog::error(
              "Failed to set module logic address. UID[{:s}] not found.",
              uid.to_ascii());
            return;
          }
          module_handle.value().logic_address = logic_address;
          modules.insert(std::move(module_handle));
          emit_data_changed_signal = true;
          spdlog::info("Module uid[{}] logic address set to {}",
                       uid.to_ascii(),
                       logic_address);
        }); // locked scope
    });     // locked scope

  if (emit_data_changed_signal)
  {
    config_module_data_changed(uid);
  }
}

void
config_t::set_modules_gateway_id(const std::vector<piwo::uid>& uids,
                                 piwo::gateway_id_t gateway_id)
{
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting set modules "
                     "gateway id request!");
        return;
      }

      if (gateway_id >= MAX_TX_COUNT)
      {
        spdlog::warn("Setting gatway id to {} failed. Max number of active "
                     "gateways is {}",
                     gateway_id,
                     MAX_TX_COUNT);
        return;
      }

      for (auto uid : uids)
      {
        bool emit_data_changed_signal = false;
        modinfo key{};
        key.base.uid = uid;

        this->_modules.locked_scope(
          [&](modinfo_t& modules)
          {
            auto module_handle = modules.extract(key);

            if (!module_handle)
            {
              spdlog::error(
                "Failed to set module gateway id. UID[{:s}] not found.",
                uid.to_ascii());
              return;
            }
            module_handle.value().gateway_id = gateway_id;
            modules.insert(std::move(module_handle));
            emit_data_changed_signal = true;
            spdlog::info("Module uid[{}] gateway id set to {}",
                         uid.to_ascii(),
                         gateway_id);
          }); // locked scope

        if (emit_data_changed_signal)
        {
          config_module_data_changed(uid);
        }
      }
    }); // locked scope
}

void
config_t::set_modules_radio_id(const std::vector<piwo::uid>& uids,
                               piwo::radio_id_t radio_id)
{
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting set modules "
                     "radio id request!");
        return;
      }
      for (auto uid : uids)
      {
        bool emit_data_changed_signal = false;
        modinfo key{};
        key.base.uid = uid;

        this->_modules.locked_scope(
          [&](modinfo_t& modules)
          {
            auto module_handle = modules.extract(key);

            if (!module_handle)
            {
              spdlog::error(
                "Failed to set module radio id. UID[{:s}] not found.",
                uid.to_ascii());
              return;
            }
            module_handle.value().radio_id = radio_id;
            modules.insert(std::move(module_handle));
            emit_data_changed_signal = true;
            spdlog::info(
              "Module uid[{}] radio id set to {}", uid.to_ascii(), radio_id);
          }); // locked scope

        if (emit_data_changed_signal)
        {
          config_module_data_changed(uid);
        }
      }
    }); // locked scope
}

void
config_t::set_modules_static_color(const std::vector<piwo::uid>& uids,
                                   piwo::color static_color)
{
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting set modules "
                     "static color request!");
        return;
      }

      for (auto uid : uids)
      {
        bool emit_data_changed_signal = false;
        modinfo key{};
        key.base.uid = uid;

        this->_modules.locked_scope(
          [&](modinfo_t& modules)
          {
            auto module_handle = modules.extract(key);

            if (!module_handle)
            {
              spdlog::error(
                "Failed to set module radio id. UID[{:s}] not found.",
                uid.to_ascii());
              return;
            }
            module_handle.value().static_color = static_color;
            modules.insert(std::move(module_handle));
            emit_data_changed_signal = true;
            spdlog::info("Module uid[{}] static color set to #{:x}",
                         uid.to_ascii(),
                         static_color.packed());
          }); // locked scope

        if (emit_data_changed_signal)
        {
          config_module_data_changed(uid);
        }
      }
    }); // locked scope
}

void
config_t::blink_modules(
  [[maybe_unused]] const std::vector<piwo::uid>& blink_modules)
{
  bool failed = false;
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting blink "
                     "modules request!");
        failed = true;
        return;
      }
    });

  if (failed)
  {
    return;
  }

  std::array<piwo::net_byte_t, FORAWRDING_BUFFER_SIZE> packet_buffer{};
  piwo::raw_packet raw_packet(packet_buffer.data(), packet_buffer.size());
  std::optional forward_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);
  auto forward_builder = forward_builder_opt.value();
  piwo::gateway_id_t last_used_tx = 0;

  auto [lock, modules] = this->_modules.get_guarded();

  for (const auto& m_uid : blink_modules)
  {
    auto mod = std::find_if(modules.begin(),
                            modules.end(),
                            [&m_uid](const modinfo& m)
                            { return m.base.uid.packed() == m_uid.packed(); });
    if (mod == modules.end())
      continue;

    std::optional buff_slot_opt = forward_builder.get_next_slot();
    // no more memory send packet and continue
    if (!buff_slot_opt.has_value())
    {
      forward_packet_and_reset_builder(
        get_tx_context(last_used_tx), forward_builder, raw_packet);
      buff_slot_opt = forward_builder.get_next_slot();
    }

    if (last_used_tx != mod->gateway_id)
    {
      forward_packet_and_reset_builder(
        get_tx_context(last_used_tx), forward_builder, raw_packet);
      last_used_tx = mod->gateway_id;
      buff_slot_opt = forward_builder.get_next_slot();
    }

    auto buff_slot = *buff_slot_opt;

    std::optional blink_builder_opt =
      piwo::blink_builder::make_blink_builder(buff_slot);

    // not enough memory for blink packet
    // send packet and create new one
    if (!blink_builder_opt.has_value())
    {
      forward_packet_and_reset_builder(
        get_tx_context(last_used_tx), forward_builder, raw_packet);
      buff_slot_opt = forward_builder.get_next_slot();

      blink_builder_opt =
        piwo::blink_builder::make_blink_builder(buff_slot_opt.value());
    }
    auto blink_builder = *blink_builder_opt;

    blink_builder.set_uid(mod->base.uid);

    auto blink_packet = piwo::blink(blink_builder);
    forward_builder.commit(blink_packet, mod->radio_id);
  }
  forward_packet_and_reset_builder(
    get_tx_context(last_used_tx), forward_builder, raw_packet);
}

void
config_t::color_modules(
  [[maybe_unused]] const std::vector<piwo::uid>& color_modules)
{
  bool failed = false;
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting color "
                     "modules request!");
        failed = true;
        return;
      }
    });

  if (failed)
  {
    return;
  }

  std::array<piwo::net_byte_t, FORAWRDING_BUFFER_SIZE> packet_buffer{};
  piwo::raw_packet raw_packet(packet_buffer.data(), packet_buffer.size());
  std::optional forward_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);
  auto forward_builder = forward_builder_opt.value();
  piwo::gateway_id_t last_used_tx = 0;

  auto [lock, modules] = this->_modules.get_guarded();

  for (uint8_t i = 0; const auto& m_uid : color_modules)
  {
    auto mod = std::find_if(modules.begin(),
                            modules.end(),
                            [&m_uid](const modinfo& m)
                            { return m.base.uid.packed() == m_uid.packed(); });

    if (mod == modules.end())
      continue;

    std::optional buff_slot_opt = forward_builder.get_next_slot();
    // no more memory send packet and continue
    if (!buff_slot_opt.has_value())
    {
      forward_packet_and_reset_builder(
        get_tx_context(last_used_tx), forward_builder, raw_packet);
      buff_slot_opt = forward_builder.get_next_slot();
    }

    if (last_used_tx != mod->gateway_id)
    {
      forward_packet_and_reset_builder(
        get_tx_context(last_used_tx), forward_builder, raw_packet);
      last_used_tx = mod->gateway_id;
      buff_slot_opt = forward_builder.get_next_slot();
    }

    auto buff_slot = *buff_slot_opt;

    std::optional constant_builder_opt =
      piwo::constant_color_builder::make_constant_color_builder(buff_slot);

    // not enough memory for const color packet
    // send packet and create new one with color
    if (!constant_builder_opt.has_value())
    {
      forward_packet_and_reset_builder(
        get_tx_context(last_used_tx), forward_builder, raw_packet);
      buff_slot_opt = forward_builder.get_next_slot();

      constant_builder_opt =
        piwo::constant_color_builder::make_constant_color_builder(
          buff_slot_opt.value());
    }
    auto constant_color_builder = *constant_builder_opt;

    constant_color_builder.set_seq(i);
    constant_color_builder.set_uid(mod->base.uid);
    constant_color_builder.set_color(
      { mod->static_color.r, mod->static_color.g, mod->static_color.b });

    auto color_packet = piwo::constant_color(constant_color_builder);
    forward_builder.commit(color_packet, mod->radio_id);
    i++;
  }
  forward_packet_and_reset_builder(
    get_tx_context(last_used_tx), forward_builder, raw_packet);
}

void
config_t::assign_la_for_modules(
  [[maybe_unused]] const std::vector<piwo::uid>& la_modules)
{
  bool failed = false;
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting color "
                     "modules request!");
        failed = true;
        return;
      }
    });

  if (failed)
  {
    return;
  }

  std::array<piwo::net_byte_t, FORAWRDING_BUFFER_SIZE> packet_buffer{};
  piwo::raw_packet raw_packet(packet_buffer.data(), packet_buffer.size());
  std::optional forward_builder_opt =
    piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet);
  auto forward_builder = forward_builder_opt.value();
  piwo::gateway_id_t last_used_tx = 0;

  auto [lock, modules] = this->_modules.get_guarded();

  for (uint8_t i = 0; const auto& m_uid : la_modules)
  {
    auto mod = std::find_if(modules.begin(),
                            modules.end(),
                            [&m_uid](const modinfo& m)
                            { return m.base.uid.packed() == m_uid.packed(); });
    if (mod == modules.end())
      continue;

    std::optional buff_slot_opt = forward_builder.get_next_slot();
    // no more memory send packet and continue
    if (!buff_slot_opt.has_value())
    {
      forward_packet_and_reset_builder(
        this->get_tx_context(last_used_tx), forward_builder, raw_packet);
      buff_slot_opt = forward_builder.get_next_slot();
    }

    if (last_used_tx != mod->gateway_id)
    {
      forward_packet_and_reset_builder(
        this->get_tx_context(last_used_tx), forward_builder, raw_packet);
      last_used_tx = mod->gateway_id;
      buff_slot_opt = forward_builder.get_next_slot();
    }

    auto buff_slot = *buff_slot_opt;

    auto la_builder_opt =
      piwo::assign_la_builder::make_assign_la_builder(buff_slot);

    // not enough memory for assign logic address packet
    // send packet and create new one
    if (!la_builder_opt.has_value())
    {
      forward_packet_and_reset_builder(
        this->get_tx_context(last_used_tx), forward_builder, raw_packet);
      buff_slot_opt = forward_builder.get_next_slot();

      la_builder_opt =
        piwo::assign_la_builder::make_assign_la_builder(buff_slot);
    }
    auto la_builder = *la_builder_opt;

    la_builder.set_seq(i);
    la_builder.set_uid(mod->base.uid);
    la_builder.set_la(mod->logic_address);

    auto la_packet = piwo::assign_la(la_builder);
    forward_builder.commit(la_packet, mod->radio_id);
    i++;
  }
  forward_packet_and_reset_builder(
    this->get_tx_context(last_used_tx), forward_builder, raw_packet);
}

void
config_t::load_raw_modules(std::istream& f)
{
  rapidjson::Document d;
  rapidjson::BasicIStreamWrapper bsw(f);
  d.ParseStream(bsw);

  if (!d.IsArray())
  {
    spdlog::warn("Invalid or corrupted raw modules file!");
    return;
  }

  std::vector<piwo::uid> new_modules;
  this->_modules.locked_scope(
    [&](modinfo_t& modules)
    {
      for (rapidjson::SizeType i = 0; i < d.Size(); i++)
      {
        modinfo module{};
        module.static_color.r =
          std::numeric_limits<piwo::color::subpixel_t>::max();
        module.static_color.g =
          std::numeric_limits<piwo::color::subpixel_t>::max();
        module.static_color.b =
          std::numeric_limits<piwo::color::subpixel_t>::max();
        module.position.x = MODULE_INVALID_POS;
        module.position.y = MODULE_INVALID_POS;
        deserialize(module.base, d[i].GetObject());
        if (auto [module_it, inserted] =
              // Now module is trivially-copyale so move does not have any
              // effect but if it will become non-trivially-copyable in the
              // future then it will be good to have that move here
              // NOLINTNEXTLINE [performance-move-const-arg]
            modules.insert(std::move(module));
            inserted)
        {
          new_modules.push_back(module_it->base.uid);
        }
        else
        {
          spdlog::warn("Duplicated module uid[{}] will not be loaded to config",
                       module_it->base.uid.to_ascii());
        }
      }
    });

  for (auto new_module : new_modules)
  {
    config_module_added(new_module);
  }
}

void
config_t::load_raw_modules_from_file(const char* path)
{
  std::ifstream isf(path);
  if (!isf)
  {
    spdlog::warn(
      "Failed to open file {} Errno: {} ({}). Loading raw modules failed!",
      path,
      errno,
      strerror(errno));
    return;
  }
  load_raw_modules(isf);
}

void
config_t::save_to_file(const char* path)
{
  rapidjson::Document d;
  rapidjson::StringBuffer buffer;
  rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);

  std::optional guards = scoped_lock_try(this->_modules, this->_resolution);
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guards.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Saving "
                 "config to file can't be finished!");
    return;
  }

  auto& [locks, values] = guards.value();
  auto& [modules, resolution] = values;
  auto& [lock_re, re] = guard_re.value();

  writer.StartObject();
  writer.Key("display_configuration");

  writer.StartObject();
  writer.Key("modules");
  writer.StartArray();

  for (auto module_info : modules)
  {
    serialize(module_info, writer);
  }

  writer.EndArray();
  writer.Key("display_width");
  writer.Uint64(resolution.width);
  writer.Key("display_height");
  writer.Uint64(resolution.height);
  writer.EndObject(); // display_configuration

  writer.Key("render_engine_config");

  re.get()->serialize(writer);

  writer.EndObject();
  // serialize ethernet
  // serialize audio
  // serialize lasers
  // serialize ls_task
  // TODO(all) serialize rest of this structure

  std::ofstream out_file(path, std::ofstream::out);
  if (!out_file)
  {
    spdlog::warn(
      "Failed to open file {} Errno: {} ({}). Saving configuration failed!",
      path,
      errno,
      strerror(errno));
    return;
  }

  out_file.write(buffer.GetString(), static_cast<long>(buffer.GetSize()));
  out_file.close();
}

void
config_t::load_from_file(const char* path)
{
  std::optional guards =
    scoped_lock_try(this->_modules, this->_application_state);
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guards.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Loading "
                 "config from file can't be finished!");
    return;
  }

  auto& [lock, values] = guards.value();
  auto& [modules, application_state] = values;
  auto& [lock_re, re] = guard_re.value();

  if (application_state != application_state_t::IDLE)
  {
    spdlog::warn(
      "Player is not in IDLE state. Rejecting loading config from file!");
    return;
  }

  std::ifstream input_file(path);
  if (!input_file)
  {
    spdlog::warn(
      "Failed to open file {} Errno: {} ({}). Loading configuration failed!",
      path,
      errno,
      strerror(errno));
    return;
  }

  rapidjson::Document d;
  rapidjson::IStreamWrapper bsw(input_file);
  d.ParseStream(bsw);

  if (d.HasMember("display_configuration"))
  {
    auto display_configuration_object = d["display_configuration"].GetObject();

    if (!display_configuration_object.HasMember("modules") ||
        !display_configuration_object["modules"].IsArray())
    {
      spdlog::warn("Invalid or corrupted configuration file!");
      return;
    }

    if (!display_configuration_object.HasMember("display_width") ||
        !display_configuration_object["display_width"].IsInt64() ||
        !display_configuration_object.HasMember("display_height") ||
        !display_configuration_object["display_height"].IsInt64())
    {
      spdlog::warn("Invalid or corrupted configuration file!");
      return;
    }

    this->set_resolution(
      display_configuration_object["display_width"].GetInt64(),
      display_configuration_object["display_height"].GetInt64());

    rapidjson::Document::Array attributes =
      display_configuration_object["modules"].GetArray();

    modules.clear();
    for (rapidjson::SizeType i = 0; i < attributes.Size(); i++)
    {
      modinfo module{};
      deserialize(module, attributes[i].GetObject());
      if (auto [module_it, inserted] =
            // Now module is trivially-copyale so move does not have any
            // effect but if it will become non-trivially-copyable in the
            // future then it will be good to have that move here
            // NOLINTNEXTLINE [performance-move-const-arg]
          modules.insert(std::move(module));
          !inserted)
      {
        spdlog::warn("Duplicated module uid[{}] will not be loaded to config",
                     module_it->base.uid.to_ascii());
      }
    }
    modules_config_container_rearanged();
  }

  if (d.HasMember("render_engine_config"))
  {
    auto re_object = d["render_engine_config"].GetObject();
    if (!re_object.HasMember("steps") || !re_object["steps"].IsArray())
    {
      spdlog::warn(
        "Invalid or corrupted configuration file! RE steps are not an array");
      return;
    }

    re.get()->clear_steps();

    rapidjson::Document::Array re_steps = re_object["steps"].GetArray();
    for (rapidjson::SizeType i = 0; i < re_steps.Size(); i++)
    {
      pipeline_config step;
      bool deserialized = deserialize(step, re_steps[i].GetObject());
      if (deserialized)
        this->add_step(step);
    }
  }

  // TODO(all) deserialize rest of this structure
}

void
config_t::load_animation(const char* path)
{
  using po = piwo::animation::parse_option;

  named_animation anim;

  po parse_op;
  parse_op.translate = po::translate_type::ms50_exact;
  const auto err = anim.anim.build_from_file(path, parse_op);

  if (err != piwo::animation::error_e::ok)
  {
    spdlog::warn("Failed to load animation from {}", path);
    return;
  }

  spdlog::info("Loaded new animation from {}", path);

  auto [lock, playlist] = this->acquire_playlist();

  anim.name = path;
  playlist.add_animation(std::move(anim));
  playlist.reset_current_animation();
}

void
config_t::enable_light_show()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  std::optional guard_application = this->_application_state.try_lock();
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guard_application.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Lightshow "
                 "hasn't been enabled!");
    return;
  }

  auto& [lock_application, application_state] = guard_application.value();
  auto& [lock_re, re] = guard_re.value();

  if (application_state != application_state_t::IDLE)
  {
    spdlog::warn(
      "Player is not in IDLE state. Rejecting enable lightshow request!");
    return;
  }

  if (!this->_ls_task.run())
  {
    spdlog::warn("Lightshow not configured, first commit configuration. "
                 "Rejecting enable lightshow request!");
    return;
  }

  application_state = application_state_t::LIGHTSHOW;
  application_state_changed(application_state);

  spdlog::info("Lightshow enabled");

  re.get()->start();

  light_show_started();
}

void
config_t::disable_light_show()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  std::optional guard_application = this->_application_state.try_lock();
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guard_application.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Lightshow "
                 "hasn't been disabled!");
    return;
  }

  auto& [lock_application, application_state] = guard_application.value();
  auto& [lock_re, re] = guard_re.value();

  if (application_state != application_state_t::LIGHTSHOW)
  {
    spdlog::warn("Player is not in LIGHTSHOW state. Rejecting disable "
                 "lightshow request!");
    return;
  }

  application_state = application_state_t::IDLE;

  spdlog::info("Lightshow disabled");
  application_state_changed(application_state);

  light_show_stopped();

  re.get()->stop();
  this->_ls_task.stop();
}

void
config_t::enable_rssi()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  bool failed = false;
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::IDLE)
      {
        spdlog::warn("Player is not in IDLE state. Rejecting enable "
                     "RSSI request!");
        failed = true;
        return;
      }

      application_state = application_state_t::RSSI;
      application_state_changed(application_state);
    });

  if (failed)
  {
    return;
  }

  this->_rssi_config.locked_scope(
    [&](rssi_config& rssi)
    {
      this->_rssi_task.stop_and_configure(rssi);
      this->_rssi_task.run();
    });

  spdlog::info("RSSI enabled");

  rssi_started();
}

void
config_t::disable_rssi()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  bool failed = false;
  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state != application_state_t::RSSI)
      {
        spdlog::warn("Player is not in RSSI state. Rejecting disable "
                     "RSSI request!");
        failed = true;
        return;
      }

      application_state = application_state_t::IDLE;
      application_state_changed(application_state);
    });

  if (failed)
  {
    return;
  }

  this->_rssi_task.stop();

  spdlog::info("RSSI disabled");

  rssi_stopped();
}

void
config_t::start_animation()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  auto [lock, playlist] = this->acquire_playlist();
  playlist.start();
}

void
config_t::pause_animation()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  auto [lock, playlist] = this->acquire_playlist();
  playlist.pause();
}

void
config_t::stop_animation()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  auto [lock, playlist] = this->acquire_playlist();
  playlist.stop();
}

void
config_t::change_animation_to(size_t index)
{
  spdlog::trace("{} {}", __PRETTY_FUNCTION__, index);

  auto [lock, playlist] = this->acquire_playlist();
  const auto result = playlist.set_animation(index);

  if (result)
  {
    spdlog::debug("Animation changed to one with index == {}", index);
    // display_animation_changed();
  }
  else
  {
    spdlog::error("Animation change failed");
  }
}

void
config_t::connect_launchad()
{
  // TODO implement lp as a frame provider
  // std::unique_lock lck(this->_providers_mtx);
  // locked_scope(
  //   [&]
  //   {
  //     this->_providers.push_back(std::make_unique<launchpad>(name));
  //     if (!_lp.connect())
  //     {
  //       spdlog::error("Connection to launchpad failed");
  //       return;
  //     }
  //     spdlog::info("Launchpad connected");
  //   },
  //   this->_lp_mtx);
}

void
config_t::disconnect_launchad()
{
  // locked_scope(
  //   [&]
  //   {
  //     if (!_lp.disconnect())
  //     {
  //       spdlog::error("Failed to disconnect from launchpad");
  //       return;
  //     }
  //     spdlog::info("Launchpad disconnected");
  //   },
  //   this->_lp_mtx);
}

std::optional<std::reference_wrapper<tx_context_t>>
config_t::get_tx_context(size_t i)
{
  if (i >= MAX_TX_COUNT)
    return std::nullopt;

  // NOLINTNEXTLINE [cppcoreguidelines-pro-bounds-constant-array-index]
  return _tx_conn[i];
}

bool
config_t::commit_configuration()
{
  std::optional guards = scoped_lock_try(
    this->_modules, this->_resolution, this->_application_state);
  std::optional guard_re = this->_render_engine.try_lock();

  if (!guards.has_value() || !guard_re.has_value())
  {
    spdlog::warn("Some other operation is being taken on player. Logic address "
                 "configuration failed!");
    return false;
  }

  auto& [locks, values] = guards.value();
  auto& [modules, resolution, application_state] = values;
  auto& [lock_re, re] = guard_re.value();

  if (application_state != application_state_t::IDLE)
  {
    spdlog::warn("Player is not in IDLE state. Rejecting commit commiting!");
    return false;
  }

  // assign to each group
  auto owners_info = own_offsets(modules, re.get()->get_frame());

  // calculate offsets
  configure_logic_address(modules, owners_info, resolution.width);

  std::weak_ptr<frame_provider> provider_weak =
    this->weak_provider(frame_provider::provider_id::RENDER);
  auto provider = provider_weak.lock();
  if (!provider)
    return false;

  this->_ls_task.stop_and_configure(provider, owners_info);

  return true;
}

void
config_t::tx_connect(const std::string& host,
                     const std::string& tcp_port,
                     const std::string& udp_port)
{
  std::optional tx_cont_opt = this->get_tx_context(0);
  if (!tx_cont_opt.has_value())
    return;

  this->tx_disconnect();
  auto& tx_cont = tx_cont_opt.value().get();

  const auto res =
    tx_cont.connection.open(host.c_str(), tcp_port.c_str(), udp_port.c_str());

  eth_state_changed(res);
}

void
config_t::tx_disconnect()
{
  std::optional tx_cont_opt = this->get_tx_context(0);
  if (!tx_cont_opt.has_value())
    return;
  auto& tx_cont = tx_cont_opt.value().get();

  tx_cont.connection.close();
  eth_state_changed(false);
}

void
config_t::set_rssi_config(const rssi_config& config)
{
  if (config.tx >= MAX_TX_COUNT || config.tx < MIN_GATEWAY_ID ||
      config.tx > MAX_GATEWAY_ID || config.radio < MIN_RADIO_ID ||
      config.radio > MAX_RADIO_ID || config.interval > MAX_RSSI_INTERVAL ||
      config.interval < MIN_RSSI_INTERVAL)
  {
    spdlog::warn("Wrong RSSI configuration. Rejecting RSSI settings");
    this->_rssi_config.locked_scope([&](rssi_config& rssi)
                                    { rssi_config_changed(rssi); });
  }

  this->_rssi_config.locked_scope(
    [&](rssi_config& rssi)
    {
      rssi = config;
      this->_rssi_task.stop_and_configure(rssi);
      rssi_config_changed(config);
    });

  spdlog::info("RSSI config changed to TX ID={} Radio ID={} Interval={}",
               config.tx,
               config.radio,
               config.interval);

  this->_application_state.locked_scope(
    [&](application_state_t& application_state)
    {
      if (application_state == application_state_t::RSSI)
      {
        this->_rssi_task.run();
      }
    });
}

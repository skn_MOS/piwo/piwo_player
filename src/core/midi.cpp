#include "midi.hpp"

#include <memory>
#include <spdlog/spdlog.h>

midi::midi()
{
  // TODO rewrite the midi module to some other library
  // or implement the midi in PIWO:::MIPC
  // I really don't like this implementation
  try
  {
    this->_midiout = std::make_unique<RtMidiOut>();
  }
  catch (RtMidiError& error)
  {
    this->_midiout = nullptr;
    spdlog::info("Can't create midi device, reason: {}", error.getMessage());
  }
}

void
midi::connect(int device, int base_offset)
{
  if (!this->_midiout)
    return;

  try
  {
    this->_midiout->openPort(device);
  }
  catch (RtMidiError& error)
  {
    spdlog::info("Can't open midi port, reason: {}", error.getMessage());
  }
  this->_base_offset = static_cast<uint8_t>(base_offset);
  this->_connected = true;
}

void
midi::disconnect()
{
  if (!this->_midiout)
    return;

  try
  {
    this->_midiout->closePort();
  }
  catch (RtMidiError& error)
  {
    spdlog::info("Can't close midi port, reason: {}", error.getMessage());
  }
  this->_connected = false;
}

void
midi::start_lasers(uint8_t animation_offset)
{
  if (!this->_connected)
    return;

  auto message = std::to_array<uint8_t>(
    { MIDI_HEADER,
      static_cast<uint8_t>(this->_base_offset + animation_offset),
      LASER_TRIGGER_START });
  try
  {
    this->_midiout->sendMessage(message.data(), message.size());
  }
  catch (RtMidiError& error)
  {
    spdlog::info("Can't send message via MIDI , reason: {}",
                 error.getMessage());
  }
}

void
midi::stop_lasers(uint8_t animation_offset)
{
  if (!this->_connected)
    return;

  auto message = std::to_array<uint8_t>(
    { MIDI_HEADER,
      static_cast<uint8_t>(this->_base_offset + animation_offset),
      LASER_TRIGGER_STOP });
  try
  {
    this->_midiout->sendMessage(message.data(), message.size());
  }
  catch (RtMidiError& error)
  {
    spdlog::info("Can't send message via MIDI , reason: {}",
                 error.getMessage());
  }
}

bool
midi::is_connected()
{
  return _connected;
}

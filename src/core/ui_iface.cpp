#include "ui_iface.hpp"

void
start_ui([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
  spdlog::warn("No UI connected");
}

void
config_module_added([[maybe_unused]] const piwo::uid uid)
{
  spdlog::warn("No UI connected");
}

void
config_module_removed([[maybe_unused]] const piwo::uid uid)
{
  spdlog::warn("No UI connected");
}

void
config_module_data_changed([[maybe_unused]] const piwo::uid uid)
{
  spdlog::warn("No UI connected");
}

void
modules_config_container_rearanged()
{
  spdlog::warn("No UI connected");
}

void
config_resolution_changed()
{
  spdlog::warn("No UI connected");
}

void
application_state_changed([[maybe_unused]] const application_state_t state)
{
  spdlog::warn("No UI connected");
}

void
display_animation_changed()
{
  spdlog::warn("No UI connected");
}

void
playlist_changed()
{
  spdlog::warn("No UI connected");
}

void
playlist_animation_added([[maybe_unused]] const animation_id_t id)
{
  spdlog::warn("No UI connected");
}

void
playlist_animation_removed([[maybe_unused]] const animation_id_t id)
{
  spdlog::warn("No UI connected");
}

void
playlist_animation_index_changed([[maybe_unused]] const animation_id_t id)
{
  spdlog::warn("No UI connected");
}

void
light_show_started()
{
  spdlog::warn("No UI connected");
}

void
light_show_stopped()
{
  spdlog::warn("No UI connected");
}

void
rssi_started()
{
  spdlog::warn("No UI connected");
}

void
rssi_stopped()
{
  spdlog::warn("No UI connected");
}

void
animation_started()
{
  spdlog::warn("No UI connected");
}

void
animation_paused()
{
  spdlog::warn("No UI connected");
}

void
animation_stopped()
{
  spdlog::warn("No UI connected");
}

void
render_engine_state_chnaged(bool)
{
  spdlog::warn("No UI connected");
}

void
render_engine_configured([[maybe_unused]] const pipeline_config config)
{
  spdlog::warn("No UI connected");
}

void
render_engine_step_removed([[maybe_unused]] step_id id)
{
  spdlog::warn("No UI connected");
}

void
render_engine_step_state_changed([[maybe_unused]] step_id id,
                                 [[maybe_unused]] bool state)
{
  spdlog::warn("No UI connected");
}

void
audio_in_list_changed(const audio_list)
{
  spdlog::warn("No UI connected");
}

void
audio_out_list_changed(const audio_list)
{
  spdlog::warn("No UI connected");
}

void
audio_out_connection_status_changed(bool)
{
  spdlog::warn("No UI connected");
}

void
midi_list_changed(const midi_list)
{
  spdlog::warn("No UI connected");
}

void
midi_connection_status_changed(bool)
{
  spdlog::warn("No UI connected");
}

void
eth_state_changed(bool)
{
  spdlog::warn("No UI connected");
}

void
rssi_config_changed([[maybe_unused]] rssi_config config)
{
  spdlog::warn("No UI connected");
}

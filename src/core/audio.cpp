#include "audio.hpp"
#include <spdlog/spdlog.h>

#include "typeutils.hpp"

bool
audio::init_stream(int device)
{
  constexpr int freq = 44100;

#ifdef __WIN32
  constexpr int flags = 0;
#elif __linux__
  constexpr int flags = BASS_DEVICE_DMIX;
#endif

  this->_devic_id = device;
  if (!BASS_Init(this->_devic_id, freq, flags, nullptr, nullptr))
  {
    spdlog::error("Bass init failed with error: {}", BASS_ErrorGetCode());
    return false;
  }

  this->set_volume(this->_volume);

  return true;
}

bool
audio::load_music(const char* file_name)
{
  this->_stream = BASS_StreamCreateFile(FALSE, file_name, 0, 0, 0);

  if (!(this->_stream))
  {
    spdlog::error("Bass init failed with error: {}", BASS_ErrorGetCode());
    return false;
  }

  if (BASS_ChannelIsActive(this->_stream) == BASS_ACTIVE_PLAYING &&
      !BASS_ChannelSetPosition(this->_stream, 0, BASS_FILEPOS_CURRENT))
  {
    spdlog::error("Bass reseting music pos failed with error: {}",
                  BASS_ErrorGetCode());
    return false;
  }

  this->_is_music_loaded = true;

  this->set_volume(this->_volume);

  return true;
}

void
audio::set_music_position(uint64_t miliseconds)
{
  if (!this->_is_music_loaded)
    return;
  constexpr double miliseconds_in_second = 1000.0;
  std::optional narrowed_mulliseconds_opt = narrow<double>(miliseconds);
  if (!narrowed_mulliseconds_opt.has_value())
  {
    spdlog::info(
      "Cant set music play position to {} miliseconds.Value out of bound",
      miliseconds);
  }
  double narrowed_miliseconds = narrowed_mulliseconds_opt.value();
  uint64_t pos_in_bytes = BASS_ChannelSeconds2Bytes(
    _stream, narrowed_miliseconds / miliseconds_in_second);

  BASS_ChannelSetPosition(this->_stream, pos_in_bytes, BASS_POS_BYTE);
}

void
audio::stop()
{
  if (!this->_is_music_loaded)
    return;

  BASS_ChannelStop(_stream);
  BASS_StreamFree(_stream);
  this->_is_music_loaded = false;
}

void
audio::set_volume(float volume)
{
  constexpr float max_volume_val = 1.0;

  if (volume > max_volume_val || volume < 0.0)
    return;

  this->_volume = volume;

  if (!this->_is_music_loaded)
    return;

  BASS_ChannelSetAttribute(_stream, BASS_ATTRIB_VOL, volume);
}

void
audio::free_stream()
{
  this->stop();
  BASS_Free();
}

#include "pipeline_config.hpp"
#include "config.hpp"
#include "effects.hpp"

#include <spdlog/spdlog.h>

static std::string
to_effect_name(piwo::frames_intersection_operator op)
{
  return lookup_effect(op);
}

static piwo::frames_intersection_operator
from_effect_name(const char* op_name)
{
  return lookup_effect(op_name);
}

static std::string
to_provider_name(frame_provider::provider_id id)
{
  return config_t::lookup_provider(id);
}

static frame_provider::provider_id
from_provider_name(const char* name)
{
  return config_t::lookup_provider(name);
}

bool
pipeline_config::operator==(const step_id id)
{
  return this->pipeline_id == id;
}

render_engine_step::render_engine_step(
  std::weak_ptr<frame_provider> fp1_w,
  std::weak_ptr<frame_provider> fp2_w,
  const piwo::frames_intersection::frame_t& f1,
  const piwo::frames_intersection::frame_t& f2,
  int64_t offx,
  int64_t offy,
  piwo::frames_intersection_operator e)
  : id(next_id++)
  , off_x(offx)
  , off_y(offy)
  , active(true)
  , effect(e)
  , _intersection(f2, f1, offx, offy)
  , _source_provider(std::move(fp1_w))
  , _destination_provider(std::move(fp2_w))
{
}

std::optional<render_engine_step>
render_engine_step::make_step(std::weak_ptr<frame_provider> fp1_w,
                              std::weak_ptr<frame_provider> fp2_w,
                              int64_t offx,
                              int64_t offy,
                              piwo::frames_intersection_operator e)
{
  auto fp1 = fp1_w.lock();
  if (!fp1)
    return std::nullopt;

  auto fp2 = fp2_w.lock();
  if (!fp2)
    return std::nullopt;

  return render_engine_step(
    fp1, fp2, fp1->get_frame(), fp2->get_frame(), offx, offy, e);
}

bool
render_engine_step::refresh()
{

  auto fp1 = _source_provider.lock();
  if (!fp1)
    return false;

  auto fp2 = _destination_provider.lock();
  if (!fp2)
    return false;

  this->_intersection =
    piwo::frames_intersection(fp2->get_frame(), fp1->get_frame(), off_x, off_y);
  return true;
}

bool
render_engine_step::operator==(const step_id id)
{
  return this->id == id;
}

template<>
void
serialize(render_engine_step& s, serialization_writer& writer)
{

  auto fp1 = s._source_provider.lock();
  if (!fp1)
  {
    spdlog::error(
      "Can't serialize source provider of render engine step with id {}", s.id);
    return;
  }

  auto fp2 = s._destination_provider.lock();
  if (!fp2)
  {
    spdlog::error(
      "Can't serialize destination provider of render engine step with id {}",
      s.id);
    return;
  }

  const auto effect_name = to_effect_name(s.effect);

  if (!effect_name.length())
  {
    spdlog::error("Can't serialize effect of render engine step with id {}",
                  s.id);
    return;
  }

  writer.StartObject();

  const auto source_name = to_provider_name(fp1->get_id());
  writer.String("source_provider");
  writer.String(source_name.c_str());

  const auto destination_name = to_provider_name(fp2->get_id());
  writer.String("destination_provider");
  writer.String(destination_name.c_str());

  writer.String("effect");
  writer.String(effect_name.c_str());

  writer.String("off_x");
  writer.Int64(s.off_x);

  writer.String("off_y");
  writer.Int64(s.off_y);
  writer.EndObject();
}

template<>
bool
deserialize(pipeline_config& c,
            json_object d) // NOLINT(performance-unnecessary-value-param)
{
  if (!d.HasMember("source_provider"))
    return false;

  const auto* const source_name = d["source_provider"].GetString();

  if (from_provider_name(source_name) ==
      frame_provider::provider_id::INVALID_PROVIDER)
  {
    spdlog::error("Invalid source provider name. Deserialization aborted.");
    return false;
  }

  c.source_provider_name = source_name;

  if (!d.HasMember("destination_provider"))
    return false;

  const auto* const destination_name = d["destination_provider"].GetString();

  if (from_provider_name(destination_name) ==
      frame_provider::provider_id::INVALID_PROVIDER)
  {
    spdlog::error(
      "Invalid destination provider name. Deserialization aborted.");
    return false;
  }

  c.destination_provider_name = destination_name;

  if (!d.HasMember("effect"))
    return false;

  c.effect = d["effect"].GetString();
  if (from_effect_name(c.effect.c_str()) == nullptr)
  {
    spdlog::error("Invalid effect name. Deserialization aborted.");
    return false;
  }

  if (!d.HasMember("off_x"))
    return false;
  c.off_x = d["off_x"].GetInt64();

  if (!d.HasMember("off_y"))
    return false;
  c.off_y = d["off_y"].GetInt64();

  return true;
}

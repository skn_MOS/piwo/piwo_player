#include "settings.hpp"

#include "config.hpp"

#include <spdlog/spdlog.h>

namespace gui
{
settings::settings(QQmlContext* context, QWidget* parent)
  : QObject(parent)
{
  qmlRegisterType<general_settings_model>(
    "general_settings_model", 1, 0, "GeneralSettingsModel");
  qmlRegisterType<ethernet_settings_model>(
    "ethernet_settings_model", 1, 0, "EthernetSettingsModel");
  qmlRegisterType<logger_model>("logger_model", 1, 0, "LoggerModel");

  if (!context)
  {
    spdlog::error("Null ptr qml context");
    std::terminate();
  }
  context->setContextProperty(QStringLiteral("general_settings_model"),
                              &this->_general_settings_model);
  context->setContextProperty(QStringLiteral("ethernet_settings_model"),
                              &this->_ethernet_settings_model);
  context->setContextProperty(QStringLiteral("logger_model"),
                              &this->_logger_model);
  context->setContextProperty(QStringLiteral("render_engine_model"),
                              &this->_render_engine_model);
  context->setContextProperty(QStringLiteral("pipeline_model"),
                              &this->_pipeline_model);
  context->setContextProperty(QStringLiteral("audio_model"),
                              &this->_audio_model);
  context->setContextProperty(QStringLiteral("midi_model"), &this->_midi_model);
}

void
settings::apply()
{
  spdlog::debug("Settings applied");
  this->_general_settings_model.apply();
  this->_ethernet_settings_model.apply();
}

void
settings::cancel()
{
  spdlog::debug("Settings canceled");
  this->_general_settings_model.cancel();
  this->_ethernet_settings_model.cancel();
}

void
settings::lp_connect()
{
  spdlog::debug("Connecting to launchpad");
  global_config.connect_launchad();
}

void
settings::lp_disconnect()
{
  spdlog::debug("Disconnecting from launchpad");
  global_config.disconnect_launchad();
}

} // namespace gui

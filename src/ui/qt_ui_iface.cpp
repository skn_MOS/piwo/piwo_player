#include "main_window.hpp"

#include <optional>

#include <QApplication>
#include <QString>
#include <QtWidgets>

#include "piwo.hpp"
#include "qml_debug.hpp"
#include "qt_ui_iface.hpp"

void
start_ui(int argc, char** argv)
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

  QApplication app(argc, argv);

  app.setOrganizationName("MOS");
  app.setOrganizationDomain("MOS");

  qmlRegisterType<gui::qml_debugger>(
    "com.mycompany.myapplication", 1, 0, "QMLDebugger");
  gui::qml_debugger qdbg;

  QQmlApplicationEngine engine;
  gui::main_window main_widget(engine);

  const QUrl url(QStringLiteral("qrc:/MainWindow.qml"));

  engine.rootContext()->setContextProperty("qdbg", &qdbg);

  // NOLINTNEXTLINE QT ownership joke
  engine.addImageProvider(QLatin1String("piwo7"), new piwo7_image_provider);

  QObject::connect(
    &engine,
    &QQmlApplicationEngine::objectCreated,
    &app,
    [url](QObject* obj, const QUrl& obj_url)
    {
      if (!obj && url == obj_url)
        QCoreApplication::exit(-1);
    },
    Qt::QueuedConnection);
  engine.load(url);

  global_qml_engine = &engine;
  app.exec();
}

void
config_module_added(const piwo::uid uid)
{
  qt_ui_if.config_module_added(uid);
}

void
config_module_removed(const piwo::uid uid)
{
  qt_ui_if.config_module_removed(uid);
}

void
config_module_data_changed(const piwo::uid uid)
{
  qt_ui_if.config_module_data_changed(uid);
}

void
modules_config_container_rearanged()
{
  qt_ui_if.modules_config_container_rearanged();
}

void
config_resolution_changed()
{
  qt_ui_if.config_resolution_changed();
}

void
application_state_changed(const application_state_t state)
{
  qt_ui_if.application_state_changed(state);
}

void
display_animation_changed()
{
  qt_ui_if.display_animation_changed();
}

void
playlist_changed()
{
  qt_ui_if.playlist_changed();
}

void
playlist_animation_added(const animation_id_t id)
{
  qt_ui_if.playlist_animation_added(id);
}

void
playlist_animation_removed(const animation_id_t id)
{
  qt_ui_if.playlist_animation_removed(id);
}

void
playlist_animation_index_changed(const animation_id_t id)
{
  qt_ui_if.playlist_animation_index_changed(id);
}

void
light_show_started()
{
  qt_ui_if.light_show_started();
}

void
light_show_stopped()
{
  qt_ui_if.light_show_stopped();
}

void
rssi_started()
{
  qt_ui_if.rssi_started();
}

void
rssi_stopped()
{
  qt_ui_if.rssi_stopped();
}

void
animation_started()
{
  qt_ui_if.animation_started();
}

void
animation_paused()
{
  qt_ui_if.animation_paused();
}

void
animation_stopped()
{
  qt_ui_if.animation_stopped();
}

void
render_engine_configured(const pipeline_config config)
{
  qt_ui_if.render_engine_configured(config);
}

void
render_engine_step_removed(step_id id)
{
  qt_ui_if.render_engine_step_removed(id);
}

void
render_engine_step_state_changed(step_id id, bool state)
{
  qt_ui_if.render_engine_step_state_changed(id, state);
}

void
render_engine_state_chnaged(bool state)
{
  qt_ui_if.render_engine_state_chnaged(state);
}

void
audio_in_list_changed(const audio_list devices)
{
  qt_ui_if.audio_in_list_changed(devices);
}

void
audio_out_list_changed(const audio_list devices)
{
  qt_ui_if.audio_out_list_changed(devices);
}

void
audio_out_connection_status_changed(bool state)
{
  qt_ui_if.audio_out_connection_status_changed(state);
}

void
midi_list_changed(const midi_list devices)
{
  qt_ui_if.midi_list_changed(devices);
}

void
midi_connection_status_changed(bool state)
{
  qt_ui_if.midi_connection_status_changed(state);
}

void
eth_state_changed(bool new_state)
{
  qt_ui_if.eth_state_changed(new_state);
}

void
rssi_config_changed(rssi_config config)
{
  qt_ui_if.rssi_config_changed(config);
}

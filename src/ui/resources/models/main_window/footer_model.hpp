#pragma once

#include <QObject>
#include <QQmlApplicationEngine>
#include <QString>

#include "config.hpp"
#include "qt_ui_iface.hpp"
#include "shared_enums.hpp"
#include "state.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class footer_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(int application_state READ get_application_state NOTIFY
               application_state_changed)
public:
  footer_model();
  ~footer_model() override = default;

  int
  get_application_state();

  Q_INVOKABLE void
  enable_disable_lightshow();

  Q_INVOKABLE void
  enable_disable_rssi();

signals:
  void
  application_state_changed(int application_state);

private slots:
  void
  on_application_state_changed(const application_state_t state);

private:
  gui::cpp_enum::application_state_enum _application_state;
};
} // namespace gui

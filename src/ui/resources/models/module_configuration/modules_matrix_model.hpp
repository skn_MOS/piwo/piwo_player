#pragma once

#include <QAbstractListModel>

#include "config.hpp"
#include "modinfo.hpp"
#include "module_edit_popup_model.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class modules_matrix_model : public QAbstractItemModel
{
  Q_OBJECT
public:
  modules_matrix_model();
  ~modules_matrix_model() override = default;

  enum module_role
  {
    ID_ROLE = Qt::UserRole,
    UID_ROLE,
    LOGIC_ADDRESS_ROLE,
    GATEWAY_ID_ROLE,
    RADIO_ID_ROLE,
    STATIC_COLOR_ROLE,
    POSITION_X_ROLE,
    POSITION_Y_ROLE,
    IS_HIGHLIGHTED,
  };

  void
  set_module_edit_popup_model(
    std::shared_ptr<module_edit_popup_model> module_edit_popup);

  int
  rowCount(const QModelIndex& parent = QModelIndex()) const override;

  int
  columnCount(const QModelIndex& parent = QModelIndex()) const override;

  QModelIndex
  index(int row,
        int column,
        const QModelIndex& parent = QModelIndex()) const override;

  QVariant
  data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

  QVariant
  headerData(int section,
             Qt::Orientation orientation,
             int role = Qt::DisplayRole) const override;

  QModelIndex
  parent(const QModelIndex& index) const override;

  bool
  setData(const QModelIndex& index,
          const QVariant& value,
          int role = Qt::EditRole) override;

  Qt::ItemFlags
  flags(const QModelIndex& index) const override;

  QHash<int, QByteArray>
  roleNames() const override;

  Q_INVOKABLE void
  module_dropped(QString uid, int x, int y);

  Q_INVOKABLE void
  match_modules_to_tiles();

  Q_INVOKABLE void
  match_tiles_to_modules();

  Q_INVOKABLE void
  highlight(int x, int y);

  Q_INVOKABLE void
  highlight_all();

  Q_INVOKABLE void
  highlight_row(int y);

  Q_INVOKABLE void
  highlight_column(int x);

  Q_INVOKABLE void
  highlight_all_tiles_from_last_highlighted_to(int x, int y);

  Q_INVOKABLE void
  dehighlight(int x, int y);

  Q_INVOKABLE void
  dehighlight_all();

  Q_INVOKABLE void
  edit_highlighted_modules();

  Q_INVOKABLE void
  blink_highlighted_modules();

  Q_INVOKABLE void
  color_highlighted_modules();

  Q_INVOKABLE void
  assing_la_for_highlighted_modules();

public slots:
  void
  on_config_resolution_changed();

  void
  on_module_added(const piwo::uid& uid);

  void
  on_module_data_changed(const piwo::uid& uid);

  void
  on_modules_container_rearanged();

private:
  std::shared_ptr<module_edit_popup_model> _module_edit_popup;
  std::map<piwo::uid::packed_type, ipos_t> _modules_positions;
  std::vector<ipos_t> _highlighted_modules;
};

} // namespace gui

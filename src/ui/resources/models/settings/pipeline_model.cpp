#include "pipeline_model.hpp"

#include <array>
#include <mutex>
#include <qnamespace.h>
#include <spdlog/spdlog.h>

#include "modinfo.hpp"

namespace gui
{
const auto COLUMNS_NAME =
  std::to_array({ "Id", "Source", "Destination", "Off X", "Off Y", "Effect" });

pipeline_model::pipeline_model([[maybe_unused]] QObject* parent)
{
  QObject::connect(&qt_ui_if,
                   &::qt_ui_iface::signal_render_engine_configured,
                   this,
                   &pipeline_model::on_pipeline_added);

  QObject::connect(&qt_ui_if,
                   &::qt_ui_iface::signal_render_engine_step_removed,
                   this,
                   &pipeline_model::on_pipeline_removed);

  QObject::connect(&qt_ui_if,
                   &::qt_ui_iface::signal_render_engine_step_state_changed,
                   this,
                   &pipeline_model::on_pipeline_step_state_changed);
}

int
pipeline_model::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  auto [lock, list] = this->_list.get_guarded();

  return static_cast<int>(list.size());
}

int
pipeline_model::columnCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  auto [lock, list] = this->_list.get_guarded();

  return COL_COUNT;
}

QVariant
pipeline_model::headerData(int section,
                           Qt::Orientation orientation,
                           int role) const
{
  if (role == Qt::DisplayRole)
  {
    if (orientation == Qt::Horizontal)
      return QString("%1").arg(COLUMNS_NAME.at(section));
  }
  return QVariant();
}

QVariant
pipeline_model::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant();

  auto [lock, list] = this->_list.get_guarded();

  std::optional narrowed_list_size = narrow<int>(list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", list.size());
    return QVariant();
  }

  if (index.row() > narrowed_list_size.value())
    return QVariant();

  auto config = list[index.row()];

  if (role == STEP_STATE && !config.is_highlighted)
  {
    return QVariant(config.state);
  }

  if (role == IS_HIGHLIGHTED)
  {
    return QVariant(config.is_highlighted);
  }

  switch (index.column())
  {
    case PIPEPLINE_ID:
      return QString("%1").arg(config.pipeline_id);

    case SRC_PROVIDER_NAME:
      return QString::fromStdString(config.source_provider_name);

    case DST_PROVIDER_NAME:
      return QString::fromStdString(config.destination_provider_name);

    case OFF_X:
      return QString("%1").arg(config.off_x);

    case OFF_Y:
      return QString("%1").arg(config.off_y);

    case EFFECT:
      return QString::fromStdString(config.effect);

    default:
      return QVariant();
  }
}

bool
pipeline_model::setData([[maybe_unused]] const QModelIndex& index,
                        [[maybe_unused]] const QVariant& value,
                        [[maybe_unused]] int role)
{
  return false;
}

Qt::ItemFlags
pipeline_model::flags([[maybe_unused]] const QModelIndex& index) const
{
  return Qt::NoItemFlags;
}

QHash<int, QByteArray>
pipeline_model::roleNames() const
{
  QHash<int, QByteArray> names;
  names[DISPLAY_DATA] = "display_data";
  names[IS_HIGHLIGHTED] = "is_highlighted";
  names[STEP_STATE] = "state";
  return names;
}

void
pipeline_model::on_pipeline_added(const pipeline_config& config)
{
  auto [lock, list] = this->_list.get_guarded();

  std::optional list_size_opt = narrow<int>(list.size());
  if (!list_size_opt.has_value())
  {
    spdlog::error("List index out of range. Too many items in pipeline");
    return;
  }

  int pos = list_size_opt.value();
  beginInsertRows(QModelIndex(), pos, pos);
  list.push_back(config);
  endInsertRows();
}

void
pipeline_model::on_pipeline_removed(const int id)
{
  auto [lock, list] = this->_list.get_guarded();

  auto config_iter = std::find_if(list.begin(),
                                  list.end(),
                                  [id](const pipeline_config& c)
                                  { return c.pipeline_id == id; });

  if (config_iter == list.end())
    return;

  std::optional list_size_opt = narrow<int>(config_iter - list.begin());
  if (!list_size_opt.has_value())
  {
    spdlog::error("List index out of range. Too many items in pipeline");
    return;
  }
  int pos = list_size_opt.value();

  beginRemoveRows(QModelIndex(), pos, pos);
  list.erase(config_iter);
  endRemoveRows();
}

void
pipeline_model::on_pipeline_step_state_changed(const int id, bool state)
{
  auto [lock, list] = this->_list.get_guarded();

  auto config_iter = std::find_if(list.begin(),
                                  list.end(),
                                  [id](const pipeline_config& c)
                                  { return c.pipeline_id == id; });
  if (config_iter == list.end())
    return;

  std::optional narrowed_row = narrow<int>(config_iter - list.begin());
  if (!narrowed_row.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", list.size());
    return;
  }

  int row = narrowed_row.value();

  list[row].state = state;
  emit this->dataChanged(
    this->index(row, 0), this->index(row, COL_COUNT - 1), { STEP_STATE });
}

void
pipeline_model::highlight(int row)
{
  auto [lock, list] = this->_list.get_guarded();

  std::optional narrowed_list_size = narrow<int>(list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", list.size());
    return;
  }

  if (row > narrowed_list_size.value())
    return;

  list[row].is_highlighted = true;
  emit this->dataChanged(
    this->index(row, 0), this->index(row, COL_COUNT - 1), { IS_HIGHLIGHTED });
}

void
pipeline_model::dehighlight(int row)
{
  auto [lock, list] = this->_list.get_guarded();

  std::optional narrowed_list_size = narrow<int>(list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", list.size());
    return;
  }

  if (row > narrowed_list_size.value())
    return;
  list[row].is_highlighted = false;
  emit this->dataChanged(
    this->index(row, 0), this->index(row, COL_COUNT - 1), { IS_HIGHLIGHTED });
}

void
pipeline_model::remove_highlighted()
{
  auto [lock, list] = this->_list.get_guarded();

  std::optional narrowed_list_size = narrow<int>(list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", list.size());
    return;
  }

  for (int i = narrowed_list_size.value() - 1; i >= 0; i--)
  {
    const auto& item = list[i];
    if (item.is_highlighted)
      global_config.remove_step(item.pipeline_id);
  }
}

void
pipeline_model::refresh_highlighted()
{
  auto [lock, list] = this->_list.get_guarded();

  std::optional narrowed_list_size = narrow<int>(list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", list.size());
    return;
  }

  for (int i = narrowed_list_size.value() - 1; i >= 0; i--)
  {
    const auto& item = list[i];
    if (item.is_highlighted)
      global_config.refresh_step(item.pipeline_id);
  }
}

void
pipeline_model::change_state_of_highlighted(bool state)
{
  auto [lock, list] = this->_list.get_guarded();

  std::optional narrowed_list_size = narrow<int>(list.size());
  if (!narrowed_list_size.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported modules count = {}", list.size());
    return;
  }

  for (int i = narrowed_list_size.value() - 1; i >= 0; i--)
  {
    const auto& item = list[i];
    if (item.is_highlighted)
      global_config.change_step_state(item.pipeline_id, state);
  }
}

} // namespace gui

#pragma once

#include "config.hpp"
#include <QObject>
#include <QString>

constexpr int INVALID_MIDI_PORT = -1;
namespace gui
{
class midi_model : public QObject
{
  Q_OBJECT

  Q_PROPERTY(bool connected READ get_midi_connected NOTIFY midi_connected)

  Q_PROPERTY(int midi_port READ get_midi_port WRITE set_midi_port)

  Q_PROPERTY(
    QStringList midi_list READ get_midi_devices NOTIFY midi_list_changed)

public:
  explicit midi_model(QObject* parent = nullptr);

  ~midi_model() = default;

  bool
  get_midi_connected()
  {
    return this->_midi_connected;
  }

  void
  set_midi_port(int handle)
  {
    if (this->_midi_connected)
      return;
    // I created virtual device (no midi),
    // decrement the index
    this->_midi_port = handle - 1;
  }

  int
  get_midi_port()
  {
    // I created virtual device (no midi),
    // increment the index
    return this->_midi_port + 1;
  }

  QStringList
  get_midi_devices()
  {
    return this->_midi_list;
  }

signals:
  void
  midi_connected(bool);
  void
  midi_list_changed(const QStringList& providers);

public slots:

  void
  midi_refresh()
  {
    if (this->_midi_connected)
      return;
    global_config.refresh_midi_list();
  }

  void
  midi_connect(int offset)
  {
    if (this->_midi_port == INVALID_MIDI_PORT)
      return;

    auto [lck, playlist] = global_config.acquire_playlist();
    playlist.connect_midi(this->_midi_port, offset);
  }

  void
  midi_disconnect()
  {
    auto [lck, playlist] = global_config.acquire_playlist();
    playlist.disconnect_midi();
  }

  void
  on_midi_connection_state_changed(bool state)
  {
    this->_midi_connected = state;
    emit midi_connected(this->_midi_connected);
  }

  void
  on_midi_list_changed(const midi_list& devices)
  {
    this->_midi_list.clear();
    this->_midi_list.push_back("No midi");
    for (const auto& dev : devices)
    {
      this->_midi_list.push_back(dev.c_str());
    }

    emit this->midi_list_changed(this->_midi_list);
  }

private:
  QStringList _midi_list;

  bool _midi_connected = false;
  int _midi_port = INVALID_MIDI_PORT;
};
} // namespace gui

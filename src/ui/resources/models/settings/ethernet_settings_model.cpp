#include <spdlog/spdlog.h>

#include "qt_ui_iface.hpp"
#include "settings/ethernet_settings_model.hpp"

namespace gui
{

ethernet_settings_model::ethernet_settings_model(
  [[maybe_unused]] QObject* parent)
  : QObject(parent)
  , _default_ip((const char*)tx_eth_connection::DEFAULT_IP_STRING)
  , _default_port_tcp((const char*)tx_eth_connection::DEFAULT_PORT_TCP_STRING)
  , _default_port_udp((const char*)tx_eth_connection::DEFAULT_PORT_UDP_STRING)
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_eth_state_changed,
                   this,
                   &ethernet_settings_model::on_eth_status_changed);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_rssi_config_changed,
                   this,
                   &ethernet_settings_model::on_rssi_config_changed);

  // Update UI with data from core
  this->cancel();
}

bool
ethernet_settings_model::get_eth_connected()
{
  return this->_eth_connection_state;
}

void
ethernet_settings_model::set_eth_connected(bool new_state)
{
  this->_eth_connection_state = new_state;
  emit eth_connection_state_changed();
}

Q_INVOKABLE void
ethernet_settings_model::tx_connect(QString ip,
                                    QString tcp_port,
                                    QString udp_port)
{
  auto ip_std_string = ip.toStdString();
  auto tcp_port_std_string = tcp_port.toStdString();
  auto udp_port_std_string = udp_port.toStdString();

  spdlog::debug("Connecting to tx {}:{}", ip_std_string, tcp_port_std_string);
  auto [mtx, ev] = global_config.acquire_event_loop();
  event_loop::eth_conn_data data{ ip_std_string,
                                  tcp_port_std_string,
                                  udp_port_std_string };
  ev.send_event(data);
}

Q_INVOKABLE void
ethernet_settings_model::tx_disconnect()
{
  spdlog::debug("Disconnecting from tx");
  auto [mtx, ev] = global_config.acquire_event_loop();
  event_loop::eth_disconn_data data;
  ev.send_event(data);
}

void
ethernet_settings_model::on_eth_status_changed(bool new_state)
{
  if (this->_eth_connection_state == new_state)
    return;

  this->_eth_connection_state = new_state;
  emit eth_connection_state_changed();
}

unsigned int
ethernet_settings_model::get_tx_id()
{
  return this->_rssi_config.tx;
}

void
ethernet_settings_model::set_tx_id(int id)
{
  auto tx_id = std::clamp(id, MIN_GATEWAY_ID, MAX_GATEWAY_ID);

  auto narrowed_tx_id_opt = narrow<unsigned int>(tx_id);
  if (!narrowed_tx_id_opt.has_value())
  {
    spdlog::warn("Unsupported tx ID={}. RSSI settings will not be applied", id);

    emit this->tx_id_changed(this->_rssi_config.tx);
    return;
  }

  this->_rssi_config.tx = narrowed_tx_id_opt.value();

  this->_rssi_tx_id_was_changed = true;

  emit this->tx_id_changed(this->_rssi_config.tx);
}

unsigned int
ethernet_settings_model::get_radio_id()
{
  return this->_rssi_config.radio;
}

void
ethernet_settings_model::set_radio_id(int id)
{
  auto radio_id = std::clamp(id, MIN_RADIO_ID, MAX_RADIO_ID);
  auto narrowed_radio_id_opt = narrow<uint8_t>(radio_id);
  if (!narrowed_radio_id_opt.has_value())
  {
    spdlog::warn("Unsupported radio ID={}. RSSI settings will not be applied",
                 id);

    emit this->radio_id_changed(this->_rssi_config.radio);
    return;
  }

  this->_rssi_config.radio = narrowed_radio_id_opt.value();

  this->_rssi_radio_id_was_changed = true;

  emit this->radio_id_changed(this->_rssi_config.radio);
}

unsigned int
ethernet_settings_model::get_interval()
{
  return this->_rssi_config.interval;
}

void
ethernet_settings_model::set_interval(int new_interval)
{
  auto interval =
    std::clamp(new_interval, MIN_RSSI_INTERVAL, MAX_RSSI_INTERVAL);

  auto narrowed_interval_opt = narrow<unsigned int>(interval);
  if (!narrowed_interval_opt.has_value())
  {
    spdlog::warn(
      "Unsupported rssi interval={}. RSSI settings will not be applied",
      new_interval);

    emit this->interval_changed(this->_rssi_config.interval);
    return;
  }

  this->_rssi_config.interval = narrowed_interval_opt.value();

  this->_rssi_interval_was_changed = true;

  emit this->interval_changed(this->_rssi_config.interval);
}

void
ethernet_settings_model::on_rssi_config_changed(rssi_config config)
{
  this->_rssi_config = config;

  this->_rssi_tx_id_was_changed = false;
  this->_rssi_radio_id_was_changed = false;
  this->_rssi_interval_was_changed = false;

  emit this->tx_id_changed(this->_rssi_config.tx);
  emit this->radio_id_changed(this->_rssi_config.radio);
  emit this->interval_changed(this->_rssi_config.interval);
}

void
ethernet_settings_model::apply()
{
  if (this->_rssi_tx_id_was_changed || this->_rssi_radio_id_was_changed ||
      this->_rssi_interval_was_changed)
  {
    this->_rssi_tx_id_was_changed = false;
    this->_rssi_radio_id_was_changed = false;
    this->_rssi_interval_was_changed = false;
    global_config.set_rssi_config(this->_rssi_config);
  }
}

void
ethernet_settings_model::cancel()
{
  auto [lck, rssi_config] = global_config.acquire_rssi_config();

  this->_rssi_config = rssi_config;

  this->_rssi_tx_id_was_changed = false;
  this->_rssi_radio_id_was_changed = false;
  this->_rssi_interval_was_changed = false;

  emit this->tx_id_changed(this->_rssi_config.tx);
  emit this->radio_id_changed(this->_rssi_config.radio);
  emit this->interval_changed(this->_rssi_config.radio);
}

} // namespace gui

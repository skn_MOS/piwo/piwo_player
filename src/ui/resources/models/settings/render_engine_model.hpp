#pragma once

#include "config.hpp"
#include "effects.hpp"
#include "ui_iface.hpp"
#include <QObject>
#include <QString>

namespace gui
{
class render_engine_model : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QStringList provider_list MEMBER _provider_list CONSTANT)
  Q_PROPERTY(QStringList effects_list MEMBER _effects_list CONSTANT)

  Q_PROPERTY(
    QString source_provider READ get_source_provider WRITE set_source_provider)

  Q_PROPERTY(QString destination_provider READ get_destination_provider WRITE
               set_destination_provider)

  Q_PROPERTY(QString effect READ get_effect WRITE set_effect)

  Q_PROPERTY(int off_x READ get_off_x WRITE set_off_x NOTIFY off_x_changed)
  Q_PROPERTY(int off_y READ get_off_y WRITE set_off_y NOTIFY off_y_changed)

public:
  explicit render_engine_model(QObject* parent = nullptr)
    : QObject(parent)
    , _provider_list(make_QStringList_from_provider(config_t::providers_names))
    , _effects_list(make_QStringList_from_provider(global_effects_list))
    , _config(set_defaults())
  {
  }

  ~render_engine_model() = default;

  void
  set_source_provider(const QString& p)
  {
    this->_config.source_provider_name = p.toStdString();
  }

  QString
  get_source_provider()
  {
    return this->_config.source_provider_name.c_str();
  }

  void
  set_destination_provider(const QString& p)
  {
    this->_config.destination_provider_name = p.toStdString();
  }

  QString
  get_destination_provider()
  {
    return this->_config.destination_provider_name.c_str();
  }

  QString
  get_effect()
  {
    return this->_config.effect.c_str();
  }

  void
  set_effect(const QString& e)
  {
    this->_config.effect = e.toStdString();
  }

  void
  set_off_x(int x)
  {
    this->_config.off_x = x;
    emit this->off_x_changed(this->_config.off_x);
  }

  int
  get_off_x()
  {
    return this->_config.off_x;
  }

  void
  set_off_y(int y)
  {
    this->_config.off_y = y;
    emit this->off_y_changed(this->_config.off_y);
  }

  int
  get_off_y()
  {
    return this->_config.off_y;
  }

private:
  QStringList
  make_QStringList_from_provider(const std::vector<const char*>& lookup)

  {
    QStringList list;
    list.reserve(lookup.size());

    for (const auto* name : lookup)
      list.push_back(name);

    return list;
  }

  pipeline_config
  set_defaults()
  {
    pipeline_config config;

    config.source_provider_name = config_t::DEFAULT_SOURCE_PROVIDER;
    config.destination_provider_name = config_t::DEFAULT_DESTINATION_PROVIDER;
    config.effect = DEFAULT_EFFECT;
    config.off_x = 0;
    config.off_y = 0;

    return config;
  }

signals:

  void
  off_x_changed(int);

  void
  off_y_changed(int);

public slots:

  void
  apply()
  {
    global_config.add_step(this->_config);
  }

private:
  const QStringList _provider_list;
  const QStringList _effects_list;
  pipeline_config _config;
};
} // namespace gui

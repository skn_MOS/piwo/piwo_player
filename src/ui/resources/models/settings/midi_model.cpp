#include "midi_model.hpp"

#include <algorithm>

#include <spdlog/spdlog.h>

#include "qt_ui_iface.hpp"

namespace gui
{
midi_model::midi_model(QObject* parent)
  : QObject(parent)
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_midi_connection_status_changed,
                   this,
                   &midi_model::on_midi_connection_state_changed);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_midi_list_changed,
                   this,
                   &midi_model::on_midi_list_changed);

  global_config.refresh_midi_list();
}

} // namespace gui

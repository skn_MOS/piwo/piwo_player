#include "audio_model.hpp"

#include <algorithm>

#include <spdlog/spdlog.h>

#include "qt_ui_iface.hpp"

namespace gui
{
audio_model::audio_model(QObject* parent)
  : QObject(parent)
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_audio_in_list_changed,
                   this,
                   &audio_model::on_audio_in_list_changed);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_audio_out_list_changed,
                   this,
                   &audio_model::on_audio_out_list_changed);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_audio_out_connection_status_changed,
                   this,
                   &audio_model::on_audio_out_connection_state_changed);

  global_config.refresh_audio_out_list();
}

} // namespace gui

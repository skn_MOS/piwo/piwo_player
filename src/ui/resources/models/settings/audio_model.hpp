#pragma once

#include "config.hpp"
#include <QObject>
#include <QString>

namespace gui
{
constexpr int AUDIO_INVALID_HANDLE = 0;
class audio_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QStringList audio_in_list READ get_audio_in_devices NOTIFY
               audio_in_list_changed)
  Q_PROPERTY(QStringList audio_out_list READ get_audio_out_devices NOTIFY
               audio_out_list_changed)
  Q_PROPERTY(int audio_in READ get_audio_in WRITE set_audio_in)
  Q_PROPERTY(int audio_out READ get_audio_out WRITE set_audio_out)
  Q_PROPERTY(
    bool in_connected READ get_audio_in_connected NOTIFY audio_in_connected)
  Q_PROPERTY(
    bool out_connected READ get_audio_out_connected NOTIFY audio_out_connected)

public:
  explicit audio_model(QObject* parent = nullptr);

  ~audio_model() = default;

  bool
  get_audio_in_connected()
  {
    return this->_audio_in_connected;
  }

  bool
  get_audio_out_connected()
  {
    return this->_audio_out_connected;
  }
  int
  get_audio_in()
  {
    return this->_audio_in;
  }

  int
  get_audio_out()
  {
    return this->_audio_out;
  }

  void
  set_audio_out(int handle)
  {
    if (this->_audio_out_connected)
      return;
    this->_audio_out = handle;
  }

  void
  set_audio_in(int handle)
  {
    if (this->_audio_in_connected)
      return;
    this->_audio_in = handle;
  }

  QStringList
  get_audio_out_devices()
  {
    return this->_audio_out_list;
  }

  QStringList
  get_audio_in_devices()
  {
    return this->_audio_in_list;
  }

signals:
  void
  audio_in_list_changed(const QStringList& providers);
  void
  audio_out_list_changed(const QStringList& providers);
  void
  audio_in_connected(bool);
  void
  audio_out_connected(bool);

public slots:

  void
  audio_refresh()
  {
    if (this->_audio_out_connected)
      return;
    global_config.refresh_audio_out_list();
  }

  void
  audio_in_connect()
  {
  }

  void
  audio_out_connect()
  {
    if (this->_audio_out == AUDIO_INVALID_HANDLE)
      return;

    auto [lck, playlist] = global_config.acquire_playlist();
    playlist.connect_audio(this->_audio_out);
  }

  void
  audio_out_disconnect()
  {
    if (this->_audio_out == AUDIO_INVALID_HANDLE)
      return;
    auto [lck, playlist] = global_config.acquire_playlist();
    playlist.disconnect_audio();
  }

  void
  on_audio_in_list_changed(const audio_list& devices)
  {
    this->_audio_in_list.clear();
    for (const auto& dev : devices)
    {
      this->_audio_in_list.push_back(dev.c_str());
    }

    emit this->audio_out_list_changed(this->_audio_in_list);
  }

  void
  on_audio_out_list_changed(const audio_list& devices)
  {
    this->_audio_out_list.clear();
    for (const auto& dev : devices)
    {
      this->_audio_out_list.push_back(dev.c_str());
    }
    emit this->audio_out_list_changed(this->_audio_out_list);
  }

  void
  on_audio_out_connection_state_changed(bool state)
  {
    if (this->_audio_out == AUDIO_INVALID_HANDLE)
      return;

    this->_audio_out_connected = state;
    emit audio_out_connected(this->_audio_out_connected);
  }

private:
  QStringList _audio_in_list;
  QStringList _audio_out_list;

  int _audio_in = AUDIO_INVALID_HANDLE, _audio_out = AUDIO_INVALID_HANDLE;

  bool _audio_in_connected = false, _audio_out_connected = false;
};
} // namespace gui

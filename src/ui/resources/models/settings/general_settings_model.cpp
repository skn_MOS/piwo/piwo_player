#include "general_settings_model.hpp"

#include <algorithm>

#include <spdlog/spdlog.h>

#include "qt_ui_iface.hpp"

namespace gui
{

general_settings_model::general_settings_model([[maybe_unused]] QObject* parent)
  : _resolution({})
{
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_config_resolution_changed,
                   this,
                   &general_settings_model::on_resolution_changed);

  auto [mtx, resolution] = global_config.acquire_resolution();

  std::optional narrowed_height = narrow<int>(resolution.height);
  std::optional narrowed_width = narrow<int>(resolution.width);

  if (!narrowed_height.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported resolution height = {}",
                     this->_resolution.height);
    std::terminate();
  }

  if (!narrowed_width.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported resolution width = {}",
                     this->_resolution.width);
    std::terminate();
  }

  this->_resolution = resolution;

  emit resolution_height_changed(narrowed_height.value());
  emit resolution_width_changed(narrowed_width.value());
}

void
general_settings_model::apply()
{
  if (this->_resolution_was_changed)
  {
    global_config.set_resolution(this->_resolution.width,
                                 this->_resolution.height);
  }

  this->_resolution_was_changed = false;
}

void
general_settings_model::cancel()
{
  auto [mtx, resolution] = global_config.acquire_resolution();

  std::optional narrowed_height = narrow<int>(resolution.height);
  std::optional narrowed_width = narrow<int>(resolution.width);

  if (!narrowed_height.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported resolution height = {}",
                     this->_resolution.height);
    return;
  }

  if (!narrowed_width.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported resolution width = {}",
                     this->_resolution.width);
    return;
  }

  this->_resolution = resolution;

  emit resolution_height_changed(narrowed_height.value());
  emit resolution_width_changed(narrowed_width.value());

  this->_resolution_was_changed = false;
}

void
general_settings_model::on_resolution_changed()
{
  if (this->_resolution_was_changed)
  {
    // We do not want to modify the resolution after the user has modified it
    // but not yet approved it
    return;
  }
  auto [mtx, resolution] = global_config.acquire_resolution();

  std::optional narrowed_height = narrow<int>(resolution.height);
  std::optional narrowed_width = narrow<int>(resolution.width);

  if (!narrowed_height.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported resolution height = {}",
                     this->_resolution.height);
    return;
  }

  if (!narrowed_width.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported resolution width = {}",
                     this->_resolution.width);
    return;
  }

  this->_resolution = resolution;

  emit resolution_height_changed(narrowed_height.value());
  emit resolution_width_changed(narrowed_width.value());
}

int
general_settings_model::get_resolution_width()
{
  return static_cast<int>(this->_resolution.width);
}

void
general_settings_model::set_resolution_width(int width)
{
  spdlog::trace("{} {}", __PRETTY_FUNCTION__, width);
  if (width == static_cast<int>(this->_resolution.width))
  {
    return;
  }

  this->_resolution.width =
    std::clamp(width, MIN_RESOLUTION_WIDTH, MAX_RESOLUTION_WIDTH);
  _resolution_was_changed = true;

  emit resolution_width_changed(static_cast<int>(this->_resolution.width));
}

int
general_settings_model::get_resolution_height()
{
  return static_cast<int>(this->_resolution.height);
}

void
general_settings_model::set_resolution_height(int height)
{
  spdlog::trace("{} {}", __PRETTY_FUNCTION__, height);
  if (height == static_cast<int>(this->_resolution.height))
  {
    return;
  }

  this->_resolution.height =
    std::clamp(height, MIN_RESOLUTION_HEIGHT, MAX_RESOLUTION_HEIGHT);
  _resolution_was_changed = true;
  emit resolution_height_changed(static_cast<int>(this->_resolution.height));
}

} // namespace gui

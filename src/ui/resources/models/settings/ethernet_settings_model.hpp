#pragma once

#include "config.hpp"
#include "network.hpp"
#include <QObject>
namespace gui
{

class ethernet_settings_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool eth_connected READ get_eth_connected WRITE set_eth_connected
               NOTIFY eth_connection_state_changed)
  Q_PROPERTY(int tx_id READ get_tx_id WRITE set_tx_id NOTIFY tx_id_changed)
  Q_PROPERTY(
    int radio_id READ get_radio_id WRITE set_radio_id NOTIFY radio_id_changed)
  Q_PROPERTY(
    int interval READ get_interval WRITE set_interval NOTIFY interval_changed)

  Q_PROPERTY(QString default_ip MEMBER _default_ip CONSTANT)

  Q_PROPERTY(QString default_port_tcp MEMBER _default_port_tcp CONSTANT)

  Q_PROPERTY(QString default_port_udp MEMBER _default_port_udp CONSTANT)

public:
  explicit ethernet_settings_model(QObject* parent = nullptr);

  void
  apply();

  void
  cancel();

  Q_INVOKABLE void
  tx_connect(QString ip, QString tcp_port, QString udp_port);

  Q_INVOKABLE void
  tx_disconnect();

  bool
  get_eth_connected();

  void
  set_eth_connected(bool new_state);

  unsigned int
  get_tx_id();

  void
  set_tx_id(int);

  unsigned int
  get_radio_id();

  void
  set_radio_id(int);

  unsigned int
  get_interval();

  void
  set_interval(int);

signals:
  void
  eth_connection_state_changed();

  void
  tx_id_changed(int);

  void
  radio_id_changed(int);

  void
  interval_changed(unsigned int);

public slots:
  void
  on_eth_status_changed(bool new_state);

  void
  on_rssi_config_changed(rssi_config config);

private:
  rssi_config _rssi_config;
  bool _eth_connection_state = false;
  bool _rssi_tx_id_was_changed = false;
  bool _rssi_radio_id_was_changed = false;
  bool _rssi_interval_was_changed = false;

  const QString _default_ip;
  const QString _default_port_tcp;
  const QString _default_port_udp;
};

} // namespace gui

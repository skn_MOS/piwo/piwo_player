#pragma once

#include <QAbstractItemModel>
#include <QAbstractListModel>

#include "playlist.hpp"

#include <list>
#include <vector>

namespace
{
constexpr int INVALID_ACTIVE_ANIMATION_INDEX = -1;
}

namespace gui
{
class lightshow_playlist_model : public QAbstractListModel
{
  Q_OBJECT

public:
  enum playlist_role
  {
    NAME_ROLE = Qt::UserRole,
    DURATION_ROLE,
    CURRENTLY_PLAYED,
    IS_HIGHLIGHTED,
  };

public:
  explicit lightshow_playlist_model(QObject* parent = nullptr);

  [[nodiscard]] int
  rowCount(const QModelIndex& parent = QModelIndex()) const override;

  [[nodiscard]] QVariant
  data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

  bool
  setData(const QModelIndex& index,
          const QVariant& value,
          int role = Qt::EditRole) override;

  [[nodiscard]] Qt::ItemFlags
  flags(const QModelIndex& index) const override;

  [[nodiscard]] QHash<int, QByteArray>
  roleNames() const override;

  Q_INVOKABLE void
  change_animation_to(int index);

  Q_INVOKABLE void
  change_animation_index_to(int current_index, int requested_index);

  Q_INVOKABLE void
  highlight(int row);

  Q_INVOKABLE void
  highlight_all();

  Q_INVOKABLE void
  dehighlight(int row);

  Q_INVOKABLE void
  dehighlight_all();

  Q_INVOKABLE void
  remove_highlighted_animations();

public slots:
  void
  on_playlist_changed();

  void
  on_animation_changed();

  void
  on_playlist_animation_added(animation_id_t name);

  void
  on_playlist_animation_removed(animation_id_t name);

  void
  on_playlist_animation_index_changed(animation_id_t id);

private:
  int _active_animation = INVALID_ACTIVE_ANIMATION_INDEX;
  std::vector<animation_id_t> _animations_list;
  std::vector<animation_id_t> _highlighted_animations;
};

} // namespace gui

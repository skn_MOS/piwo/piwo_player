#include "lightshow_player_model.hpp"
#include "qt_ui_iface.hpp"

namespace gui
{

lightshow_player_model::lightshow_player_model(QObject* parent)
  : QObject(parent)
  , _image_source("image://piwo7//")
{
  constexpr int frame_incremet_interval = 50;

  this->_framerate_timer_up = std::make_unique<QTimer>(this);
  this->_framerate_timer_up->setInterval(frame_incremet_interval);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_animation_started,
                   this,
                   &lightshow_player_model::on_animation_started);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_animation_paused,
                   this,
                   &lightshow_player_model::on_animation_paused);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_animation_stopped,
                   this,
                   &lightshow_player_model::on_animation_stopped);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_animation_changed,
                   this,
                   &lightshow_player_model::on_animation_changed);

  QObject::connect(_framerate_timer_up.get(),
                   &QTimer::timeout,
                   this,
                   &lightshow_player_model::on_display_refresh);

  emit this->current_frame_changed(this->_current_frame);
  emit this->light_show_running_changed(this->_light_show_running);
}

int
lightshow_player_model::get_animation_frames_count()
{
  return this->_current_animation_frames_count;
}

int
lightshow_player_model::get_current_frame()
{
  return this->_current_frame;
}

bool
lightshow_player_model::get_light_show_running()
{
  return this->_light_show_running;
}

QString
lightshow_player_model::get_image_source()
{
  return this->_image_source;
}

QString
lightshow_player_model::get_playlist_current_animation()
{
  return "image://piwo7//playlist_animation";
}

Q_INVOKABLE void
lightshow_player_model::start_animation()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  global_config.start_animation();
}

Q_INVOKABLE void
lightshow_player_model::pause_animation()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  global_config.pause_animation();
}

Q_INVOKABLE void
lightshow_player_model::stop_animation()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  global_config.stop_animation();
}

Q_INVOKABLE void
lightshow_player_model::next_animation()
{
  auto [lock, playlist] = global_config.acquire_playlist();
  playlist.next();
}

Q_INVOKABLE void
lightshow_player_model::replay_or_prev_animation()
{
  constexpr int replay_or_prev_anim_frame_threshold = 40;
  auto [lock, playlist] = global_config.acquire_playlist();

  if (this->_current_frame < replay_or_prev_anim_frame_threshold)
  {
    playlist.prev();
  }
  else
  {
    if (playlist.set_frame_no(0))
    {
      // Safe narrow cast as this value was successfully set to 0
      this->_current_frame =
        static_cast<int>(playlist.get_current_animation_frame_no());
      emit this->current_frame_changed(this->_current_frame);
    }
  }
}

Q_INVOKABLE void
lightshow_player_model::change_current_frame(int current_frame)
{
  spdlog::trace(__PRETTY_FUNCTION__);

  if (current_frame < 0)
  {
    return;
  }

  auto [lock, playlist] = global_config.acquire_playlist();

  if (playlist.set_frame_no(static_cast<size_t>(current_frame)))
  {
    this->_current_frame = current_frame;
    emit this->current_frame_changed(this->_current_frame);
  }
}

void
lightshow_player_model::on_animation_started()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  this->on_animation_changed();
  this->_light_show_running = true;
  emit this->light_show_running_changed(this->_light_show_running);
  this->_framerate_timer_up->start();
}

void
lightshow_player_model::on_animation_paused()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  this->_light_show_running = false;
  emit this->light_show_running_changed(this->_light_show_running);

  this->_framerate_timer_up->stop();
}

void
lightshow_player_model::on_animation_stopped()
{
  spdlog::trace(__PRETTY_FUNCTION__);
  this->_light_show_running = false;
  emit this->light_show_running_changed(this->_light_show_running);

  this->_current_frame = 0;
  this->_current_animation_frames_count = 1;
  emit this->animation_frames_count_changed(
    this->_current_animation_frames_count);
  emit this->current_frame_changed(this->_current_frame);

  this->_framerate_timer_up->stop();
}

void
lightshow_player_model::on_animation_changed()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  auto [playlist_lock, playlist] = global_config.acquire_playlist();
  std::optional current_anim_opt = playlist.get_current_animation();

  if (!current_anim_opt.has_value())
  {
    // TODO(all) Figure out what should happen here.
    // Maybe display should be cleared.
    this->_current_frame = 0;
    this->_current_animation_frames_count = 1;
    emit this->animation_frames_count_changed(
      this->_current_animation_frames_count);
    emit this->current_frame_changed(this->_current_frame);
    return;
  }

  auto current_anim = current_anim_opt.value();

  // TODO(all) 8 bytes frame_count should be supported by UI
  // Consider Integer64

  std::optional narrowed_current_anim_frames_count =
    narrow<int>(current_anim.frame_count());

  if (!narrowed_current_anim_frames_count.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported animation frame cout = {}",
                     current_anim.frame_count());
    return;
  }

  this->_current_animation_frames_count =
    narrowed_current_anim_frames_count.value();

  this->_image_source = "image://piwo7/";

  emit this->animation_frames_count_changed(
    this->_current_animation_frames_count);
  emit this->image_source_changed(this->_image_source);
}

void
lightshow_player_model::on_display_refresh()
{
  auto [lock, playlist] = global_config.acquire_playlist();

  std::optional narrowed_current_anim_frame =
    narrow<int>(playlist.get_current_animation_frame_no());

  if (!narrowed_current_anim_frame.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported animation frame number = {}",
                     playlist.get_current_animation_frame_no());
    return;
  }

  this->_current_frame = narrowed_current_anim_frame.value();
  emit this->current_frame_changed(this->_current_frame);
}

void
lightshow_player_model::volume_changed(int volume)
{
  auto [playlist_lock, playlist] = global_config.acquire_playlist();

  constexpr int max_volume = 100;

  if (volume > max_volume || volume < 0)
    return;

  const float new_volume = (float)volume / max_volume;

  playlist.set_sound_volume(new_volume);
}
} // namespace gui

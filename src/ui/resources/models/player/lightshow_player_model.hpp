#pragma once

#include <QObject>
#include <QString>
#include <QTimer>
#include <memory>

#include "common_types.hpp"
#include "config.hpp"

namespace gui
{
class lightshow_player_model : public QObject
{
  Q_OBJECT
  Q_PROPERTY(int animation_frames_count READ get_animation_frames_count NOTIFY
               animation_frames_count_changed)
  Q_PROPERTY(
    int current_frame READ get_current_frame NOTIFY current_frame_changed)
  Q_PROPERTY(bool light_show_running READ get_light_show_running NOTIFY
               light_show_running_changed)
  Q_PROPERTY(
    QString image_source READ get_image_source NOTIFY image_source_changed)

  Q_PROPERTY(QString playlist_current_anim READ get_playlist_current_animation
               NOTIFY playlist_current_animation)

public:
  explicit lightshow_player_model(QObject* parent = nullptr);

  int
  get_resolution_width();

  void
  set_resolution_width(int width);

  int
  get_resolution_height();

  void
  set_resolution_height(int height);

  int
  get_animation_frames_count();

  int
  get_current_frame();

  bool
  get_light_show_running();

  QString
  get_image_source();

  QString
  get_playlist_current_animation();

  Q_INVOKABLE void
  start_animation();

  Q_INVOKABLE void
  pause_animation();

  Q_INVOKABLE void
  stop_animation();

  Q_INVOKABLE void
  next_animation();

  Q_INVOKABLE void
  replay_or_prev_animation();

  Q_INVOKABLE void
  change_current_frame(int current_frame);

  Q_INVOKABLE void
  volume_changed(int volume);

public slots:
  void
  on_animation_started();

  void
  on_animation_paused();

  void
  on_animation_stopped();

  void
  on_animation_changed();

  void
  on_display_refresh();

signals:
  void
  animation_frames_count_changed(int frames_count);

  void
  current_frame_changed(int current_frame);

  void
  light_show_running_changed(bool light_show_running);

  void
  image_source_changed(QString current_frame);

  void
  playlist_current_animation(QString current_frame);

private:
  ures_t _res{ .width = 0, .height = 0 };
  int _current_animation_frames_count = 0;
  int _current_frame = 0;
  bool _light_show_running = false;
  QString _image_source;

  std::unique_ptr<QTimer> _framerate_timer_up;
};
} // namespace gui

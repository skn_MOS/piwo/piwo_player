#include "lightshow_playlist_model.hpp"

#include "config.hpp"
#include "iterator.hpp"
#include "qt_ui_iface.hpp"

#include <piwo/protodef.h>

namespace gui
{

lightshow_playlist_model::lightshow_playlist_model(QObject* parent)
  : QAbstractListModel(parent)
{
  spdlog::trace(__PRETTY_FUNCTION__);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_playlist_changed,
                   this,
                   &lightshow_playlist_model::on_playlist_changed);

  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_animation_changed,
                   this,
                   &lightshow_playlist_model::on_animation_changed);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_playlist_animation_added,
                   this,
                   &lightshow_playlist_model::on_playlist_animation_added);
  QObject::connect(&qt_ui_if,
                   &qt_ui_iface::signal_playlist_animation_removed,
                   this,
                   &lightshow_playlist_model::on_playlist_animation_removed);
  QObject::connect(
    &qt_ui_if,
    &qt_ui_iface::signal_playlist_animation_index_changed,
    this,
    &lightshow_playlist_model::on_playlist_animation_index_changed);
}

int
lightshow_playlist_model::rowCount(const QModelIndex& parent) const
{
  if (parent.isValid())
    return 0;

  std::optional narrowed_row = narrow<int>(this->_animations_list.size());
  if (!narrowed_row.has_value()) [[unlikely]]
  {
    spdlog::error("Unsupported playlist size = {}",
                  this->_animations_list.size());
    return 0;
  }

  return narrowed_row.value();
}

QVariant
lightshow_playlist_model::data(const QModelIndex& index, int role) const
{
  if (!index.isValid())
    return QVariant();

  std::optional uindex = narrow<size_t>(index.row());
  if (!uindex.has_value() || uindex.value() >= this->_animations_list.size())
    [[unlikely]]
  {
    spdlog::warn("Got invalid row == {}", index.row());
    return QVariant();
  }

  if (role == IS_HIGHLIGHTED)
  {
    return std::find(this->_highlighted_animations.begin(),
                     this->_highlighted_animations.end(),
                     this->_animations_list[uindex.value()]) !=
           this->_highlighted_animations.end();
  }

  auto [playlist_lock, playlist] = global_config.acquire_playlist();
  auto [animations_lock, animations] = playlist.acquire_animations();

  if (animations.size() <= uindex.value())
  {
    spdlog::warn("Got invalid index == {}", uindex.value());
    return QVariant();
  }

  std::string animation_path = animations[index.row()].name;

  auto index_of_filename = animation_path.find_last_of('/');
  if (index_of_filename == std::string::npos)
  {
    index_of_filename = animation_path.find_last_of('\\');
    // No need to check if it is npos
    // It is incremented in next step so is going to be 0 anyway
  }
  auto index_of_ext = animation_path.find_last_of('.');

  std::string animation_name;

  if (index_of_ext != std::string::npos)
  {
    // Increment index of filename to get rid of '/' or '\'
    // Decrement index of ext to get rid of '.'
    animation_name.append(animation_path.substr(
      index_of_filename + 1, index_of_ext - index_of_filename - 1));
  }
  else
  {
    // Increment index of filename to get rid of '/' or '\'
    animation_name.append(
      animation_path.substr(index_of_filename + 1, index_of_ext));
  }

  switch (role)
  {
    case NAME_ROLE:
      return QString::fromStdString(animation_name);
    case DURATION_ROLE:
      return static_cast<qulonglong>(animations[index.row()].anim.total_time());
    case CURRENTLY_PLAYED:
      return index.row() == this->_active_animation;
    default:
      return QVariant();
  }

  return QVariant();
}

bool
lightshow_playlist_model::setData([[maybe_unused]] const QModelIndex& index,
                                  [[maybe_unused]] const QVariant& value,
                                  [[maybe_unused]] int role)
{
  // Not needed now. Implement when needed
  return false;
}

Qt::ItemFlags
lightshow_playlist_model::flags([[maybe_unused]] const QModelIndex& index) const
{
  return Qt::NoItemFlags;
}

QHash<int, QByteArray>
lightshow_playlist_model::roleNames() const
{
  QHash<int, QByteArray> names;
  names[NAME_ROLE] = "name";
  names[DURATION_ROLE] = "duration";
  names[CURRENTLY_PLAYED] = "is_currently_played";
  names[IS_HIGHLIGHTED] = "is_highlighted";
  return names;
}

void
lightshow_playlist_model::on_playlist_changed()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  this->beginResetModel();
  this->_animations_list.clear();
  this->_highlighted_animations.clear();
  {
    auto [playlist_lock, playlist] = global_config.acquire_playlist();
    auto [animations_lock, animations] = playlist.acquire_animations();

    std::optional narrowed_playlist_size = narrow<int>(playlist.size());
    if (!narrowed_playlist_size.has_value()) [[unlikely]]
    {
      spdlog::critical("Unsupported playlist size = {}", playlist.size());
      return;
    }

    this->_active_animation = INVALID_ACTIVE_ANIMATION_INDEX;

    this->_animations_list.reserve(playlist.size());
    this->_highlighted_animations.reserve(playlist.size());

    for (size_t row = 0; row < playlist.size(); row++)
    {
      this->_animations_list.push_back(animations[row].id);
    }
  }

  this->endResetModel();
}

void
lightshow_playlist_model::on_playlist_animation_added(animation_id_t id)
{
  spdlog::trace("{} id = {}", __PRETTY_FUNCTION__, id);

  std::optional<size_t> index_opt;
  {
    auto [playlist_lock, playlist] = global_config.acquire_playlist();

    index_opt = playlist.get_animation_index(id);
  }
  if (!index_opt.has_value())
  {
    spdlog::warn("Got invalid id == {}", id);
    return;
  }

  auto index = index_opt.value();

  std::optional narrowed_index = narrow<int>(index);
  if (!narrowed_index.has_value()) [[unlikely]]
  {
    spdlog::error("Unsupported animation index = {}", index);
    return;
  }

  this->beginInsertRows(
    QModelIndex(), narrowed_index.value(), narrowed_index.value());
  this->_animations_list.insert(::next(this->_animations_list.begin(), index),
                                id);
  this->endInsertRows();
}

void
lightshow_playlist_model::on_playlist_animation_removed(animation_id_t id)
{
  spdlog::trace("{} id = {}", __PRETTY_FUNCTION__, id);

  if (auto it = std::find(this->_highlighted_animations.begin(),
                          this->_highlighted_animations.end(),
                          id);
      it != this->_highlighted_animations.end())
  {
    this->_highlighted_animations.erase(it);
  }

  for (int index = 0; index < static_cast<int>(this->_animations_list.size());
       index++)
  {
    if (this->_animations_list[index] == id)
    {
      this->beginRemoveRows(QModelIndex(), index, index);
      this->_animations_list.erase(
        ::next(this->_animations_list.begin(), index));
      this->endRemoveRows();
      break;
    }
  }
}

void
lightshow_playlist_model::on_playlist_animation_index_changed(animation_id_t id)
{
  spdlog::trace("{} id = {}", __PRETTY_FUNCTION__, id);

  std::optional<size_t> new_index_opt;
  {
    auto [playlist_lock, playlist] = global_config.acquire_playlist();

    new_index_opt = playlist.get_animation_index(id);
  }
  if (!new_index_opt.has_value())
  {
    spdlog::warn("Got invalid animation id == {}", id);
    return;
  }

  std::optional narrowed_anim_index = narrow<int>(new_index_opt.value());
  if (!narrowed_anim_index.has_value()) [[unlikely]]
  {
    spdlog::critical("Unsupported animation index = {}", new_index_opt.value());
    return;
  }

  for (int index = 0; index < static_cast<int>(this->_animations_list.size());
       index++)
  {
    if (this->_animations_list[index] == id)
    {
      auto new_index = narrowed_anim_index.value();
      this->beginRemoveRows(QModelIndex(), index, index);
      this->_animations_list.erase(this->_animations_list.begin() + index);
      this->endRemoveRows();

      this->beginInsertRows(QModelIndex(), new_index, new_index);
      this->_animations_list.insert(
        ::next(this->_animations_list.begin(), new_index), id);
      this->endInsertRows();
      break;
    }
  }
}

void
lightshow_playlist_model::on_animation_changed()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  auto previously_active_animation_index = this->_active_animation;

  auto [lock, playlist] = global_config.acquire_playlist();

  std::optional current_animation_index_opt =
    playlist.get_current_animation_index();
  if (current_animation_index_opt.has_value())
  {
    auto current_animation_index = current_animation_index_opt.value();
    std::optional narrowed_active_anim_index =
      narrow<int>(current_animation_index);
    if (!narrowed_active_anim_index.has_value()) [[unlikely]]
    {
      spdlog::critical("Unsupported animation index = {}",
                       current_animation_index);
      return;
    }
    this->_active_animation = narrowed_active_anim_index.value();
    this->dehighlight(narrowed_active_anim_index.value());
  }
  else
  {
    this->_active_animation = INVALID_ACTIVE_ANIMATION_INDEX;
  }

  if (previously_active_animation_index >= 0)
  {
    emit this->dataChanged(this->index(previously_active_animation_index, 0),
                           this->index(previously_active_animation_index, 0),
                           { CURRENTLY_PLAYED });
  }

  emit this->dataChanged(this->index(this->_active_animation, 0),
                         this->index(this->_active_animation, 0),
                         { CURRENTLY_PLAYED });
}

void
lightshow_playlist_model::change_animation_to(int index)
{
  std::optional uindex = narrow<size_t>(index);

  if (!uindex.has_value()) [[unlikely]]
  {
    spdlog::warn("Got invalid size_t index == {}. Ignoring.", index);
    return;
  }

  global_config.change_animation_to(uindex.value());
}

void
lightshow_playlist_model::change_animation_index_to(int current_index,
                                                    int requested_index)
{
  spdlog::trace(
    "{}, ({}, {})", __PRETTY_FUNCTION__, current_index, requested_index);
  std::optional ucurrent_index = narrow<size_t>(current_index);
  std::optional urequested_index = narrow<size_t>(requested_index);

  if (!ucurrent_index.has_value()) [[unlikely]]
  {
    spdlog::warn("Got invalid size_t index == {}. Ignoring.", current_index);
    return;
  }

  if (!urequested_index.has_value()) [[unlikely]]
  {
    spdlog::warn("Got invalid size_t index == {}. Ignoring.", requested_index);
    return;
  }

  auto [playlist_lock, playlist] = global_config.acquire_playlist();

  playlist.change_animation_index(ucurrent_index.value(),
                                  urequested_index.value());
}

Q_INVOKABLE void
lightshow_playlist_model::highlight(int row)
{
  spdlog::trace("{} row = {}", __PRETTY_FUNCTION__, row);

  if (this->_active_animation == row)
  {
    return;
  }

  std::optional uindex = narrow<size_t>(row);
  if (!uindex.has_value()) [[unlikely]]
  {
    spdlog::warn("Got invalid size_t animation index == {}. Ignoring.", row);
    return;
  }

  this->_highlighted_animations.emplace_back(
    this->_animations_list[uindex.value()]);

  emit this->dataChanged(
    this->index(row, 0), this->index(row, 0), { IS_HIGHLIGHTED });
}

Q_INVOKABLE void
lightshow_playlist_model::highlight_all()
{
  this->_highlighted_animations.clear();

  for (auto id : this->_animations_list)
  {
    this->_highlighted_animations.emplace_back(id);
  }

  emit this->dataChanged(
    this->index(0, 0),
    this->index(static_cast<int>(this->_animations_list.size()), 0),
    { IS_HIGHLIGHTED });
}

Q_INVOKABLE void
lightshow_playlist_model::dehighlight(int row)
{
  spdlog::trace("{} row = {}", __PRETTY_FUNCTION__, row);

  std::optional uindex = narrow<size_t>(row);
  if (!uindex.has_value())
  {
    spdlog::warn("Got invalid size_t animation index == {}. Ignoring.", row);
    return;
  }

  if (this->_animations_list.size() <= uindex.value())
  {
    spdlog::warn("Got invalid animation index == {}. Ignoring.", row);
    return;
  }

  if (auto it = std::find(this->_highlighted_animations.begin(),
                          this->_highlighted_animations.end(),
                          this->_animations_list[uindex.value()]);
      it != this->_highlighted_animations.end())
  {
    this->_highlighted_animations.erase(it);
    emit this->dataChanged(
      this->index(row, 0), this->index(row, 0), { IS_HIGHLIGHTED });
  }
}

Q_INVOKABLE void
lightshow_playlist_model::dehighlight_all()
{
  spdlog::trace(__PRETTY_FUNCTION__);

  this->_highlighted_animations.clear();

  auto data_changed_range =
    (this->_animations_list.size() > 0) ? this->_animations_list.size() - 1 : 0;

  emit this->dataChanged(this->index(0, 0),
                         this->index(static_cast<int>(data_changed_range), 0),
                         { IS_HIGHLIGHTED });
}

Q_INVOKABLE void
lightshow_playlist_model::remove_highlighted_animations()
{
  auto [playlist_lock, playlist] = global_config.acquire_playlist();

  auto highlighted_animations_copy = this->_highlighted_animations;

  for (size_t id : highlighted_animations_copy)
  {
    playlist.remove_animation_by_id(id);
  }
}

} // namespace gui

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import footer_model 1.0
import application_state_enum 1.0

import "common"

Rectangle {
  color: palette.light

  FooterModel {
    id: footer_model_instance
    onApplication_state_changed:
    {
      switch(application_state) {
        case ApplicationStateEnum.IDLE:
          rssi_switch.checked = false;
          lightshow_switch.checked = false;
        break;
        case ApplicationStateEnum.DISPLAY_AUTO_CONFIG:
          rssi_switch.checked = false;
          lightshow_switch.checked = false;
        break;
        case ApplicationStateEnum.LIGHTSHOW:
          rssi_switch.checked = false;
          lightshow_switch.checked = true;
        break;
        case ApplicationStateEnum.RSSI:
          rssi_switch.checked = true;
          lightshow_switch.checked = false;
        break;
        default:
          rssi_switch.checked = false;
          lightshow_switch.checked = false;
      }

      lightshow_switch.checkable = false;
      rssi_switch.checkable = false;
    }
  }

  RowLayout {
    anchors.fill: parent
    Text {
      Layout.alignment: Qt.AlignLeft
      Layout.leftMargin: 10
      verticalAlignment: Text.AlignVCenter
      Layout.fillHeight: true
      color: palette.text
      font.pointSize: 10
      font.family: "Ubuntu"
      text: {
        if(footer_model)
        {
          switch(footer_model.application_state) {
            case ApplicationStateEnum.IDLE:
              "Ready";
            break;
            case ApplicationStateEnum.DISPLAY_AUTO_CONFIG:
              "Display auto configuration in progress";
            break;
            case ApplicationStateEnum.LIGHTSHOW:
              "Lightshow is running";
            break;
            case ApplicationStateEnum.RSSI:
              "RSSI is running";
            break;
            default:
            "Undefined";
          }
        }
        else
        {
          "Undefined";
        }
      }
    }

    Item {
      Layout.fillWidth: true
    }

    Text {
      Layout.alignment: Qt.AlignRight | Qt.AlignRight
      verticalAlignment: Text.AlignVCenter
      Layout.fillHeight: true
      color: palette.text
      font.pointSize: 10
      font.family: "Ubuntu"
      text: {
        if(typeof footer_model != "undefined" &&
            footer_model &&
            footer_model.application_state == ApplicationStateEnum.LIGHTSHOW) {
          "Lightshow ON";
        }
        else {
          "Lightshow OFF"
        }
      }
    }

    FlatDesignSwitch {
      id: lightshow_switch
      Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
      Layout.preferredHeight: 17
      checkable: false
      onClicked: {
        logger_model.log_trace("Lightshow switch pressed");
        footer_model.enable_disable_lightshow();
      }
    }

    Text {
      Layout.alignment: Qt.AlignRight | Qt.AlignRight
      verticalAlignment: Text.AlignVCenter
      Layout.fillHeight: true
      color: palette.text
      font.pointSize: 10
      font.family: "Ubuntu"
      text: {
        if(typeof footer_model != "undefined" &&
            footer_model &&
            footer_model.application_state == ApplicationStateEnum.RSSI) {
          "RSSI ON";
        }
        else {
          "RSSI OFF"
        }
      }
    }

    FlatDesignSwitch {
      id: rssi_switch
      Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
      Layout.preferredHeight: 17
      checkable: false
      onClicked: {
        logger_model.log_trace("RSSI switch pressed");
        footer_model.enable_disable_rssi();
      }
    }
  }
}

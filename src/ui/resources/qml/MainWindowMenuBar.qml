import QtQuick 2.12
import QtQuick.Controls 2.12

import log_level_enum 1.0

MenuBar {
  Menu {
    title: qsTr("File")
    Menu {
      title: qsTr("Import")
      Action {
        text: qsTr("Modules basic info");
        onTriggered: importModulesInfoFileDialog.open();
      }
      Action {
        text: qsTr("Configuration");
        onTriggered: importDisplayConfigurationFileDialog.open();
      }
      Action {
        text: qsTr("Animation(s)");
        onTriggered: importAnimationFileDialog.open();
      }
    }
    Menu {
      title: qsTr("Export")
      Action {
        text: qsTr("Configuration");
        onTriggered: exportDisplayConfigurationFileDialog.open();
      }
    }
    MenuSeparator { }
    Action {
      text: qsTr("Quit")
      onTriggered: Qt.callLater(Qt.quit);
    }
  }
  Menu {
    title: qsTr("Preferences")
    Menu {
      title: qsTr("Theme")
      Action {
        text: qsTr("Light mode")
        onTriggered: {
          mainWindow.switchToLightMode();
          logger_model.log_trace("Light mode enabled");
        }
      }
      Action {
        text: qsTr("Dark mode")
        onTriggered: {
          mainWindow.switchToDarkMode();
          logger_model.log_trace("Dark mode enabled");
        }
      }
    }
    Menu {
      title: qsTr("Logger")
      Menu {
        title: qsTr("Logging level")
        Action {
          text: qsTr("Trace");
          onTriggered: {
            if(logger_model) {
              logger_model.logging_level = LogLevelEnum.TRACE;
            }
          }
          checkable: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.TRACE;
            }
            else {
              false;
            }
          }
          checked: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.TRACE;
            }
            else {
              false;
            }
          }
        }
        Action {
          text: qsTr("Debug");
          onTriggered: {
            if(logger_model) {
              logger_model.logging_level = LogLevelEnum.DEBUG;
            }
          }
          checkable: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.DEBUG;
            }
            else {
              false;
            }
          }
          checked: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.DEBUG;
            }
            else {
              false;
            }
          }
        }
        Action {
          text: qsTr("Info");
          onTriggered: {
            if(logger_model) {
              logger_model.logging_level = LogLevelEnum.INFO;
            }
          }
          checkable: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.INFO;
            }
            else {
              false;
            }
          }
          checked: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.INFO;
            }
            else {
              false;
            }
          }
        }
        Action {
          text: qsTr("Warn");
          onTriggered: {
            if(logger_model) {
              logger_model.logging_level = LogLevelEnum.WARN;
            }
          }
          checkable: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.WARN;
            }
            else {
              false;
            }
          }
          checked: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.WARN;
            }
            else {
              false;
            }
          }
        }
        Action {
          text: qsTr("Error");
          onTriggered: {
            if(logger_model) {
              logger_model.logging_level = LogLevelEnum.ERR;
            }
          }
          checkable: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.ERR;
            }
            else {
              false;
            }
          }
          checked: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.ERR;
            }
            else {
              false;
            }
          }
        }
        Action {
          text: qsTr("Critical");
          onTriggered: {
            if(logger_model) {
              logger_model.logging_level = LogLevelEnum.CRITICAL;
            }
          }
          checkable: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.CRITICAL;
            }
            else {
              false;
            }
          }
          checked: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.CRITICAL;
            }
            else {
              false;
            }
          }
        }
        Action {
          text: qsTr("Off");
          onTriggered: {
            if(logger_model) {
              logger_model.logging_level = LogLevelEnum.OFF;
            }
          }
          checkable: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.OFF;
            }
            else {
              false;
            }
          }
          checked: {
            if(logger_model) {
              logger_model.logging_level == LogLevelEnum.OFF;
            }
            else {
              false;
            }
          }
        }
      }
    }
  }
  Menu {
    title: qsTr("Help")
    Action { text: qsTr("About") }
  }
}

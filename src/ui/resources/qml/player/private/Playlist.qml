import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

Rectangle {
    property variant internal_model

    color: palette.dark
    Layout.preferredWidth: 350
    Layout.fillHeight: true
    ColumnLayout {
        anchors.fill: parent
        Rectangle {
            Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
            Layout.topMargin: 25
            Layout.bottomMargin: 25
            Layout.leftMargin: 25
            Layout.rightMargin: 25
            Layout.minimumHeight: 100
            Layout.fillHeight: true
            Layout.fillWidth: true

            ColumnLayout {
                anchors.fill: parent
                spacing: {0, 0}
                Rectangle {
                    Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
                    Layout.preferredHeight: 50
                    Layout.fillWidth: true
                    color: palette.button

                    Text {
                    anchors.fill: parent
                    font.family: "Ubuntu"
                    text: "Lightshow playlist"
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: 18
                    color: palette.text
                    }
                }
                Rectangle {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 3
                    color: palette.mid
                }
                RowLayout {
                    Layout.topMargin: 10
                    Layout.leftMargin: 10
                    Layout.fillWidth: true
                    Layout.preferredHeight: 7
                    Text {
                    Layout.fillWidth: true
                    color: palette.text
                    font.pointSize: 9
                    font.family: "Ubuntu"
                    text: "Title"
                    }
                    Text {
                    Layout.preferredWidth: 100
                    color: palette.text
                    font.pointSize: 9
                    font.family: "Ubuntu"
                    text: "Duration"
                    }
                }
                Rectangle {
                    Layout.topMargin: 10
                    Layout.fillWidth: true
                    Layout.preferredHeight: 2
                    color: palette.dark
                }

                Menu {
                    id: playlistContextMenu

                    MenuItem {
                        text: "Remove"
                        onTriggered: {
                            internal_model.remove_highlighted_animations();
                        }
                    }
                }

                ListView {
                    id: playerPlaylistList
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    clip: true
                    model: internal_model
                    focus: true
                    delegate: ColumnLayout {
                        width: playerPlaylistList.width
                        spacing: {0,0}
                        Rectangle {
                            id: playlistTile
                            Binding on Drag.active {
                                value: playlistMouseArea.drag.active
                                delayed: true
                            }
                            Drag.dragType: Drag.Automatic
                            Layout.fillWidth: true
                            Layout.preferredHeight: 25
                            color: {
                                if(model.is_currently_played || model.is_highlighted) {
                                    palette.light
                                }
                                else {
                                    palette.midlight
                                }
                            }
                            opacity: {
                                if(model.is_currently_played) {
                                    1;
                                }
                                else if(model.is_highlighted)
                                {
                                    0.5;
                                }
                                else {
                                    1;
                                }
                            }
                            RowLayout {
                                anchors.fill: parent
                                clip: true

                                Text {
                                    Layout.leftMargin: 10
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                                    verticalAlignment: Text.AlignVCenter
                                    color: palette.text
                                    font.pointSize: 10
                                    font.family: "Ubuntu"
                                    text: model.name
                                }

                                Text {
                                    Layout.preferredWidth: 100
                                    Layout.fillHeight: true
                                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                                    verticalAlignment: Text.AlignVCenter
                                    font.pointSize: 10
                                    font.family: "Ubuntu"
                                    color: palette.text
                                    text: {
                                    let ms = Math.floor(model.duration % 1000)
                                    let s  = Math.floor((model.duration / 1000) % 60)
                                    let m  = Math.floor((model.duration / 1000 / 60))

                                    return m + ":" + s + "." + ms
                                    }
                                }
                            }

                            states: [
                                State {
                                name: "NORM"

                                PropertyChanges {
                                    target: playlistMouseArea
                                    drag.target: undefined
                                }

                                PropertyChanges {
                                    target: playlistTile
                                }
                                },
                                State {
                                name: "DRAG"

                                PropertyChanges {
                                    target: playlistMouseArea
                                    drag.target: drag_data
                                }

                                PropertyChanges {
                                    target: playlistTile
                                }
                                }
                            ]
                            MouseArea {
                                id: playlistMouseArea

                                pressAndHoldInterval: 500
                                acceptedButtons: Qt.LeftButton | Qt.RightButton
                                propagateComposedEvents: true
                                drag.threshold: 0
                                anchors.fill: parent
                                hoverEnabled: true
                                Component.onCompleted: {
                                    playlistTile.state = "NORM";
                                }
                                onPressed: {
                                    if((mouse.modifiers & Qt.ControlModifier) && model.is_highlighted == true) {
                                        internal_model.dehighlight(index);
                                    }
                                    else if(mouse.button & Qt.LeftButton) {
                                        internal_model.highlight(index);
                                    }
                                }
                                onPressAndHold: {
                                    mouse.accepted = false;
                                    parent.grabToImage(function(result) {
                                        parent.Drag.imageSource = result.url;
                                    });
                                    playlistTile.state = "DRAG";
                                }
                                onDoubleClicked: {
                                    internal_model.change_animation_to(index)
                                }
                            }

                            DropArea {
                                id: playlist_drop_area
                                anchors.fill: parent
                                onDropped: {
                                    playlistTile.state = "NORM";
                                    internal_model.change_animation_index_to(drag.source.currentIndex, index);
                                }
                            }

                            Drag.source: PlaylistDragData {
                                id: drag_data
                                currentIndex: index
                            }
                        }
                        Rectangle {
                            Layout.fillWidth: true
                            Layout.preferredHeight: 2
                            color: palette.dark
                        }
                    }
                }
            }
            color: palette.midlight
        }
    }
    MouseArea {
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        anchors.fill: parent
        onPressed: {
            mouse.accepted = false;
            if (mouse.button & Qt.RightButton) {
                playlistContextMenu.popup();
            }
            else if (!(mouse.modifiers & Qt.ControlModifier)) {
                internal_model.dehighlight_all();
            }
        }
    }
}
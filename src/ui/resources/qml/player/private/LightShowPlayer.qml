import QtQuick 2.12
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Shapes 1.15
import QtQuick.Controls.Styles 1.4

ColumnLayout {
  id: lightShowMainLayout

  Shortcut {
    sequence: "Space"
    onActivated: {
      if(lightshow_player_model.light_show_running)
      {
        lightshow_player_model.pause_animation();
      }
      else
      {
        lightshow_player_model.start_animation();
      }
    }
  }

  Window {
    id: fullScreenWindow
    x: 0
    y: 0

    Rectangle {
      id: fullScreenArea
      anchors.fill: parent
      color: "transparent"
    }

    onClosing: {
      lightShowDisplay.state = "NORM";
    }
  }


  RowLayout {
    Rectangle {
      id:lightShowDisplayContainer
      Layout.fillHeight: true
      Layout.fillWidth: true
      color: "transparent"

      Rectangle {
        color: palette.dark
        anchors.fill: parent
        visible: lightShowDisplay.state = "FULL_SCREEN"
        Text {
          anchors.fill: parent
          text: "Full screen enabled"
          font.family: "Ubuntu"
          verticalAlignment: Text.AlignVCenter
          horizontalAlignment: Text.AlignHCenter
          font.pixelSize: 18
          color: palette.text
        }
      }
      Rectangle {
        id: lightShowDisplay
        anchors.fill: parent
        color: palette.dark
        state: "NORM"

        Component.onCompleted: lightShowDisplay.state = "NORM"

        states: [
          State {
            name: "NORM"
            ParentChange { target: lightShowDisplay; parent: lightShowDisplayContainer;}
          },
          State {
            name: "FULL_SCREEN"
            ParentChange { target: lightShowDisplay; parent: fullScreenArea;}
          }
        ]

        ColumnLayout {
          anchors.fill: parent
          Rectangle {
            id: renderArea
            Layout.fillHeight: true
            Layout.fillWidth: true
            color: palette.dark

            Image {
              id: image
              anchors.verticalCenter: parent.verticalCenter
              anchors.horizontalCenter: parent.horizontalCenter
              source: {
                if(typeof lightshow_player_model != "undefined" &&
                   lightshow_player_model &&
                   lightshow_player_model.animation_frames_count > 0) {
                  lightshow_player_model.image_source  + '/' + lightshow_player_model.current_frame;
                }
                else {
                  "";
                }
              }
              sourceSize.width: parent.width
              sourceSize.height: parent.height
            }
          }

          Rectangle {
            id: lightShowProgressBar
            Layout.alignment : Qt.AlignBottom
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            Layout.fillWidth: true
            Layout.preferredHeight: 10
            color: palette.midlight

            Rectangle {
              Popup {
                id: animationFramePreviewPopup
                property int framePreviewNo: 0
                width: 400
                height: 400
                Image {
                  id: framePreviewImage
                  anchors.verticalCenter: parent.verticalCenter
                  anchors.horizontalCenter: parent.horizontalCenter
                  source: {
                    if(typeof lightshow_player_model != "undefined" &&
                       lightshow_player_model &&
                       lightshow_player_model.animation_frames_count > 0) {
                      lightshow_player_model.playlist_current_anim + '/' + animationFramePreviewPopup.framePreviewNo
                    }
                    else {
                      ""
                    }
                  }
                  sourceSize.width: parent.width
                  sourceSize.height: parent.height
                }
              }
              height: parent.height
              width: {
                if(typeof lightshow_player_model != "undefined" &&
                   lightshow_player_model &&
                   lightshow_player_model.animation_frames_count > 0) {
                  var value = parent.width * (lightshow_player_model.current_frame / lightshow_player_model.animation_frames_count)
                  if(parent.width > value) {
                    value;
                  }
                  else {
                    parent.width;
                  }
                }
                else {
                  0
                }
              }
              color: palette.light
            }
            MouseArea {
              anchors.fill: parent
              hoverEnabled: true
              onClicked: {
                var percenage = mouse.x / width;
                var frame = parseInt(lightshow_player_model.animation_frames_count * percenage);
                logger_model.log_trace("Animation progress bar clicked on frame " + frame);
                lightshow_player_model.change_current_frame(frame);
              }
              onEntered: {
                if(lightshow_player_model.animation_frames_count > 0) {
                  animationFramePreviewPopup.x = (animationFramePreviewPopup.width / 2);
                  animationFramePreviewPopup.y = (animationFramePreviewPopup.height + 10);
                  animationFramePreviewPopup.open();
                }
              }
              onPositionChanged: {
                animationFramePreviewPopup.x = mouse.x - (animationFramePreviewPopup.width / 2);
                animationFramePreviewPopup.y = mouse.y - (animationFramePreviewPopup.width + 10);
                var percenage = mouse.x / width;
                var frame = parseInt(lightshow_player_model.animation_frames_count * percenage);
                animationFramePreviewPopup.framePreviewNo = frame;
              }
              onExited: {
                animationFramePreviewPopup.framePreviewNo = 0;
                animationFramePreviewPopup.close();
              }
            }
          }

          RowLayout {
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            Layout.topMargin: 5
            Layout.bottomMargin: 5
            Layout.alignment: Qt.AlignBottom
            Layout.fillWidth: true

            Button {
              ToolTip.visible: hovered
              ToolTip.text: qsTr("Replay / Prev animation");
              indicator: Shape {
                anchors.fill: parent
                ShapePath {
                  startX: 5; startY: 5;
                PathLine { x: lightshowPlayerNextAnimationButton.width - (lightshowPlayerNextAnimationButton.width / 1.4); y: 5}
                  PathLine { x: lightshowPlayerNextAnimationButton.width - (lightshowPlayerNextAnimationButton.width / 1.4); y: lightshowPlayerNextAnimationButton.height - 5}
                  PathLine { x: 5; y: lightshowPlayerNextAnimationButton.height - 5}
                  PathLine { x: 5; y: 5}
                }
                ShapePath {
                  startX: lightshowPlayerNextAnimationButton.width - 5; startY: 5
                  PathLine { x: lightshowPlayerNextAnimationButton.width - (lightshowPlayerNextAnimationButton.width / 1.4); y: (lightshowPlayerNextAnimationButton.height / 2) }
                  PathLine { x: lightshowPlayerNextAnimationButton.width - 5; y: lightshowPlayerNextAnimationButton.height - 5 }
                }

              }
              Layout.preferredHeight: 25
              Layout.preferredWidth: 25

              palette.button: "transparent"
              onClicked: {
                lightshow_player_model.replay_or_prev_animation();
              }
            }

            Button {
              id: lightshowPlayerPlayButton
              visible: {
                if(typeof lightshow_player_model != "undefined" && lightshow_player_model)
                {
                  !lightshow_player_model.light_show_running
                }
                else {
                  false;
                }
              }
              ToolTip.visible: hovered
              ToolTip.text: {
                qsTr("Play animation");
              }
              indicator: Shape {
                anchors.fill: parent
                ShapePath {
                  startX: 5; startY: 5
                  PathLine { x: lightshowPlayerPlayButton.width - 5; y: (lightshowPlayerPlayButton.height / 2) }
                  PathLine { x: 5; y: lightshowPlayerPlayButton.height - 5 }
                }
              }

              palette.button: "transparent"

              Layout.preferredHeight: 30
              Layout.preferredWidth: 30
              onClicked: {
                lightshow_player_model.start_animation();
              }
            }

            Button {
              id: lightshowPlayerPauseButton
              visible: {
                if(typeof lightshow_player_model != "undefined" && lightshow_player_model) {
                  lightshow_player_model.light_show_running
                }
                else {
                  false;
                }
              }
              ToolTip.visible: hovered
              ToolTip.text: {
                qsTr("Pause animation");
              }
              indicator: Shape {
                anchors.fill: parent
                ShapePath {
                  startX: 5; startY: 5
                  PathLine { x: 10; y: 5}
                  PathLine { x: 10; y: lightshowPlayerPauseButton.height - 5}
                  PathLine { x: 5; y: lightshowPlayerPauseButton.height - 5}
                  PathLine { x: 5; y: 5}
                }
                ShapePath {
                  startX: lightshowPlayerPauseButton.width - 5; startY: 5
                  PathLine { x: lightshowPlayerPauseButton.width - 10; y: 5}
                  PathLine { x: lightshowPlayerPauseButton.width - 10; y: lightshowPlayerPauseButton.height - 5}
                  PathLine { x: lightshowPlayerPauseButton.width - 5; y: lightshowPlayerPauseButton.height - 5}
                  PathLine { x: lightshowPlayerPauseButton.width - 5; y: 5}
                }
              }

              palette.button: "transparent"

              Layout.preferredHeight: 30
              Layout.preferredWidth: 30
              onClicked: {
                lightshow_player_model.pause_animation();
              }
            }

            Button {
              id: lightshowPlayerStopButton
              ToolTip.visible: hovered
              ToolTip.text: {
                qsTr("Stop animation");
              }
              indicator: Shape {
                anchors.fill: parent
                ShapePath {
                  startX: 5; startY: 5;
                  PathLine { x: 30; y: 5}
                  PathLine { x: 30; y: lightshowPlayerStopButton.height - 5}
                  PathLine { x: 5; y: lightshowPlayerStopButton.height - 5}
                  PathLine { x: 5; y: 5}
                }
              }

              palette.button: "transparent"

              Layout.preferredHeight: 30
              Layout.preferredWidth: 30
              onClicked: {
                lightshow_player_model.stop_animation();
              }
            }

            Button {
              id: lightshowPlayerNextAnimationButton
              ToolTip.text: qsTr("Next animation")
              ToolTip.visible: hovered

              indicator: Shape {
                anchors.fill: parent
                ShapePath {
                  startX: 5; startY: 5
                  PathLine { x: lightshowPlayerNextAnimationButton.width / 1.4; y: (lightshowPlayerNextAnimationButton.height / 2) }
                  PathLine { x: 5; y: lightshowPlayerNextAnimationButton.height - 5 }
                }
                ShapePath {
                  startX: lightshowPlayerNextAnimationButton.width / 1.4; startY: 5
                  PathLine { x: lightshowPlayerNextAnimationButton.width - 5; y: 5}
                  PathLine { x: lightshowPlayerNextAnimationButton.width - 5; y: lightshowPlayerNextAnimationButton.height - 5}
                  PathLine { x: lightshowPlayerNextAnimationButton.width / 1.4; y: lightshowPlayerNextAnimationButton.height - 5}
                  PathLine { x: lightshowPlayerNextAnimationButton.width / 1.4; y: 5}
                }
              }

              Layout.preferredHeight: 25
              Layout.preferredWidth: 25

              palette.button: "transparent"
              onClicked: {
                lightshow_player_model.next_animation();
              }
            }

            Item {
              Layout.fillWidth: true
            }

            Slider {
                id: music_volume

                implicitWidth: 200
                implicitHeight: 26

                background: Rectangle {
                    y: (music_volume.height - height) / 2
                    height: 10
                    radius: 2
                    color: "darkgrey"

                    Rectangle {
                        width: music_volume.visualPosition * parent.width
                        height: parent.height
                        color: "dodgerblue"
                        radius: 2
                    }
                }
                from: 0
                value: 50
                to: 100
                onMoved : lightshow_player_model.volume_changed(parseInt(music_volume.value))
            }
            Label {
                text: "Volume: " + parseInt(music_volume.value)
            }

            Button {
              ToolTip.text:{
                if(lightShowDisplay.state == "NORM") {
                    qsTr("Full screen")
                }
                else {
                  qsTr("Exit full screen")
                }
              }

              ToolTip.visible: hovered
              ControllerButtonText {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                fontSize: 12
                text: {
                  if(lightShowDisplay.state == "NORM") {
                      "[ ]";
                  }
                  else {
                    "> <"
                  }
                }
              }
              width:10
              Layout.preferredHeight: 30
              Layout.preferredWidth: 30

              palette.button: "transparent"
              onClicked: {
                if(lightShowDisplay.state == "NORM") {
                  fullScreenWindow.showFullScreen();
                  lightShowDisplay.state = "FULL_SCREEN";
                }
                else {
                  fullScreenWindow.close();
                  lightShowDisplay.state = "NORM";
                }

              }
            }
          }
        }
      }
    }
    Playlist {
      internal_model: lightshow_playlist_model
    }
  }
}

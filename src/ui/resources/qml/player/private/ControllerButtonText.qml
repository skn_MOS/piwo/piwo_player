import QtQuick 2.12

Text {
  property int fontSize: 19
  color: palette.text
  font.pointSize: fontSize
  font.bold: true
  font.family: "C059"
}
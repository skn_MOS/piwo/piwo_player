import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.12

RowLayout {
  property var currentIndex: 0

  Button {
    id: light_show_player_mode_button
    Layout.fillWidth: true
    text: qsTr("Lightshow")
    highlighted: true;
    onPressed: {
      currentIndex = 0;
      highlighted = true;
      light_show_player_mode_button.highlighted = true;
      render_engine_mode_button.highlighted = false;
    }
  }

  Button {
    id: render_engine_mode_button
    Layout.fillWidth: true
    text: qsTr("Render Engine")
    onPressed: {
      currentIndex = 1;
      highlighted = true;
      light_show_player_mode_button.highlighted = false;
      render_engine_mode_button.highlighted = true;
    }
  }
}

import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12
import Qt.labs.qmlmodels 1.0

import general_settings_model 1.0

import "../common"

RowLayout {
  Layout.topMargin: 10
  spacing: 2

  ColumnLayout {
    RowLayout{
    Label {
      text: "Render engine. "
      font.pixelSize: 16
      }
    }

    GridLayout
    {
      Layout.fillWidth: true
      columns: 2

          Label {
            text: "Source: "
          }
          ComboBox {
              width: 500
              model: {
                if(render_engine_model) {
                  render_engine_model.provider_list
                }else{
                  []
                }
              }

              onActivated: render_engine_model.source_provider = currentText

              Component.onCompleted: currentIndex =
                  indexOfValue(render_engine_model.source_provider);

          }

          Label {
            text: "Destination: "
          }
          ComboBox {
              model: {
                if(render_engine_model){
                  render_engine_model.provider_list
                }else{
                  []
                }
              }

              onActivated: render_engine_model.destination_provider = currentText

              Component.onCompleted: currentIndex =
                  indexOfValue(render_engine_model.destination_provider);
            }

          Label {
            text: "X offset: "
          }
          TextField {
            text: {
              if (render_engine_model && typeof render_engine_model.off_x!= "undefined") {
                render_engine_model.off_x.toString();
              }
              else {
                "";
              }
            }

            onEditingFinished: if(render_engine_model) {render_engine_model.off_x = parseInt(text)}
          }


          Label {
            text: "Y offset: "
          }
          TextField {
            text: {
              if (render_engine_model && typeof render_engine_model.off_y != "undefined") {
                render_engine_model.off_y.toString();
              }
              else {
                "";
              }
            }

            onEditingFinished: if(render_engine_model) {render_engine_model.off_y = parseInt(text)}
          }

          Label {
            text: "Effect: "
          }
          ComboBox {
              width: 500
              model: {
                if(render_engine_model) {
                  render_engine_model.effects_list
                } else {
                  []
                }
              }

              onActivated: render_engine_model.effect = currentValue

              Component.onCompleted: currentIndex =
                  indexOfValue(render_engine_model.effect);
          }

      Button {
        Layout.alignment: Qt.AlignRight
        text: qsTr("Append")
        onClicked: {
          logger_model.log_trace("Append to render engine pipeline requested");
          render_engine_model.apply();
        }
      }

    }
  }
}

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

import "../common"

ColumnLayout {
  Layout.maximumWidth: 350
  Layout.topMargin: 10

  RowLayout
  {
    Label {
      text: "Midi offset: "
    }

    TextField {
      id: midi_offset
      Layout.fillWidth: true
      enabled:(midi_model && !midi_model.connected)
      placeholderText: qsTr("Enter offset")
      text: "10"
      validator: IntValidator {bottom: 1; top: 100}
    }
  }

  RowLayout
  {
    ComboBox {
        Layout.minimumWidth: 300
        Layout.fillWidth: true
        enabled:(midi_model && !midi_model.connected)
        model: {
          if(midi_model) {
            midi_model.midi_list
          }else{
            []
          }
        }
        onActivated: midi_model.midi_port = currentIndex
        onPressedChanged: {
          if(pressed) {
            midi_model.midi_refresh();
          }
        }
    }

    Button {
      text: if(midi_model && !midi_model.connected)
            {
              qsTr("Connect")
            }else
            {
              qsTr("Disconnect")
            }
      onClicked: {
        if(midi_model && !midi_model.connected)
        {
          midi_model.midi_connect(parseInt(midi_offset.text));
        }
        else
        {
          midi_model.midi_disconnect();
        }
      }
    }
  }
}

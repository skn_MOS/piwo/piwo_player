import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

Rectangle {
  property variant internal_model
  property int col_count
  color: palette.midlight
  id: table_model_id

  ColumnLayout {
    anchors.fill: parent
    spacing: {0, 0}

    Rectangle {
      Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
      Layout.preferredHeight: 50
      Layout.fillWidth: true
      color: palette.button

      Text {
        anchors.fill: parent
        font.family: "Ubuntu"
        text: "Configured pipeline steps"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 18
        color: palette.text
      }
    }

    Menu {
      id: contextMenu

      MenuItem {
        text: "Remove"
        onTriggered: {
          internal_model.remove_highlighted();
        }
      }
      MenuItem {
        text: "Refresh"
        onTriggered: {
          internal_model.refresh_highlighted();
        }
      }
      MenuItem {
        text: "Enable"
        onTriggered: {
          internal_model.change_state_of_highlighted(true);
        }
      }
      MenuItem {
        text: "Disable"
        onTriggered: {
          internal_model.change_state_of_highlighted(false);
        }
      }
    }

    Row {
        id: columnsHeader
        y: 0
        z: 2
        Repeater {
            model: col_count
            Label {
                width: table_model_id.width/col_count
                height: 35
                text: if (internal_model){internal_model.headerData(modelData, Qt.Horizontal)} else {""}
                color: "white"
                font.pixelSize: 15
                verticalAlignment: Text.AlignVCenter
                background: Rectangle { color: palette.dark }
            }
        }
    }

    TableView {
      id: pipelineTable
      model: internal_model
      Layout.fillHeight: true
      Layout.fillWidth: true
      property var tileSize: 100
      columnSpacing: 1
      rowSpacing: 1
      clip: true
      onWidthChanged: pipelineTable.tileSize = pipelineTable.width/col_count

      delegate: ColumnLayout {
        width: pipelineTable.tileSize

        Rectangle {
            id: renderItemTile
            width: pipelineTable.tileSize

            Layout.minimumHeight: 30
            Layout.fillWidth: true
            color: {
              if(model.is_highlighted == true) {
                palette.mid;
              }
              else if(model.state == false) {
                "red";
              }
              else {
                palette.midlight;
              }
            }
            Text {
              Layout.leftMargin: 10
              Layout.fillHeight: true
              verticalAlignment: Text.AlignVCenter
              color: palette.text
              font.pointSize: 10
              font.family: "Ubuntu"
              text: model.display_data
            }

          MouseArea {
            id: mouseArea

            acceptedButtons: Qt.LeftButton | Qt.RightButton
            propagateComposedEvents: true
            anchors.fill: parent
            onPressed: {
              if((mouse.modifiers & Qt.ControlModifier) && model.is_highlighted == true) {
                internal_model.dehighlight(row);
              }
              else if(mouse.button & Qt.LeftButton) {
                internal_model.highlight(row);
              }
            }
          }
        }

        Rectangle {
          Layout.fillWidth: true
          height: 2
          color: palette.dark
        }
      }
    }
  }
  MouseArea {
    acceptedButtons: Qt.LeftButton | Qt.RightButton
    anchors.fill: parent
    onPressed: {
      mouse.accepted = false;
      if (mouse.button & Qt.RightButton) {
        contextMenu.popup();
      }
    }
  }
}

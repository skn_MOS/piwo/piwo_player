import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

import "../common"

RowLayout {
  Layout.fillWidth: true
  Layout.topMargin: 10

  GridLayout
  {
    columns: 3
    Label {
      text: "Audio in(unsed)"
    }
    ComboBox {
        Layout.minimumWidth: 500
        enabled:false
        model: {
          if(audio_model) {
            audio_model.audio_in_list
          }else{
            []
          }
        }
        onActivated: audio_model.audio_in = currentIndex
        onPressedChanged: {
          if(pressed){
            audio_model.audio_refresh();
          }
        }

    }
    Button {
      enabled:false
      Layout.alignment: Qt.AlignRight
      text: if(audio_model && !audio_model.in_connected)
            {
              qsTr("Connect")
            }else
            {
              qsTr("Disconnect")
            }
      onClicked: {
        audio_model.audio_in_connect();
      }
    }

    Label {
      text: "Audio out"
    }
    ComboBox {
        Layout.minimumWidth: 500
        enabled:(audio_model && !audio_model.out_connected)
        model: {
          if(audio_model) {
            audio_model.audio_out_list
          }else{
            []
          }
        }
        onActivated: audio_model.audio_out = currentIndex
        onPressedChanged: {
          if(pressed){
            audio_model.audio_refresh();
          }
        }
    }
    Button {
      Layout.alignment: Qt.AlignRight
      text: if(audio_model && !audio_model.out_connected)
            {
              qsTr("Connect")
            }else
            {
              qsTr("Disconnect")
            }
      onClicked: {
        if(audio_model && !audio_model.out_connected)
        {
          audio_model.audio_out_connect();
        }
        else
        {
          audio_model.audio_out_disconnect();
        }
      }
    }
  }
}

import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12

import general_settings_model 1.0

ColumnLayout {
  Layout.fillWidth: true
  Layout.topMargin: 10

  Label {
    text: "Resolution"
    font.pixelSize: 16
  }

  RowLayout
  {
    Layout.fillWidth: true
    Layout.leftMargin: 50
    spacing: 5

    Label {
      text: "Width"
    }

    TextField {
      Layout.fillWidth: true
      text: {
        if (general_settings_model && typeof general_settings_model.width != "undefined") {
          general_settings_model.width.toString();
        }
        else {
          "";
        }
      }

      onEditingFinished: general_settings_model.width = parseInt(text)
    }

    Label {
      text: "Height"
    }

    TextField {
      Layout.fillWidth: true
      text: {
        if (general_settings_model && typeof general_settings_model.height != "undefined") {
          general_settings_model.height.toString();
        }
        else {
          "";
        }
      }

      onEditingFinished: general_settings_model.height = parseInt(text)
    }
  }
}

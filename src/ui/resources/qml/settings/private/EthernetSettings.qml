import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.12

import ethernet_settings_model 1.0

ColumnLayout {
  Layout.fillWidth: true
  ColumnLayout {
    Layout.fillWidth: true
    Layout.topMargin: 10

    Label {
      text: "Connection info"
      font.pixelSize: 16
    }

    RowLayout
    {
      Layout.fillWidth: true
      Layout.leftMargin: 50
      spacing: 5

      Label {
        text: "IP"
      }

      TextField {
        Layout.fillWidth: true
        id: settings_ethernet_ip
        text: ethernet_settings_model.default_ip;
      }

      Label {
        text: "TCP Port"
      }

      TextField {
        Layout.fillWidth: true
        id: settings_ethernet_port_tcp
        text: ethernet_settings_model.default_port_tcp;
      }

      Label {
        text: "UDP Port"
      }

      TextField {
        Layout.fillWidth: true
        id: settings_ethernet_port_udp
        text: ethernet_settings_model.default_port_udp;
      }

      Button {
        id: settings_ethernet_connect_button
        Layout.alignment: Qt.AlignRight

        text: ethernet_settings_model ? ethernet_settings_model.eth_connected ? qsTr("Disconnect") : qsTr("Connect") : "";

        onPressed: {
          if (ethernet_settings_model.eth_connected) {
            ethernet_settings_model.tx_disconnect();
          }

          else {
            ethernet_settings_model.tx_connect(settings_ethernet_ip.text, settings_ethernet_port_tcp.text, settings_ethernet_port_udp.text)
          }
        }
      }
    }
  }

  ColumnLayout {
    Layout.fillWidth: true
    Layout.topMargin: 20

    Label {
      text: "RSSI"
      font.pixelSize: 16
    }

    RowLayout
    {
      Layout.fillWidth: true
      Layout.leftMargin: 50
      spacing: 5

      Label {
        text: "TX ID"
      }

      TextField {
        Layout.fillWidth: true
        id: settings_ethernet_rssi_tx_id
        text: {
          if (ethernet_settings_model && typeof ethernet_settings_model.tx_id != "undefined") {
              ethernet_settings_model.tx_id.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: ethernet_settings_model.tx_id = parseInt(text)
      }

      Label {
        text: "Radio ID"
      }

      TextField {
        Layout.fillWidth: true
        id: settings_ethernet_rssi_radio_id
        text: {
          if (ethernet_settings_model && typeof ethernet_settings_model.radio_id != "undefined") {
              ethernet_settings_model.radio_id.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: ethernet_settings_model.radio_id = parseInt(text)
      }

      Label {
        text: "Interval (ms)"
      }

      TextField {
        Layout.fillWidth: true
        id: settings_ethernet_rssi_interval
        text: {
          if (ethernet_settings_model && typeof ethernet_settings_model.interval != "undefined") {
              ethernet_settings_model.interval.toString();
          }
          else {
            "";
          }
        }

        onEditingFinished: ethernet_settings_model.interval = parseInt(text)
      }
    }
  }
}

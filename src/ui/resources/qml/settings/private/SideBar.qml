import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.12

ColumnLayout {
  id: side_settings_menu

  property var currentIndex: 0

  Button {
    id: general_settings_button
    Layout.fillHeight: true
    text: qsTr("General")
    highlighted: true;
    onPressed: {
      currentIndex = 0;
      highlighted = true;
      ethernet_settings_button.highlighted = false;
      audio_settings_button.highlighted = false;
      midi_settings_button.highlighted = false;
      lpd_settings_button.highlighted = false;
      re_settings_button.highlighted = false;
    }
  }

  Button {
    id: re_settings_button
    Layout.fillHeight: true
    text: qsTr("Render engine")
    onPressed: {
      currentIndex = 1;
      highlighted = true;
      general_settings_button.highlighted = false;
      ethernet_settings_button.highlighted = false;
      audio_settings_button.highlighted = false;
      midi_settings_button.highlighted = false;
      lpd_settings_button.highlighted = false;
    }
  }

  Button {
    id: ethernet_settings_button
    Layout.fillHeight: true
    text: qsTr("Ethernet")
    onPressed: {
      currentIndex = 2;
      highlighted = true;
      general_settings_button.highlighted = false;
      audio_settings_button.highlighted = false;
      midi_settings_button.highlighted = false;
      lpd_settings_button.highlighted = false;
      re_settings_button.highlighted = false;
    }
  }

  Button {
    id: audio_settings_button
    Layout.fillHeight: true
    text: qsTr("Audio")
    onPressed: {
      currentIndex = 3;
      highlighted = true;
      general_settings_button.highlighted = false;
      ethernet_settings_button.highlighted = false;
      midi_settings_button.highlighted = false;
      lpd_settings_button.highlighted = false;
      re_settings_button.highlighted = false;
    }
  }

  Button {
    id: midi_settings_button
    Layout.fillHeight: true
    text: qsTr("MIDI")
    onPressed: {
      currentIndex = 4;
      highlighted = true;
      general_settings_button.highlighted = false;
      ethernet_settings_button.highlighted = false;
      audio_settings_button.highlighted = false;
      lpd_settings_button.highlighted = false;
      re_settings_button.highlighted = false;
    }
  }

  Button {
    id: lpd_settings_button
    Layout.fillHeight: true
    text: qsTr("LPD")
    onPressed: {
      currentIndex = 5;
      highlighted = true;
      general_settings_button.highlighted = false;
      ethernet_settings_button.highlighted = false;
      audio_settings_button.highlighted = false;
      midi_settings_button.highlighted = false;
      re_settings_button.highlighted = false;
    }
  }
}

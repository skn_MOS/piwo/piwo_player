import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

RowLayout {
  Layout.fillWidth: true
  Layout.topMargin: 10

  RenderEngineSettings {
    width: 300
    Layout.alignment: Qt.AlignTop
  }

  PipelineModel {
    internal_model: pipeline_model
    col_count: 6
    Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
    Layout.topMargin: 25
    Layout.bottomMargin: 25
    Layout.leftMargin: 25
    Layout.rightMargin: 25
    Layout.minimumHeight: 100
    Layout.fillWidth: true
    Layout.fillHeight: true
  }
}

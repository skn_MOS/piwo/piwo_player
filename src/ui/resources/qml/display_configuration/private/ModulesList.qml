import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.12

Rectangle {
  readonly property int searchTabHeight: 30
  property variant internal_model
  color: palette.midlight

  ColumnLayout {
    anchors.fill: parent
    spacing: {0, 0}
    Rectangle {
      Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
      Layout.preferredHeight: 50
      Layout.fillWidth: true
      color: palette.button

      Text {
        anchors.fill: parent
        font.family: "Ubuntu"
        text: "Unassigned modules"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 18
        color: palette.text
      }
    }
    Rectangle {
      Layout.fillWidth: true
      Layout.preferredHeight: 3
      color: palette.mid
    }
    RowLayout {
      Layout.leftMargin: 10
      Layout.fillWidth: true
      Layout.preferredHeight: 30
      Text {
        Layout.preferredWidth: 30
        color: palette.text
        font.pointSize: 9
        font.family: "Ubuntu"
        text: "ID"
      }
      Text {
        Layout.fillWidth: true
        color: palette.text
        font.pointSize: 9
        font.family: "Ubuntu"
        text: "UID"
      }
    }
    Rectangle {
      Layout.fillWidth: true
      Layout.preferredHeight: 2
      color: palette.dark
    }

    Menu {
      id: contextMenu

      MenuItem {
        text: "Edit"
        onTriggered: {
          internal_model.edit_highlighted_modules();
        }
      }

      Menu {
        title: "Automation"
        MenuItem {
          text: "Auto config selected modules"
          onTriggered: {
            internal_model.auto_configure_modules();
          }
        }
      }
    }

    ListView {
      id: list

      model: internal_model
      Layout.fillHeight: true
      Layout.fillWidth: true
      clip: true
      spacing: {0, 0}

      DropArea {
        id: raw_modules_list_drop_area
        anchors.fill: parent
        onDropped: {
            internal_model.module_dropped(drag.source.uid);
        }
      }

      // Rectangle {
      //   id: search_tab

      //   height: searchTabHeight
      //   width: parent.width
      //   anchors.bottom: list.bottom
      //   anchors.left: list.left
      //   anchors.right: list.right
      //   color: palette.midlight

      //   TextField {
      //     placeholderText: "Type to search..."

      //     background: Rectangle {
      //         color: "transparent"
      //     }
      //   }
      // }

      delegate: ColumnLayout {
        width: list.width
        spacing: {0, 0}

        Rectangle {
          id: moduleTile

          Layout.minimumHeight: 30
          Layout.fillWidth: true
          Drag.active: moduleMouseArea.drag.active
          Drag.dragType: Drag.Automatic
          color: {
            if(model.is_highlighted == true) {
              palette.mid;
            }
            else {
              palette.midlight;
            }
          }
          states: [
            State {
              name: "NORM"

              PropertyChanges {
                target: moduleMouseArea
                drag.target: undefined
              }

              PropertyChanges {
                target: moduleTile
              }
            },
            State {
              name: "DRAG"

              PropertyChanges {
                target: moduleMouseArea
                drag.target: drag_data
              }

              PropertyChanges {
                target: moduleTile
              }
            }
          ]

          RowLayout {
            anchors.fill: parent
            clip: true

            Text {
              Layout.leftMargin: 10
              Layout.preferredWidth: 25
              Layout.fillWidth: true
              Layout.fillHeight: true
              Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
              verticalAlignment: Text.AlignVCenter
              color: palette.text
              font.pointSize: 10
              font.family: "Ubuntu"
              text: model.id
            }

            Text {
              Layout.fillWidth: true
              Layout.fillHeight: true
              Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
              verticalAlignment: Text.AlignVCenter
              font.pointSize: 10
              font.family: "Ubuntu"
              color: palette.text
              text: model.uid
            }
          }

          MouseArea {
            id: moduleMouseArea

            pressAndHoldInterval: 200
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            propagateComposedEvents: true
            drag.threshold: 0
            anchors.fill: parent
            hoverEnabled: true
            Component.onCompleted: {
              moduleTile.state = "NORM";
            }
            onPressed: {
              if((mouse.modifiers & Qt.ControlModifier) && model.is_highlighted == true) {
                internal_model.dehighlight(index);
              }
              else if(mouse.button & Qt.LeftButton) {
                internal_model.highlight(index);
              }
            }
            onPressAndHold: {
              mouse.accepted = false;
              if ((mouse.button & Qt.LeftButton) && (model.uid)) {
                parent.grabToImage(function(result) {
                  parent.Drag.imageSource = result.url;
                });
                internal_model.dehighlight(index);
                moduleTile.state = "DRAG";
              }
            }
            onReleased: {
              moduleTile.state = "NORM";
            }
          }

          Drag.source: ModuleDragData {
            id: drag_data
            uid: model.uid
          }
        }

        Rectangle {
          Layout.fillWidth: true
          height: 2
          color: palette.dark
        }
      }
    }
  }

  MouseArea {
    acceptedButtons: Qt.LeftButton | Qt.RightButton
    anchors.fill: parent
    onPressed: {
      mouse.accepted = false;
      if (mouse.button & Qt.RightButton) {
        contextMenu.popup();
      }
      else {
        if(!(mouse.modifiers & Qt.ControlModifier)) {
          internal_model.dehighlight_all();
        }
      }
    }
  }
}

import QtQuick 2.12

import QtQuick.Controls 2.12
import QtQml.Models 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls.Styles 1.4

import "private"
import "common"

RowLayout {

  Shortcut {
    sequence: "Ctrl+A"
    onActivated: {
      if(display_layout_model)
      {
        display_layout_model.highlight_all()
      }
    }
  }

  ModuleEditPopup {
  }
  Display {
    Layout.fillHeight: true
    Layout.fillWidth: true
  }
  Rectangle {
    id: side_container
    color: palette.dark
    Layout.preferredWidth: 350
    Layout.fillHeight: true
    ColumnLayout {
      anchors.fill: side_container
      ModulesList {
        id: avail_modules_list
        internal_model: raw_modules_model
        Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
        Layout.topMargin: 25
        Layout.bottomMargin: 25
        Layout.leftMargin: 25
        Layout.rightMargin: 25
        Layout.minimumHeight: 100
        Layout.fillWidth: true
        Layout.fillHeight: true
      }

      FlatDesignSwitch {
        Layout.leftMargin: 25
        onColor: palette.light
        onBorder: palette.light
        Text {
          anchors.left: parent.right
          anchors.verticalCenter: parent.verticalCenter
          text: "Verify with LSD"
          font.pixelSize: 14
          color: palette.text
        }
      }
      Button {
        Layout.leftMargin: 25
        Layout.rightMargin: 25
        Layout.bottomMargin: 25
        Layout.topMargin: 10
        Layout.fillWidth: true
        Layout.preferredHeight: 50
        Text {
          anchors.horizontalCenter: parent.horizontalCenter
          anchors.verticalCenter: parent.verticalCenter
          text: "Commit configuration"
          font.pixelSize: 14
          color: palette.text
        }
        onClicked: {
          module_configuration_widget.commit_configuration()
        }
      }
    }
  }
}

#pragma once

#include <QString>
#include <QtQuick>

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class qml_debugger : public QObject
{
  Q_OBJECT
public:
  qml_debugger([[maybe_unused]] QObject* parent = nullptr){};
  ~qml_debugger() override = default;

  Q_INVOKABLE static QString
  print_object(QQuickItem* item);
};

} // namespace gui

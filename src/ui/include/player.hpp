#pragma once

#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QString>
#include <QWidget>

#include "player/lightshow_player_model.hpp"
#include "player/lightshow_playlist_model.hpp"
#include "player/render_engine_model.hpp"

#include "config.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class player : public QObject
{
  Q_OBJECT

public:
  explicit player(QQmlContext* context, QWidget* parent = nullptr);
  ~player() override = default;

  Q_INVOKABLE void
  load_animation(const QString& qpath);

private:
  lightshow_player_model _lightshow_player_model;
  lightshow_playlist_model _lightshow_playlist_model;
  render_engine_scene_model _render_engine_model;
};
} // namespace gui

#pragma once

#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QWidget>

#include "module_configuration/module_edit_popup_model.hpp"
#include "module_configuration/modules_list_model.hpp"
#include "module_configuration/modules_matrix_model.hpp"

namespace gui
{
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class modules_configuration : public QObject
{
  Q_OBJECT

public:
  explicit modules_configuration(QQmlContext* context,
                                 QWidget* parent = nullptr);
  void
  fill_buttons(uint32_t rows, uint32_t cols);
  ~modules_configuration() override = default;
public slots:
  void
  import_modules(const QString& path);
  void
  export_modules(const QString& path);
  void
  import_configuration(const QString& path);
  void
  export_configuration(const QString& path);
  void
  commit_configuration();

private:
  std::shared_ptr<module_edit_popup_model> _module_edit_popup_model_sp;
  modules_list_model _raw_modules_list_model;
  modules_matrix_model _display_layout_model;

  bool _config_commited = false;
};
} // namespace gui

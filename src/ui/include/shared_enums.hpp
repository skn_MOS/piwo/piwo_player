#pragma once

#include <QObject>

#include "config.hpp"

// Only one occurence of Q_NAMESPACE with same name is allowed by Qt
// This is the place for all enums shared between cpp and qml
namespace gui::cpp_enum
{
Q_NAMESPACE
enum application_state_enum
{
  IDLE = static_cast<int>(application_state_t::IDLE),
  DISPLAY_AUTO_CONFIG =
    static_cast<int>(application_state_t::DISPLAY_AUTO_CONFIG),
  LIGHTSHOW = static_cast<int>(application_state_t::LIGHTSHOW),
  RSSI = static_cast<int>(application_state_t::RSSI),
};
Q_ENUM_NS(application_state_enum)

enum logging_level_enum
{
  TRACE = SPDLOG_LEVEL_TRACE,
  DEBUG = SPDLOG_LEVEL_DEBUG,
  INFO = SPDLOG_LEVEL_INFO,
  WARN = SPDLOG_LEVEL_WARN,
  ERR = SPDLOG_LEVEL_ERROR,
  CRITICAL = SPDLOG_LEVEL_CRITICAL,
  OFF = SPDLOG_LEVEL_OFF,
};
Q_ENUM_NS(logging_level_enum)

} // namespace gui::cpp_enum

#pragma once

#include <algorithm>

#include <QQuickImageProvider>
#include <qnamespace.h>
#include <qrgb.h>

#include <piwo/animation.h>
#include <piwo/frame.h>
#include <piwo/lp_protodef.h>
#include <spdlog/spdlog.h>

#include "config.hpp"
#include "qt_ui_iface.hpp"

class piwo7_image_provider final
  : public QQuickImageProvider
  , public QObject
{
public:
  piwo7_image_provider()
    : QQuickImageProvider(QQuickImageProvider::Pixmap)
  {
    _render_engine_frame = piwo::alloc_frame_shared(1, 1);
    _playlist_frame = piwo::alloc_frame_shared(1, 1);

    QObject::connect(&qt_ui_if,
                     &qt_ui_iface::signal_light_show_started,
                     this,
                     &piwo7_image_provider::on_light_show_started);

    QObject::connect(&qt_ui_if,
                     &qt_ui_iface::signal_animation_started,
                     this,
                     &piwo7_image_provider::on_playlist_started);

    QObject::connect(&qt_ui_if,
                     &qt_ui_iface::signal_light_show_stopped,
                     this,
                     &piwo7_image_provider::on_light_show_stopped);

    QObject::connect(&qt_ui_if,
                     &qt_ui_iface::signal_animation_paused,
                     this,
                     &piwo7_image_provider::on_playlist_stopped);

    QObject::connect(&qt_ui_if,
                     &qt_ui_iface::signal_animation_changed,
                     this,
                     &piwo7_image_provider::on_animation_changed);
  }

  static QImage
  to_qimage(piwo::frame& frame)
  {
    auto image = QImage(frame.width, frame.height, QImage::Format_RGB32);

    size_t x = 0, y = 0;
    for (const auto& c : frame)
    {
      image.setPixelColor(static_cast<int>(x),
                          static_cast<int>(frame.height - y - 1ULL),
                          QColor(qRgb(c.r, c.g, c.b)));

      ++x;

      if (x >= frame.width)
      {
        ++y;
        x = 0;
      }
    }

    return image;
  }

  QImage
  requestImage(const QString& id_qstr,
               QSize* size,
               const QSize& requested_size) final
  {
    auto id = id_qstr.toStdString();

    size_t nframe = 0;
    if (auto arg_pos = id.find_last_of('/');
        arg_pos != std::string::npos && !id.ends_with('/'))
    {
      auto [_, conv_res] =
        std::from_chars(&id[arg_pos + 1], &*id.end(), nframe);

      if (conv_res == std::errc())
        id.erase(arg_pos, std::numeric_limits<size_t>::max());
    }
    nframe = std::min(nframe, this->_current_anim.frame_count() - 1);

    if (id == "/")
      return QImage();

    QImage image;

    if (id == "/pipeline")
    {
      image = to_qimage(*this->_render_engine_frame);
    }
    else if (id == "/playlist_animation" &&
             this->_current_anim.frame_count() > nframe)
    {
      image = to_qimage(this->_current_anim[nframe]);
    }
    else
    {
      image = to_qimage(*this->_playlist_frame);
    }

    auto ret =
      image.scaled(requested_size, Qt::AspectRatioMode::KeepAspectRatio);

    if (size)
      *size = ret.size();

    return ret;
  }

  [[nodiscard]] ImageType
  imageType() const override
  {
    return ImageType::Image;
  }

public slots:

  void
  on_light_show_started()
  {
    auto [render_engine_lock, render_engine] =
      global_config.acquire_render_engine();
    _render_engine_frame = render_engine.get_frame();
  }

  void
  on_animation_changed()
  {
    auto [playlist_lock, playlist] = global_config.acquire_playlist();
    std::optional anim_opt = playlist.get_current_animation();
    if (!anim_opt)
    {
      spdlog::debug("Animation index is not valid");
      this->_current_anim = piwo::animation();
      return;
    }

    this->_current_anim = anim_opt.value();
  }

  void
  on_light_show_stopped()
  {
    _render_engine_frame = piwo::alloc_frame_shared(1, 1);
  }

  void
  on_playlist_started()
  {
    auto [playlist_lock, playlist] = global_config.acquire_playlist();
    _playlist_frame = playlist.get_frame();
  }

  void
  on_playlist_stopped()
  {
    _playlist_frame = piwo::alloc_frame_shared(1, 1);
  }

private:
  piwo::frames_intersection::frame_t _render_engine_frame;
  piwo::frames_intersection::frame_t _playlist_frame;

  // keeping a copy of currently displayed animation
  // is required only for animation preview
  piwo::animation _current_anim;
};

#pragma once

#include "state.hpp"
#include <piwo/protodef.h>

#include <QObject>
#include <qqmlapplicationengine.h>

#include "common_types.hpp"
#include "network.hpp"
#include "pipeline_config.hpp"

inline QQmlApplicationEngine* global_qml_engine;

class qt_ui_iface : public QObject
{
  Q_OBJECT

public:
  void
  config_module_added(const piwo::uid uid)
  {
    emit signal_config_module_added(uid);
  }

  void
  config_module_removed(const piwo::uid uid)
  {
    emit signal_config_module_removed(uid);
  }

  void
  config_module_data_changed(const piwo::uid uid)
  {
    emit signal_config_module_data_changed(uid);
  }

  void
  modules_config_container_rearanged()
  {
    emit signal_modules_config_container_rearanged();
  }

  void
  config_resolution_changed()
  {
    emit signal_config_resolution_changed();
  }

  void
  application_state_changed(const application_state_t state)
  {
    emit signal_application_state_changed(state);
  }

  void
  display_animation_changed()
  {
    emit signal_animation_changed();
  }

  void
  playlist_changed()
  {
    emit signal_playlist_changed();
  }

  void
  playlist_animation_added(const animation_id_t id)
  {
    emit signal_playlist_animation_added(id);
  }

  void
  playlist_animation_removed(const animation_id_t id)
  {
    emit signal_playlist_animation_removed(id);
  }

  void
  playlist_animation_index_changed(const animation_id_t id)
  {
    emit signal_playlist_animation_index_changed(id);
  }

  void
  light_show_started()
  {
    emit signal_light_show_started();
  }

  void
  light_show_stopped()
  {
    emit signal_light_show_stopped();
  }

  void
  rssi_started()
  {
    emit signal_rssi_started();
  }

  void
  rssi_stopped()
  {
    emit signal_rssi_stopped();
  }

  void
  animation_started()
  {
    emit signal_animation_started();
  }

  void
  animation_paused()
  {
    emit signal_animation_paused();
  }

  void
  animation_stopped()
  {
    emit signal_animation_stopped();
  }

  void
  render_engine_configured(const pipeline_config config)
  {
    emit signal_render_engine_configured(config);
  }

  void
  render_engine_step_removed(step_id id)
  {
    emit signal_render_engine_step_removed(id);
  }

  void
  render_engine_step_state_changed([[maybe_unused]] step_id id, bool state)
  {
    emit signal_render_engine_step_state_changed(id, state);
  }

  void
  render_engine_state_chnaged(bool state)
  {
    emit signal_render_engine_state_changed(state);
  }

  void
  audio_in_list_changed(const audio_list devices)
  {
    emit signal_audio_in_list_changed(devices);
  }

  void
  audio_out_list_changed(const audio_list devices)
  {
    emit signal_audio_out_list_changed(devices);
  }

  void
  audio_out_connection_status_changed(bool state)
  {
    emit signal_audio_out_connection_status_changed(state);
  }

  void
  midi_list_changed(const midi_list devices)
  {
    emit signal_midi_list_changed(devices);
  }

  void
  midi_connection_status_changed(bool state)
  {
    emit signal_midi_connection_status_changed(state);
  }

  void
  eth_state_changed(bool new_state)
  {
    emit signal_eth_state_changed(new_state);
  }

  void
  rssi_config_changed(rssi_config config)
  {
    emit signal_rssi_config_changed(config);
  }

signals:
  void
  signal_config_module_added(const piwo::uid uid);

  void
  signal_config_module_removed(const piwo::uid uid);

  void
  signal_config_module_data_changed(const piwo::uid uid);

  void
  signal_modules_config_container_rearanged();

  void
  signal_config_resolution_changed();

  void
  signal_application_state_changed(const application_state_t state);

  void
  signal_animation_changed();

  void
  signal_playlist_animation_added(const animation_id_t id);

  void
  signal_playlist_animation_removed(const animation_id_t id);

  void
  signal_playlist_animation_index_changed(const animation_id_t id);

  void
  signal_playlist_changed();

  void
  signal_light_show_started();

  void
  signal_light_show_stopped();

  void
  signal_rssi_started();

  void
  signal_rssi_stopped();

  void
  signal_animation_started();

  void
  signal_animation_paused();

  void
  signal_animation_stopped();

  void
  signal_render_engine_configured(const pipeline_config config);

  void
  signal_render_engine_step_removed(step_id id);

  void
  signal_render_engine_step_state_changed(step_id id, bool state);

  void
  signal_render_engine_state_changed(bool);

  void
  signal_audio_in_list_changed(const audio_list);

  void
  signal_audio_out_list_changed(const audio_list);

  void
  signal_audio_out_connection_status_changed(bool);

  void
  signal_midi_list_changed(const midi_list);

  void
  signal_midi_connection_status_changed(bool);

  void
  signal_eth_state_changed(bool new_state);

  void
  signal_rssi_config_changed(rssi_config config);

} inline qt_ui_if; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

#include "player.hpp"

#include <spdlog/spdlog.h>

namespace gui
{
player::player(QQmlContext* context, QWidget* parent)
  : QObject(parent)
{
  qmlRegisterType<lightshow_playlist_model>(
    "lightshow_playlist_model", 1, 0, "LightshowPlaylistModel");
  qmlRegisterType<lightshow_player_model>(
    "lightshow_player_model", 1, 0, "LightshowPlayerModel");
  if (!context)
  {
    spdlog::critical("Null ptr qml context");
    std::terminate();
  }

  context->setContextProperty(QStringLiteral("lightshow_player_model"),
                              &this->_lightshow_player_model);
  context->setContextProperty(QStringLiteral("lightshow_playlist_model"),
                              &this->_lightshow_playlist_model);
  context->setContextProperty(QStringLiteral("render_engine_scene_model"),
                              &this->_render_engine_model);
}

Q_INVOKABLE void
player::load_animation(const QString& qpath)
{
  spdlog::trace(__PRETTY_FUNCTION__);

  std::string path = QUrl(qpath).toLocalFile().toStdString();

  global_config.load_animation(path.c_str());
}

} // namespace gui

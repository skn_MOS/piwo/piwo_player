#include "modules_configuration.hpp"

#include <spdlog/spdlog.h>

#include <QDebug>
#include <QQmlApplicationEngine>

namespace gui
{
modules_configuration::modules_configuration(QQmlContext* context,
                                             QWidget* parent)
  : QObject(parent)
  , _module_edit_popup_model_sp(std::make_shared<module_edit_popup_model>())
{
  qmlRegisterType<modules_list_model>(
    "modules_list_model", 1, 0, "ModulesListModel");
  qmlRegisterType<modules_matrix_model>(
    "display_layout_model", 1, 0, "DisplayLayoutModel");
  qmlRegisterType<module_edit_popup_model>(
    "module_edit_popup_model", 1, 0, "ModuleEditPopupModel");
  if (!context)
  {
    spdlog::error("Null ptr qml context");
    std::terminate();
  }
  context->setContextProperty(QStringLiteral("raw_modules_model"),
                              &this->_raw_modules_list_model);
  context->setContextProperty(QStringLiteral("display_layout_model"),
                              &this->_display_layout_model);
  context->setContextProperty(QStringLiteral("module_edit_popup_model"),
                              this->_module_edit_popup_model_sp.get());

  this->_raw_modules_list_model.set_module_edit_popup_model(
    this->_module_edit_popup_model_sp);
  this->_display_layout_model.set_module_edit_popup_model(
    this->_module_edit_popup_model_sp);
}

void
modules_configuration::import_modules(const QString& path)
{
  const auto c_string_path = QUrl(path).toLocalFile().toStdString();
  spdlog::info("Import raw modules info requested. PATH: {}", c_string_path);

  global_config.load_raw_modules_from_file(c_string_path.c_str());
}

void
modules_configuration::export_modules(const QString& path)
{
  spdlog::info("Export raw modules info requested. PATH: {}",
               path.toStdString());
}

void
modules_configuration::import_configuration(const QString& path)
{
  const auto c_string_path = QUrl(path).toLocalFile().toStdString();
  spdlog::info("Import configuration requested. PATH: {}", c_string_path);

  global_config.load_from_file(c_string_path.c_str());
}

void
modules_configuration::export_configuration(const QString& path)
{
  const auto c_string_path = QUrl(path).toLocalFile().toStdString();
  spdlog::info("Export configuration requested. PATH: {}", path.toStdString());

  global_config.save_to_file(c_string_path.c_str());
}

void
modules_configuration::commit_configuration()
{
  this->_config_commited = global_config.commit_configuration();
}

} // namespace gui

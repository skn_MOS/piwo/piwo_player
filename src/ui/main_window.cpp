#include "main_window.hpp"

namespace gui
{
main_window::main_window(QQmlApplicationEngine& e, QObject* p)
  : QObject(p)
  , _configuration_widget(e.rootContext())
  , _settings_widget(e.rootContext())
  , _player_widget(e.rootContext())
{
  qmlRegisterType<footer_model>("footer_model", 1, 0, "FooterModel");

  e.rootContext()->setContextProperty(QStringLiteral("footer_model"),
                                      &_footer_model);
  e.rootContext()->setContextProperty(
    QStringLiteral("module_configuration_widget"), &_configuration_widget);
  e.rootContext()->setContextProperty(QStringLiteral("settings_widget"),
                                      &_settings_widget);
  e.rootContext()->setContextProperty(QStringLiteral("player_widget"),
                                      &_player_widget);
}
} // namespace gui

#!/bin/env sh

export PKG_CONFIG_PATH=/usr/lib/pkgconfig
mkdir -p build
cd build
cmake -GNinja -DBUILD_TESTS=ON -DWITH_QT_UI=OFF ..
ninja
test/core_test


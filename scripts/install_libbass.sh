#!/bin/env sh
set -ex

[ $# -eq 0 ] && \
	echo -e "Usage: $0 <target_platform>\n\n\ttarget_platform - linux or win" && \
	exit 1


function cleanup()
{
  rm -rf /tmp/libbass
}

#set target [win or linux]
target=$1

trap "cleanup ${bass_tmp}" EXIT SIGINT

# install libbass

mkdir -p /tmp/libbass

if [[ $target == 'linux' ]]
then
  wget 'http://us.un4seen.com/files/bass24-linux.zip' --output-document=/tmp/libbass/bass24-linux.zip
  unzip /tmp/libbass/bass24-linux.zip -d /tmp/libbass
  install -m 775 -o root /tmp/libbass/libs/x86_64/libbass.so /usr/lib/
  install -m 664 -o root /tmp/libbass/bass.h /usr/include/
elif [[ $target == 'win' ]]
then
  wget 'http://us.un4seen.com/files/bass24.zip' --output-document=/tmp/libbass/bass24.zip
  unzip /tmp/libbass/bass24.zip -d /tmp/libbass
  install -m 775 -o root /tmp/libbass/x64/bass.dll /usr/x86_64-w64-mingw32/bin/
  install -m 664 -o root /tmp/libbass/c/x64/bass.lib /usr/x86_64-w64-mingw32/lib/
  install -m 664 -o root /tmp/libbass/c/bass.h /usr/x86_64-w64-mingw32/include/
else
  echo 'Target not supported'
fi

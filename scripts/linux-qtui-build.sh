#!/bin/env sh

export PKG_CONFIG_PATH=/usr/lib/pkgconfig
mkdir build
cd build
cmake -GNinja -DWITH_QT_UI=YES ..
ninja


#!/bin/env sh

if [ "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}" == "" ]
then
	git fetch origin master --depth 1
	tools/format-check origin/master
else
	git fetch origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} --depth 1
	tools/format-check "${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
fi

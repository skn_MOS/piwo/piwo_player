#!/bin/env sh

export PKG_CONFIG_PATH=/usr/x86_64-w64-mingw32/sys-root/mingw/lib/pkgconfig
mkdir build
cd build
cmake -GNinja -DCMAKE_TOOLCHAIN_FILE=../cmake/cross-compile/linux_cc_windows.cmake -DWITH_QT_UI=NO ..
ninja


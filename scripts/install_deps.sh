#!/bin/env sh
set -ex

function cleanup()
{
  rm -rf ${tmp_dir}
}

trap "cleanup ${bass_tmp}" EXIT SIGINT

export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig

cmake_command="cmake -GNinja"
clone_command="git clone --depth 1"

#default linux
target=$1
if [[ "$target" != 'linux' && "$target" != 'win' ]]
then
  echo "No target specified. [linux or win required]"
  exit 1
fi

if [ "$target" == "linux" ]
then
  cmake_command="$cmake_command -DCMAKE_INSTALL_PREFIX=/usr"
elif [ "$target" == "win" ]
then
  cmake_command="$cmake_command -DCMAKE_TOOLCHAIN_FILE=/linux_cc_windows.cmake -DCMAKE_INSTALL_PREFIX=/usr/x86_64-w64-mingw32"
fi

h_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
tmp_dir="/tmp/piwo_player/deps/"
mipc_dir="${tmp_dir}/libmipc"
piwo_dir="${tmp_dir}/libpiwo"
rapid_dir="${tmp_dir}/rapidjson"
spdlog_dir="${tmp_dir}/spdlog"
fmt_dir="${tmp_dir}/fmt"
rtmidi_dir="${tmp_dir}/rtmidi"

rm -rf "${tmp_dir}"
mkdir -p "${mipc_dir}" "${piwo_dir}" "${rapid_dir}" "${fmt_dir}" "${spdlog_dir}" "${tmp_dir}" "${rtmidi_dir}"

#install rapidjson

${clone_command} https://github.com/Tencent/rapidjson "${rapid_dir}"
cd "${rapid_dir}"
mkdir  build && cd build
eval "$cmake_command -DRAPIDJSON_BUILD_ASAN=OFF \
                     -DRAPIDJSON_BUILD_EXAMPLES=OFF \
                     -DRAPIDJSON_BUILD_TESTS=OFF \
                     -DBUILD_TESTING=OFF \
                     -DRAPIDJSON_BUILD_THIRDPARTY_GTEST=OFF \
                     -DRAPIDJSON_BUILD_UBSAN=OFF \
                     -DCMAKE_BUILD_TYPE=Release .."
sudo ninja install

#install fmt

${clone_command} --branch 8.0.1 https://github.com/fmtlib/fmt "${fmt_dir}"
cd "${fmt_dir}"
mkdir -p build && cd build
eval "$cmake_command  -DFMT_TEST=OFF -DFMT_CUDA_TEST=OFF -DCMAKE_BUILD_TYPE=Release .."
sudo ninja install

#install mipc

${clone_command} --branch v1.4 https://gitlab.com/skn_MOS/piwo/libraries/mipc "${mipc_dir}"
cd "${mipc_dir}"
mkdir build && cd build
eval "$cmake_command  .."
sudo ninja install

#install libpiwo
${clone_command} --branch v1.10.1 https://gitlab.com/skn_MOS/piwo/libraries/libpiwo "${piwo_dir}"

cd "${piwo_dir}"
mkdir build && cd build
eval "$cmake_command  -DPIWO_ENABLE_LAYERS='all' .."
sudo ninja install

#install spdlog
${clone_command} --branch v1.9.0 https://github.com/gabime/spdlog.git "${spdlog_dir}"

cd "${spdlog_dir}"
mkdir build && cd build
eval "$cmake_command -DSPDLOG_FMT_EXTERNAL=ON -DSPDLOG_BUILD_EXAMPLE=OFF .."
sudo ninja install

#install rtmidi
${clone_command} --branch 5.0.0 https://github.com/thestk/rtmidi "${rtmidi_dir}"

cd "${rtmidi_dir}"
mkdir build && cd build
eval "$cmake_command -DCMAKE_BUILD_TYPE=Release .."
sudo ninja install

#install libbass
/install_libbass.sh $target


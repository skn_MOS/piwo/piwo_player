#!/bin/env sh

export PKG_CONFIG_PATH=/usr/lib/pkgconfig
mkdir build
cd build
cmake -GNinja -DRUN_CLANG_TIDY=ON -DCLANG_TIDY_WARNINGS_AS_ERRORS=* ..
ninja clean
ninja


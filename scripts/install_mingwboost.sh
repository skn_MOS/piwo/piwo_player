#!/bin/env sh
set -ex

wget https://archives.boost.io/release/1.86.0/source/boost_1_86_0.tar.gz
tar -zxvf boost_1_86_0.tar.gz
cd boost_1_86_0

./bootstrap.sh \
  toolset=gcc-mingw \
  target-os=windows \
  --with-libraries=thread,regex,system \
  --prefix=/usr/x86_64-w64-mingw32/

./b2 install

rm -rf boost_1_86_0*

#!/bin/env python3

import argparse
import os
from pydub import AudioSegment

def normalize_audio(input_file, output_file, target_db):
    """
    Normalize the audio in the input file to the target dB level.
    """
    audio = AudioSegment.from_file(input_file)

    change_in_db = target_db - audio.dBFS

    normalized_audio = audio.apply_gain(change_in_db)

    normalized_audio.export(output_file, format=os.path.splitext(input_file)[1][1:])

def main():
    parser = argparse.ArgumentParser(description="Audio Normalizer")
    parser.add_argument("-i", "--input", help="Input directory", required=True)
    parser.add_argument("-o", "--output", help="Output directory", required=True)
    parser.add_argument("-t", "--target", help="Target dB level", type=float, default=-20.0)
    args = parser.parse_args()

    print(f"Input directory: {args.input}")
    print(f"Output directory: {args.output}")
    print(f"Target dB level: {args.target}")

    if not os.path.isdir(args.input):
        print(f"Error: The input directory {args.input} does not exist.")
        return
        
    os.makedirs(args.output, exist_ok=True)

    for filename in os.listdir(args.input):
        if filename.endswith(('.mp3', '.wav')):
            input_file = os.path.join(args.input, filename)
            output_file = os.path.join(args.output, filename)
            print(f"Normalizing {filename}...")
            normalize_audio(input_file, output_file, args.target)
            print(f"Normalized {filename} saved to {args.output}.")

if __name__ == "__main__":
    main()

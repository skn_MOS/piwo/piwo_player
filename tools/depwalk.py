#!/bin/env python3

import argparse
import os
import re
from subprocess import check_output
from shutil import which, copyfile
import logging

_CROSS_COMPILE = 'x86_64-w64-mingw32-'
_DLL_REGEX = r'DLL Name: (.*\.dll)'
_SEARCH_HINTS = {'/usr/x86_64-w64-mingw32/'}
_FOUND_DEPS = {}
_LOG_LEVEL = logging.INFO

logger = logging.getLogger()
logger.setLevel(_LOG_LEVEL)

ch = logging.StreamHandler()
ch.setLevel(logger.level)
ch.setFormatter(logging.Formatter('[%(levelname)s] %(message)s'))
logging.addLevelName(logging.ERROR, "-")
logging.addLevelName(logging.INFO, " ")
logging.addLevelName(logging.WARN, "!")
logger.addHandler(ch)

parser = argparse.ArgumentParser()

parser.add_argument('file', type=str, help='target filepath')
parser.add_argument('-cc', '--cross-compile', type=str, help='CROSS_COMPILE prefix')
parser.add_argument('-o', '--out', type=str, help='output dir')
parser.add_argument('-sh', '--search-hint', nargs='*', type=str, help='search hint for dlls')
parser.add_argument('--dry-run', action='store_true', help='Don\'t copy any files, just print what would be copied')
args = parser.parse_args()

target = args.file

if not os.path.isfile(target):
    logger.error(f'{target} is not a file.')
    exit(1)

if args.cross_compile:
    _CROSS_COMPILE = args.cross_compile

if  args.search_hint:
    _SEARCH_HINTS.extend(args.search_hint)

objdump_name = f'{_CROSS_COMPILE}objdump'
objdump = which(objdump_name)

if not objdump:
    logger.error(f'could not find objdump ({objdump_name})')
    exit(2)

def find_binary_deps(binname, level = 0):
    indent_str = f'{"." * 2 * level} '

    logger.info(indent_str + f'Looking for imports of {binname}...')

    pe_headers_str = check_output([objdump, '-x', binname]).decode()

    m = re.compile(_DLL_REGEX)
    dlls = set(m.findall(pe_headers_str))

    for dll in dlls:
        logger.info(indent_str + f'Found import: {dll}')

    logger.info(indent_str + f'Looking for import of {binname}... done')
    logger.info(indent_str + f'Searching for dependecies in filesystem...')

    if _SEARCH_HINTS:
        logger.info(indent_str + f'Using hints: {",".join(_SEARCH_HINTS)}')

    for hint in _SEARCH_HINTS:
        for root, dirnames, filenames in os.walk(hint):
            for filename in filenames:
                if filename in dlls:
                    full_filename = os.path.join(root, filename)

                    try:
                        depsrc = _FOUND_DEPS[filename]
                    except KeyError:
                        depsrc = None

                    if depsrc:
                        logger.warning(indent_str + f'{filename}: Found second source of the dep here: {full_filename} - ignored.')
                        logger.warning(indent_str + f'Note: First source of dep: {filename} found at {depsrc}')
                        continue

                    logger.info(indent_str + f'Dependency {filename} found at {full_filename}')

                    copyfile(full_filename, os.path.join('.', filename))
                    _FOUND_DEPS[filename] = full_filename
                    find_binary_deps(full_filename, level + 1)

find_binary_deps(target)

logger.info(f'Searching for dependecies in filesystem...done')

any_not_found = False

#for dll in dlls:
#    if dll not in _FOUND_DEPS:
#        logger.warning(f'Dependency {dll} not found.')
#        any_not_found = True
#

if any_not_found:
    exit(1)
exit(0)

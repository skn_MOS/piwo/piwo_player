#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <piwo/frame.h>
#include <piwo/proto.h>

#include "piwo_core.hpp"

using namespace testing;
namespace
{
struct tx_descriptor
{
  uint8_t tx_id;
  uint8_t radio_id;
};

constexpr uint8_t tx1 = 0;
constexpr uint8_t r1 = 0;
constexpr uint8_t r2 = 1;
constexpr uint8_t r3 = 2;
} // namespace

using ls_config_array = ls_sending_task::ls_config_array;

TEST(
  logic_address,
  group_modules_by_tx_and_radio_with_valid_category_and_group_into_containers_correctly)
{
  struct expected_results
  {
    tx_descriptor tx_desc;
    int expected_group_size;
    std::vector<size_t> expected_modules_position;
  };

  constexpr uint8_t m_width = 4;
  constexpr uint8_t m_height = 4;

  modinfo_t modules;
  const auto frame = piwo::alloc_frame_shared(m_width, m_height);

  /*
   * CONFIG ONLY FOR TX1
   *---------------------
   *| R2 | R2 | R1 | R1 |
   *| R1 | R1 | R3 | R3 |
   *| R2 | R2 | R2 | R2 |
   *| R1 | R1 | R1 | R1 |
   *---------------------
   */
  constexpr tx_descriptor test_suite[m_width * m_height] = {
    { tx1, r2 }, { tx1, r2 }, { tx1, r1 }, { tx1, r1 },
    { tx1, r1 }, { tx1, r1 }, { tx1, r3 }, { tx1, r3 },
    { tx1, r2 }, { tx1, r2 }, { tx1, r2 }, { tx1, r2 },
    { tx1, r1 }, { tx1, r1 }, { tx1, r1 }, { tx1, r1 }
  };
  constexpr size_t results_size = 3;
  expected_results results[results_size] = {
    { .tx_desc = { tx1, r1 },
      .expected_group_size = 8,
      .expected_modules_position = { 2, 3, 4, 5, 12, 13, 14, 15 } },
    { .tx_desc = { tx1, r2 },
      .expected_group_size = 6,
      .expected_modules_position = { 0, 1, 8, 9, 10, 11 } },
    { .tx_desc = { tx1, r3 },
      .expected_group_size = 2,
      .expected_modules_position = { 6, 7 } },
  };

  constexpr const char* uid_format = "54:FF:6B:6:49:72:FF:49:33:49:11:{}";

  modinfo mod;
  for (int i = 0; auto c : test_suite)
  {
    mod.base.id = i;
    mod.base.uid = piwo::uid::from_ascii(fmt::format(uid_format, i)).value();
    mod.gateway_id = c.tx_id;
    mod.radio_id = c.radio_id;
    mod.position.x = i % 4;
    mod.position.y = i / 4;

    modules.insert(mod);
    i++;
  }

  ls_config_array grouped_modules = own_offsets(modules, frame);

  ASSERT_EQ(grouped_modules.size(), std::size(results));
  size_t expected_device_offset = 0;
  for (int i = 0; const auto& gm : grouped_modules)
  {
    auto found_result =
      std::find_if(results,
                   results + results_size,
                   [&](const expected_results& res)
                   {
                     return res.tx_desc.tx_id == gm.id.tx &&
                            res.tx_desc.radio_id == gm.id.radio;
                   });

    ASSERT_NE(found_result, results + results_size);

    ASSERT_EQ(gm.owner_info.owned_offsets.size(),
              found_result->expected_group_size);
    for (int j = 0; const auto m : gm.owner_info.owned_offsets)
    {
      EXPECT_EQ(m, found_result->expected_modules_position[j]);
      j++;
    }
    EXPECT_EQ(gm.device_offset, expected_device_offset);
    expected_device_offset += found_result->expected_modules_position.size();
    i++;
  }
}

TEST(logic_address, assign_logic_address_to_modules_with_success)
{
  constexpr uint8_t m_width = 4;
  constexpr uint8_t m_height = 4;
  /*
   * CONFIG ONLY FOR TX1
   *---------------------
   *| R2 | R2 | R1 | R1 |
   *| R1 | R1 | R3 | R3 |
   *| R2 | R2 | R2 | R2 |
   *| R1 | R1 | R1 | R1 |
   *---------------------
   */
  constexpr tx_descriptor test_suite[m_width * m_height] = {
    { tx1, r2 }, { tx1, r2 }, { tx1, r1 }, { tx1, r1 },
    { tx1, r1 }, { tx1, r1 }, { tx1, r3 }, { tx1, r3 },
    { tx1, r2 }, { tx1, r2 }, { tx1, r2 }, { tx1, r2 },
    { tx1, r1 }, { tx1, r1 }, { tx1, r1 }, { tx1, r1 }
  };

  constexpr const char* uid_format = "54:FF:6B:6:49:72:FF:49:33:49:11:{}";

  modinfo_t modules;
  modinfo mod;
  for (int i = 0; auto c : test_suite)
  {
    mod.base.id = i;
    mod.base.uid = piwo::uid::from_ascii(fmt::format(uid_format, i)).value();
    mod.gateway_id = c.tx_id;
    mod.radio_id = c.radio_id;
    mod.position.x = i % 4;
    mod.position.y = i / 4;

    modules.insert(mod);
    i++;
  }
  constexpr int logic_address_map[] = { 8,  9,  0,  1,  2, 3, 14, 15,
                                        10, 11, 12, 13, 4, 5, 6,  7 };
  tx_pixel_owner_info own1 = { .owned_offsets = { 2, 3, 4, 5, 12, 13, 14, 15 },
                               .color_offsets = {} };
  tx_pixel_owner_info own2 = { .owned_offsets = { 0, 1, 8, 9, 10, 11 },
                               .color_offsets = {} };
  tx_pixel_owner_info own3 = { .owned_offsets = { 6, 7 }, .color_offsets = {} };

  // frame and frame provider are not used in this test
  // they are neccessary to create the config
  const auto frame = piwo::alloc_frame_shared(m_width, m_height);

  ls_config_array grouped_modules = {
    { .device_offset = 0, .owner_info = own1, .frame = frame },
    { .device_offset = 8, .owner_info = own2, .frame = frame },
    { .device_offset = 14, .owner_info = own3, .frame = frame },
  };

  configure_logic_address(modules, grouped_modules, m_width);

  for (size_t i = 0; i < modules.size(); i++)
  {
    auto m = std::find_if(modules.begin(),
                          modules.end(),
                          [i](const modinfo& m)
                          { return static_cast<size_t>(m.base.id) == i; });

    if (m == modules.end())
      continue;

    EXPECT_EQ(m->logic_address, logic_address_map[i]);
    i++;
  }
}

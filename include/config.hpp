#pragma once

#include <cstring>
#include <mutex>
#include <optional>
#include <piwo/proto.h>
#include <piwo/protodef.h>
#include <spdlog/spdlog.h>
#include <string>

#include <piwo/animation.h>
#include <piwo/frame.h>

#if defined(GetObject)
#undef GetObject
#endif

// without it compilation fails when including boost/asio
#ifdef _WIN32
#include <winsock2.h>
#endif

#include <piwo/animation.h>
#include <rapidjson/document.h>

#include "common_types.hpp"
#include "event_loop.hpp"
#include "guarded.hpp"
#include "network.hpp"
#include "piwo_core.hpp"
#include "playlist.hpp"
#include "render_engine.hpp"
#include "state.hpp"
#include "ui_iface.hpp"

#ifdef _WIN32
#undef GetObject
#endif

constexpr int MIN_RESOLUTION_WIDTH = 0;
constexpr int MAX_RESOLUTION_WIDTH = 120;

constexpr int MIN_RESOLUTION_HEIGHT = 0;
constexpr int MAX_RESOLUTION_HEIGHT = 120;

constexpr int MIN_LOGIC_ADDRESS = 0;
constexpr int MAX_LOGIC_ADDRESS = 255;

constexpr int MIN_GATEWAY_ID = 0;
constexpr int MAX_GATEWAY_ID = 255;

constexpr int MIN_RADIO_ID = 0;
constexpr int MAX_RADIO_ID = 255;

constexpr int MIN_RSSI_INTERVAL = 50;
constexpr int MAX_RSSI_INTERVAL = 1000;

constexpr unsigned int MAX_TX_COUNT = 1;
// currently we are supporting only one transmitter
static_assert(MAX_TX_COUNT == 1);

struct config_t
{
  using uresolution_t = resolution_t<size_t>;

  struct playlists_t
  {
    std::shared_ptr<playlist_t> _playlist;
    std::shared_ptr<frame_provider> _future_playlist;

    playlists_t(std::shared_ptr<playlist_t>&& playlist)
      : _playlist(std::move(playlist))
      , _future_playlist(playlist_t::get_future_frame_provider(_playlist))
    {
    }
  };

  config_t()
    : _render_engine(std::make_shared<render_engine>(1, 1))
    , _playlists(
        std::make_shared<playlist_t>(uresolution_t{ .width = 0, .height = 0 }))
  {
  }

  std::weak_ptr<frame_provider>
  weak_provider(frame_provider::provider_id id);

  auto
  acquire_providers()
  {
    auto [lck_re, _re] = this->_render_engine.get_guarded();
    auto& re = *_re.get();

    auto [lck_playlists, playlists] = this->_playlists.get_guarded();
    auto& playlist = *playlists._playlist.get();
    auto& future_playlist = *playlists._future_playlist.get();

    return std::make_tuple(

      /* tuple of locks */
      std::make_tuple(std::move(lck_re), std::move(lck_playlists)),

      /* tuple of references to frame providers */
      std::make_tuple(
        std::ref(re), std::ref(playlist), std::ref(future_playlist)));
  }

  auto
  acquire_modules()
  {
    return this->_modules.get_guarded();
  }

  auto
  acquire_resolution()
  {
    return this->_resolution.get_guarded();
  }

  auto
  acquire_playlist()
  {
    auto [lck, playlists] = this->_playlists.get_guarded();
    return std::make_tuple(std::move(lck),
                           std::ref(*playlists._playlist.get()));
  }

  auto
  acquire_animations()
  {
    auto [lock, playlists] = this->_playlists.get_guarded();
    return playlists._playlist.get()->acquire_animations();
  }

  auto
  acquire_render_engine()
  {
    auto [lck, re] = this->_render_engine.get_guarded();
    return std::make_tuple(std::move(lck), std::ref(*re.get()));
  }

  auto
  acquire_event_loop()
  {
    return this->_event_loop.get_guarded();
  }

  auto
  acquire_rssi_config()
  {
    return this->_rssi_config.get_guarded();
  }

  bool
  add_step(pipeline_config& config);
  bool
  remove_step(step_id id);
  bool
  refresh_step(step_id id);
  bool
  change_step_state(step_id id, bool state);
  void
  refresh_audio_out_list();
  void
  refresh_midi_list();
  void
  auto_configure_modules(
    [[maybe_unused]] const std::vector<piwo::uid>& uid_vec);
  void
  auto_configure_modules([[maybe_unused]] const std::vector<ipos_t>& tiles_vec);
  application_state_t
  get_application_state();
  void
  set_resolution(uresolution_t::underlying_type width,
                 uresolution_t::underlying_type height);
  void
  assign_module_position(piwo::uid& uid, ipos_t& position);
  void
  set_module_id(piwo::uid& uid, int64_t id);
  void
  set_module_logic_address(piwo::uid& uid, piwo::logic_address_t logic_address);
  void
  set_modules_gateway_id(const std::vector<piwo::uid>& uids,
                         piwo::gateway_id_t gateway_id);
  void
  set_modules_radio_id(const std::vector<piwo::uid>& uids,
                       piwo::radio_id_t radio_id);
  void
  set_modules_static_color(const std::vector<piwo::uid>& uids,
                           piwo::color static_color);
  void
  blink_modules([[maybe_unused]] const std::vector<piwo::uid>& modules);
  void
  color_modules([[maybe_unused]] const std::vector<piwo::uid>& modules);
  void
  assign_la_for_modules([[maybe_unused]] const std::vector<piwo::uid>& modules);
  void
  load_raw_modules(std::istream& f);
  void
  load_raw_modules_from_file(const char* path);
  void
  save_to_file(const char* path);
  void
  load_from_file(const char* path);
  void
  load_animation(const char* path);
  void
  enable_light_show();
  void
  disable_light_show();
  void
  enable_rssi();
  void
  disable_rssi();
  void
  start_animation();
  void
  pause_animation();
  void
  stop_animation();
  void
  change_animation_to(size_t index);
  void
  connect_launchad();
  void
  disconnect_launchad();

  std::optional<std::reference_wrapper<tx_context_t>>
  get_tx_context(size_t i);
  bool
  commit_configuration();
  void
  tx_connect(const std::string& host,
             const std::string& tcp_port,
             const std::string& udp_port);
  void
  tx_disconnect();

  void
  set_rssi_config(const rssi_config& config);

  static frame_provider::provider_id
  lookup_provider(const char* lookup)
  {
    if (std::strcmp(lookup, RENDER_ENGINE_PROVIDER_NAME) == 0)
      return frame_provider::provider_id::RENDER;

    if (std::strcmp(lookup, PLAYLIST_PROVIDER_NAME) == 0)
      return frame_provider::provider_id::PLAYLIST;

    if (std::strcmp(lookup, FUTURE_PLAYLIST_PROVIDER_NAME) == 0)
      return frame_provider::provider_id::FUTURE_PLAYLIST;

    return frame_provider::provider_id::INVALID_PROVIDER;
  }

  static std::string
  lookup_provider(const frame_provider::provider_id lookup)
  {
    if (lookup == frame_provider::provider_id::RENDER)
      return RENDER_ENGINE_PROVIDER_NAME;

    if (lookup == frame_provider::provider_id::PLAYLIST)
      return PLAYLIST_PROVIDER_NAME;

    if (lookup == frame_provider::provider_id::FUTURE_PLAYLIST)
      return FUTURE_PLAYLIST_PROVIDER_NAME;

    return "";
  }

public:
  static inline const char RENDER_ENGINE_PROVIDER_NAME[] = "render";
  static inline const char PLAYLIST_PROVIDER_NAME[] = "playlist";
  static inline const char FUTURE_PLAYLIST_PROVIDER_NAME[] = "future_playlist";

  static inline constexpr const char* DEFAULT_SOURCE_PROVIDER =
    PLAYLIST_PROVIDER_NAME;
  static inline constexpr const char* DEFAULT_DESTINATION_PROVIDER =
    RENDER_ENGINE_PROVIDER_NAME;

  static inline const std::vector<const char*> providers_names{
    RENDER_ENGINE_PROVIDER_NAME,
    PLAYLIST_PROVIDER_NAME,
    FUTURE_PLAYLIST_PROVIDER_NAME
  };

private:
  std::array<tx_context_t, MAX_TX_COUNT> _tx_conn;
  ls_sending_task _ls_task;
  rssi_sending_task _rssi_task;

  guarded<rssi_config, std::recursive_mutex> _rssi_config;
  guarded<application_state_t, std::recursive_mutex> _application_state{
    application_state_t::IDLE
  };
  guarded<uresolution_t, std::recursive_mutex> _resolution{ 0, 0 };
  guarded<modinfo_t, std::recursive_mutex> _modules;
  guarded<event_loop, std::recursive_mutex> _event_loop;

  // frame providers
  guarded<std::shared_ptr<render_engine>, std::recursive_mutex> _render_engine;
  guarded<playlists_t, std::recursive_mutex> _playlists;
};

inline config_t
  global_config; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

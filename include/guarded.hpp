#pragma once

#include <functional>
#include <initializer_list>
#include <mutex>
#include <optional>
#include <type_traits>

template<typename T, typename... U>
concept IsAnyOf = (std::same_as<T, U> || ...);

template<typename T>
concept LockStrategy =
  IsAnyOf<T, std::defer_lock_t, std::try_to_lock_t, std::adopt_lock_t>;

template<typename T>
concept is_shared_ptr =
  (std::same_as<T, std::shared_ptr<typename T::element_type>>);

template<typename ValueType, typename MutexType>
class guarded
{

public:
  using ValueRef = ValueType&;
  using LockType = std::unique_lock<MutexType>;

  struct guard_pair
  {
    LockType lock;
    ValueRef value_ref;
  };

public:
  guarded() = default;

  guarded(ValueType&& other)
    : value(std::move(other))
    , mutex()
  {
  }

  template<typename... Args>
    requires is_shared_ptr<ValueType>
  guarded(Args&&... args)
    : value(std::make_shared<typename ValueType::element_type>(
        std::forward<Args>(args)...))
    , mutex()
  {
  }

  template<typename... Args>
    requires std::is_enum_v<ValueType>
  guarded(Args&&... args)
    : value(std::initializer_list<Args...>(std::forward<Args>(args)...))
    , mutex()
  {
  }

  template<typename... Args>
  guarded(Args&&... args)
    : value(std::forward<Args>(args)...)
    , mutex()
  {
  }

  guarded(const ValueType& other) = delete;
  guarded(guarded&& other) = delete;

  guarded&
  operator=(const guarded&) = delete;

  guarded&
  operator=(const guarded&&) = delete;

  [[nodiscard]] auto
  get_guarded()
  {
    return guard_pair{ .lock = LockType(this->mutex),
                       .value_ref = this->value };
  }

  template<std::invocable<ValueType&> C>
  void
  locked_scope(C&& body)
  {
    LockType lock(mutex);
    body(this->value);
  }

  std::optional<guard_pair>
  try_lock()
  {
    const bool status = this->mutex.try_lock();

    if (status)
      return guard_pair{ .lock = LockType(this->mutex, std::adopt_lock),
                         .value_ref = this->value };
    return std::nullopt;
  }

private:
  [[nodiscard]] ValueRef
  get_value_unsafe_()
  {
    /*
     * Possibly insert instrumentation
     * to track if we are actually using the unguarded version correctly.
     *
     * TODO: add DummyLockType that wraps mutex without locking it,
     * but can be used to track if we don't ever have a problem of
     * having reference to guarded and unguarded object at the same
     * time.
     */
    return this->value;
  }

  [[nodiscard]] MutexType&
  get_mutex_unsafe_()
  {
    return this->mutex;
  }

  template<LockStrategy Strategy>
  [[nodiscard]] LockType
  get_lock_unsafe_(Strategy strat)
  {
    return LockType(this->mutex, strat);
  }

private:
  ValueType value;
  MutexType mutex;

  friend auto
  scoped_lock_try(auto&&... args);

  template<typename T>
  friend std::optional<T>
  _scoped_lock_try(auto&&... args);
};

auto
scoped_lock_try(auto&&... args)
{
  using LockCollection = decltype(std::make_tuple(
    (std::forward<decltype(args)>(args).get_lock_unsafe_(std::adopt_lock))...));

  using ValuesCollection = decltype(std::make_tuple(
    (std::ref(std::forward<decltype(args)>(args).get_value_unsafe_()))...));

  using OptionalArg = std::tuple<LockCollection, ValuesCollection>;

  return _scoped_lock_try<OptionalArg>((std::forward<decltype(args)>(args))...);
}

template<typename T>
static std::optional<T>
_scoped_lock_try(auto&&... args)
{
  int status =
    std::try_lock((std::forward<decltype(args)>(args).get_mutex_unsafe_())...);

  if (status == -1)
  {
    return std::make_tuple(
      /* Tuple of locks to resources */
      std::make_tuple((std::forward<decltype(args)>(args).get_lock_unsafe_(
        std::adopt_lock))...),

      /* Tuple of references to resources */
      std::make_tuple(
        (std::ref(std::forward<decltype(args)>(args).get_value_unsafe_()))...));
  }

  return std::nullopt;
}

#pragma once
#include <mutex>

#include <piwo/frame.h>
#include <piwo/frames_intersection.h>

class frame_provider
{

public:
  enum class provider_id
  {
    RENDER,
    PLAYLIST,
    FUTURE_PLAYLIST,
    INVALID_PROVIDER,
  };

  frame_provider(bool is_waitable, provider_id id)
    : is_waitable(is_waitable)
    , _id(id)
  {
  }

  virtual ~frame_provider() = default;

  provider_id
  get_id() const
  {
    return this->_id;
  }

  void
  resize(size_t width, size_t height)
  {
    resize_(width, height);
  }

  piwo::frames_intersection::frame_t
  get_frame()
  {
    return get_frame_();
  }

  bool
  waitable()
  {
    return is_waitable;
  }

  virtual void
  wait()
  {
    assert(false && "Frame provider does not implement the wait function");
  }

  bool is_waitable;

protected:
  virtual piwo::frames_intersection::frame_t
  get_frame_() = 0;

  virtual void
  resize_(size_t width, size_t height) = 0;

private:
  const provider_id _id;
};

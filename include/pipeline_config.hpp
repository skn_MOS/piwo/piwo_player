#pragma once
#include <memory>
#include <optional>
#include <piwo/frames_intersection.h>
#include <string>

#include "frame_provider.hpp"
#include "json.hpp"

using step_id = int64_t;
static constexpr step_id INVALID_STEP_ID = -1;

struct pipeline_config
{
  std::string source_provider_name;
  std::string destination_provider_name;
  int64_t off_x, off_y;
  std::string effect;
  step_id pipeline_id = INVALID_STEP_ID; // changed only when pipeline is
                                         // correctly added
  bool is_highlighted = false;
  bool state = true;

  bool
  operator==(const step_id id);
};

struct render_engine_step
{
  static inline step_id next_id = 0;
  friend class render_engine;
  friend void
  serialize<>(render_engine_step& s, serialization_writer& writer);

private:
  render_engine_step(std::weak_ptr<frame_provider> fp1_w,
                     std::weak_ptr<frame_provider> fp2_w,
                     const piwo::frames_intersection::frame_t& f1,
                     const piwo::frames_intersection::frame_t& f2,
                     int64_t offx,
                     int64_t offy,
                     piwo::frames_intersection_operator e);

public:
  static std::optional<render_engine_step>
  make_step(std::weak_ptr<frame_provider> fp1_w,
            std::weak_ptr<frame_provider> fp2_w,
            int64_t offx,
            int64_t offy,
            piwo::frames_intersection_operator e);

  bool
  operator==(const step_id id);

  bool
  refresh();

  step_id id;
  int64_t off_x, off_y;
  bool active;
  piwo::frames_intersection_operator effect;

private:
  piwo::frames_intersection _intersection;
  std::weak_ptr<frame_provider> _source_provider, _destination_provider;
};

#pragma once
#include "bass.h"
#include <cstdint>

class audio
{
public:
  audio() = default;
  bool
  init_stream(int device);
  bool
  load_music(const char* file_name);
  void
  set_music_position(uint64_t miliseconds);

  void
  play() const
  {
    BASS_ChannelPlay(_stream, false);
  }

  void
  pause() const
  {
    BASS_ChannelPause(_stream);
  }

  void
  stop();

  void
  set_volume(float volume);

  void
  free_stream();

  inline bool
  running()
  {
    return BASS_ChannelIsActive(this->_stream) == BASS_ACTIVE_PLAYING;
  }

  inline bool
  inactive()
  {
    return BASS_ChannelIsActive(this->_stream) == BASS_ACTIVE_STOPPED;
  }

public:
  static constexpr float DEFAULT_AUDIO_VOL_FLOAT = 0.5;

private:
  HSTREAM _stream = -1;
  int _devic_id = -1;
  bool _is_music_loaded{ false };
  float _volume = DEFAULT_AUDIO_VOL_FLOAT;
};

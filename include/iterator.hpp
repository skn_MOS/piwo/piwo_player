#pragma once

#include <iterator>

template<typename Iter, typename T>
Iter
next(Iter it, T offset)
{
  if constexpr (std::is_signed_v<T>)
    return it + offset;
  else
    return it +
           static_cast<typename std::iterator_traits<Iter>::difference_type>(
             offset);
}

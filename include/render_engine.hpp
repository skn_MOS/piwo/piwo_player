#pragma once
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <piwo/common_intersection_operators.h>
#include <vector>

#include <mipc/timer.h>
#include <piwo/frame.h>
#include <piwo/frames_intersection.h>

#include "frame_provider.hpp"
#include "guarded.hpp"
#include "json.hpp"
#include "pipeline_config.hpp"

class render_engine : public frame_provider
{
  static constexpr auto SEC = std::chrono::seconds{ 0 };
  static constexpr auto MILLI = std::chrono::milliseconds{ 50 };

public:
  enum class state_t
  {
    re_busy,
    re_failed,
    re_ok,
  };

  struct render_ctx
  {
    bool _rendered = false;
    bool _render_active = false;
    std::vector<render_engine_step> _steps;
  };

  render_engine(size_t rows, size_t cols);

  ~render_engine();

  void
  stop();

  // start with last saved time
  void
  start();

  void
  start(const std::chrono::seconds& sec, const std::chrono::milliseconds& mili);

  [[nodiscard]] state_t
  blocking_add(const render_engine_step& config);

  bool
  is_active();

  [[nodiscard]] state_t
  blocking_refresh(step_id id);

  [[nodiscard]] state_t
  blocking_remove(step_id id);

  void
  serialize(serialization_writer& writer);

  [[nodiscard]] state_t
  add_step(const render_engine_step& config);

  [[nodiscard]] state_t
  remove_step(step_id id);

  void
  clear_steps();

  [[nodiscard]] state_t
  refresh_step(step_id id);

  [[nodiscard]] state_t
  set_step_state(step_id id, bool state);

  void
  render();

  piwo::frames_intersection::frame_t
  get_frame_() override;

  void
  resize_(size_t width, size_t height) override;

  void
  wait() override;

private:
  guarded<std::shared_ptr<piwo::frame>, std::mutex> _frame;
  std::condition_variable_any _cv;
  guarded<render_ctx, std::recursive_mutex> _ctx;

  mipc::timer _tim;
  std::thread _th;

  std::chrono::seconds _sec = SEC;
  std::chrono::milliseconds _mili = MILLI;
};

#pragma once
#include <cstddef>
#include <cstdint>

template<typename T>
struct position
{
  using underlying_type = T;
  T x;
  T y;
};

using ipos_t = position<int32_t>; // TODO(all) perhaps change to ssize_t
using upos_t = position<std::size_t>;

template<typename T>
struct resolution_t
{
  using underlying_type = T;
  T width;
  T height;
};

using ires_t = resolution_t<int32_t>; // TODO(all) perhaps change to ssize_t
using ures_t = resolution_t<std::size_t>;

using animation_id_t = size_t;

#pragma once
#include <memory>
#include <set>

#include <piwo/frames_intersection.h>

#include "modinfo.hpp"
#include "network.hpp"

struct modinfo_set_comparator
{
  bool
  operator()(const modinfo& lhs, const modinfo& rhs) const
  {
    return lhs.base.uid.packed() < rhs.base.uid.packed();
  }
};

using modinfo_t = std::set<modinfo, modinfo_set_comparator>;
using tx_configuration_t = std::vector<tx_config>;

// the funtion returns grouped modules by tx_id
ls_sending_task::ls_config_array
own_offsets(modinfo_t& modules, const std::shared_ptr<piwo::frame> frame);

void
configure_logic_address(modinfo_t& modules,
                        ls_sending_task::ls_config_array& conf,
                        size_t width);

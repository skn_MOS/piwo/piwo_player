#pragma once

#include <piwo/common_intersection_operators.h>
#include <piwo/frames_intersection.h>

// here you can implement common frame operators
// see libpiwo -> common_intersection_operators file to see the examples

static const char COPY_ASSIGN[] = "copy (= operator)";
static const char COPY_OR[] = "copy (| operator)";
static const char COPY_AND[] = "copy (& operator)";
static const char SUBSTRACT[] = "substract";

inline constexpr const char* DEFAULT_EFFECT = COPY_ASSIGN;

static constexpr piwo::frames_intersection_operator frame_assign_op =
  piwo::frame_assign_op;

static constexpr piwo::frames_intersection_operator frame_or_op =
  piwo::frame_or_op;

static constexpr piwo::frames_intersection_operator frame_add_op =
  piwo::frame_add_op;

static constexpr piwo::frames_intersection_operator frame_sub_op =
  piwo::frame_sub_op;

const std::vector<const char*> global_effects_list{ COPY_ASSIGN,
                                                    COPY_OR,
                                                    COPY_AND,
                                                    SUBSTRACT };

inline piwo::frames_intersection_operator
lookup_effect(const char* const lookup)
{
  if (std::strcmp(lookup, COPY_ASSIGN) == 0)
    return frame_assign_op;

  if (std::strcmp(lookup, COPY_OR) == 0)
    return frame_or_op;

  if (std::strcmp(lookup, COPY_AND) == 0)
    return frame_add_op;

  if (std::strcmp(lookup, SUBSTRACT) == 0)
    return frame_sub_op;

  return nullptr;
}

inline std::string
lookup_effect(piwo::frames_intersection_operator lookup)
{
  if (lookup == frame_assign_op)
    return COPY_ASSIGN;

  if (lookup == frame_or_op)
    return COPY_OR;

  if (lookup == frame_add_op)
    return COPY_AND;

  if (lookup == frame_sub_op)
    return SUBSTRACT;

  return "";
}

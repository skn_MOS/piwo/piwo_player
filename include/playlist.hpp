#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include <mipc/timer.h>
#include <piwo/animation.h>
#include <spdlog/spdlog.h>

#include "audio.hpp"
#include "common_types.hpp"
#include "frame_provider.hpp"
#include "midi.hpp"
#include "typeutils.hpp"
#include "ui_iface.hpp"

struct config_t;

namespace
{
constexpr int64_t PLAYLIST_INVALID_ANIMATION_INDEX = -1;
}

struct named_animation
{
  animation_id_t id;
  std::string name;
  piwo::animation anim;
};

class playlist_t : public frame_provider
{
  using anim_vector = std::vector<named_animation>;

public:
  playlist_t(resolution_t<size_t>);
  ~playlist_t();

  void
  framerate_thread_handler();

  void
  reset_current_animation();

  bool
  next();

  bool
  prev();

  bool
  set_animation(size_t index);

  bool
  set_frame_no(size_t frame_no);

  std::optional<size_t>
  get_current_animation_index();

  std::optional<piwo::animation>
  get_current_animation();

  size_t
  get_current_animation_frame_no();

  size_t
  size()
  {
    auto [lock, anims] = this->_animations.get_guarded();
    return anims.size();
  }

  auto
  acquire_animations()
  {
    return this->_animations.get_guarded();
  }

  bool
  is_running()
  {
    return this->_frame_rate_timer.is_active();
  }

  void
  connect_audio(int handle);

  void
  connect_midi(int handle, int base_offset)
  {
    this->_lasers.connect(handle, base_offset);
    midi_connection_status_changed(true);
  }

  void
  disconnect_midi()
  {
    this->_lasers.disconnect();
    midi_connection_status_changed(false);
  }

  void
  set_sound_volume(float volume)
  {
    this->_music_player.set_volume(volume);
  }

  void
  disconnect_audio()
  {
    _music_player.free_stream();
    audio_out_connection_status_changed(false);
  }

  void
  wait() override;

  bool
  start();

  void
  pause();

  void
  stop();

  void
  change_animation_index(size_t from, size_t to);

  void
  remove_animation_by_index(size_t index);

  void
  remove_animation_by_id(animation_id_t id);

  std::optional<size_t>
  get_animation_index(animation_id_t id);

  static std::shared_ptr<frame_provider>
  get_future_frame_provider(std::shared_ptr<playlist_t>& playlist_parent)
  {
    return std::shared_ptr<frame_provider>(
      playlist_parent, &playlist_parent->_future_frame_store);
  }

private:
  piwo::frames_intersection::frame_t
  get_frame_() override;

  virtual void
  resize_(size_t width, size_t height) override;

  void
  buffer_resolution_check(const named_animation& animation);

  void
  internal_reset_non_blocking();

  void
  internal_start_thread_if_not_running();

  void
  internal_stop_thread_and_wait();

  bool
  load_music_for_animation(const named_animation& anim);

  bool
  add_animation(named_animation anim);

  auto
  get_current_animation_frame_count();

private:
  friend struct config_t;

  class frame_store_t : public frame_provider
  {
    friend playlist_t;

  public:
    frame_store_t()
      : frame_provider(true, provider_id::FUTURE_PLAYLIST)
    {
      this->_frame = piwo::alloc_frame_shared(1, 1);
    }

    void
    wait() override
    {
      auto* parent = container_of(this, &playlist_t::_future_frame_store);
      parent->wait();
    }

  private:
    piwo::frames_intersection::frame_t
    get_frame_() override
    {
      return this->_frame;
    }

    void
    resize_(size_t width, size_t height) override
    {
      this->_frame = piwo::alloc_frame_shared(width, height);
    }

  private:
    piwo::frames_intersection::frame_t _frame;
  };

  suspendable_thread _frame_rate_thread;
  mipc::timer _frame_rate_timer;

  int64_t _current_animation = PLAYLIST_INVALID_ANIMATION_INDEX;
  size_t _current_frame = 0;

  guarded<anim_vector, std::recursive_mutex> _animations;

  struct framesync_data
  {
    bool _rendered = false;
    bool _is_finished = false;
  };

  guarded<framesync_data, std::mutex> _framesync;
  std::condition_variable _cv;

  piwo::frames_intersection::frame_t _frame_buffer_ptr;
  frame_store_t _future_frame_store;
  audio _music_player;
  midi _lasers;

  size_t last_animation_id = 0;
};

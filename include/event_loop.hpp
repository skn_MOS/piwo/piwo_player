#pragma once

#include <condition_variable>
#include <mutex>
#include <queue>
#include <thread>
#include <variant>

#include "guarded.hpp"

class event_loop
{
public:
  struct resize_data
  {
    size_t width, height;
  };

  struct eth_conn_data
  {
    std::string ip, tcp_port, udp_port;
  };

  struct eth_disconn_data
  {
  };

  struct finish_loop
  {
  };

  using event_variant =
    std::variant<resize_data, eth_conn_data, eth_disconn_data, finish_loop>;

  using queue_t = std::queue<event_variant>;

  void
  send_event(event_variant event);

  event_loop();

  ~event_loop();

private:
  void
  process_events();

  void
  handle_resize(size_t width, size_t height);

  void
  handle_tx_connect(const std::string& ip,
                    const std::string& tcp_port,
                    const std::string& udp_port);

  void
  handle_tx_disconnect();

  event_variant
  pop_event();

  std::condition_variable _cv;
  std::thread _th;

  guarded<queue_t, std::mutex> _events;
};

#pragma once

#include <memory>
#include <rtmidi/RtMidi.h>

class midi
{
  constexpr static int MIDI_HEADER = 0x90;
  constexpr static int LASER_TRIGGER_START = 100;
  constexpr static int LASER_TRIGGER_STOP = 50;

public:
  midi();
  midi(midi&&) = delete;
  midi(const midi&) = delete;
  midi&
  operator=(const midi&) = delete;
  midi&
  operator=(midi&&) = delete;
  ~midi() = default;

  void
  connect(int device, int base_offset);

  void
  disconnect();

  void
  start_lasers(uint8_t animation_offset);

  void
  stop_lasers(uint8_t animation_offset);

  bool
  is_connected();

private:
  uint8_t _base_offset = 0;
  bool _connected = false;
  std::unique_ptr<RtMidiOut> _midiout;
};

#pragma once

#include <boost/asio/ip/udp.hpp>
#include <list>
#include <memory>
#include <mutex>
#include <optional>
#include <piwo/protodef.h>
#include <string>
#include <thread>
#include <vector>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/use_future.hpp>
#include <boost/asio/write.hpp>

#include <spdlog/spdlog.h>

#include <piwo/animation.h>
#include <piwo/color.h>
#include <piwo/frame.h>
#include <piwo/proto.h>

#include "frame_provider.hpp"
#include "guarded.hpp"
#include "suspendable_thread.hpp"

#include "test_iface.hpp"

constexpr size_t FORAWRDING_BUFFER_SIZE = 1450;
static_assert(FORAWRDING_BUFFER_SIZE >
              piwo::forward_w_cid_without_any_packet_length);

using packet_t = piwo::raw_packet;

class network_packet_queue
{
public:
  using buffer_t = std::basic_string<std::byte>;
  using queue_t = std::list<buffer_t>;
  using iterator_t = queue_t::iterator;

public:
  iterator_t
  add(const packet_t& packet);

  void remove_pending(iterator_t);

private:
  buffer_t
  make_network_packet_buffer(size_t size);

private:
  guarded<queue_t, std::mutex> queue;
};

struct network_packet_write_handler
{
  void
  operator()(boost::system::error_code error, std::size_t bytes_written);

  static network_packet_write_handler
  make_write_packet_handler(network_packet_queue::iterator_t node_it,
                            const std::shared_ptr<network_packet_queue>&);

  std::weak_ptr<network_packet_queue> queue;
  network_packet_queue::iterator_t packet_node;
};

struct tx_config
{
  /* here you can pass other tx configuration variables */
  unsigned int radios_on_board;
};

static unsigned long
to_piwo_frame_pos(const piwo::frame& frame, size_t x, size_t y)
{
  return x + frame.width * ((frame.height - 1) - y);
}

struct tx_pixel_owner_info
{
  using offset_array_t = std::vector<size_t>;

  void
  own(const piwo::frame& frame, size_t x, size_t y)
  {
    owned_offsets.emplace_back(x + frame.width * y);
    color_offsets.emplace_back(to_piwo_frame_pos(frame, x, y));
  }

  void
  clear_owned()
  {
    this->owned_offsets.clear();
    this->color_offsets.clear();
  }

  // TODO change player coordinates
  // so the origin will be placed in bottom-left corner.
  // then remove color_offset variable
  offset_array_t owned_offsets; // offset for logical addressa
  // reversed offset for color
  // piwo::frame colors are refelcted accros the y axis
  // our (0,0) pixel is in left top corrner, but the piwo::frame
  // (0,0) pixel is placed in left bottom corrner
  offset_array_t color_offsets;
};

struct lightshow_config
{
  static constexpr uint8_t DEFAULT_TTF = 50;
  static constexpr auto UNDEFINED_ID = std::numeric_limits<uint32_t>::max();

  struct owned_pixel_info
  {
    size_t real_offset;
    piwo::color color;
  };

  struct config_id
  {
    unsigned int tx, radio;
  };

  [[nodiscard]] std::optional<owned_pixel_info>
  get_nth(size_t n) const
  {
    if (n >= this->owner_info.color_offsets.size())
      return std::nullopt;

    const auto pixel_offset = this->owner_info.color_offsets[n];
    return owned_pixel_info{ .real_offset = this->device_offset + n,
                             .color = this->frame->at_flat_(pixel_offset) };
  }

  size_t device_offset = 0;
  tx_pixel_owner_info owner_info;
  config_id id = { UNDEFINED_ID, UNDEFINED_ID };
  std::shared_ptr<piwo::frame> frame;
};

struct rssi_config
{
  uint8_t tx = 0;
  uint8_t radio = 0;
  unsigned int interval{ 1000 };
};

class tx_eth_connection
{
public:
  using mutex_t = std::mutex;
  using tcp_socket_t = boost::asio::ip::tcp::socket;
  using udp_socket_t = boost::asio::ip::udp::socket;

  static inline const char DEFAULT_IP_STRING[] = "192.168.1.12";
  static inline const char DEFAULT_PORT_TCP_STRING[] = "5556";
  static inline const char DEFAULT_PORT_UDP_STRING[] = "5555";

  friend struct network_packet_write_handler;

public:
  tx_eth_connection()
    : _tcp_io_context()
    , _udp_io_context()
    , _tcp_socket(this->_tcp_io_context)
    , _udp_socket(this->_udp_io_context)
    , _udp_io_context_runner(&tx_eth_connection::udp_io_context_runner_loop,
                             this)
    , _tcp_io_context_runner(&tx_eth_connection::tcp_io_context_runner_loop,
                             this)
  {
    // TODO move tx configuration into function that will
    // read the config from TX using piwo protcol
    this->_config = { .radios_on_board = 4 };

    this->_udp_queue = std::make_shared<network_packet_queue>();
  }

  tx_eth_connection(const tx_eth_connection& other) = delete;
  tx_eth_connection(tx_eth_connection&& other) = delete;

  tx_eth_connection&
  operator=(const tx_eth_connection& other) = delete;
  tx_eth_connection&
  operator=(tx_eth_connection&& other) = delete;

  ~tx_eth_connection();

  bool
  open(const char* host, const char* tcp_port, const char* udp_port);

  bool
  tcp_send(packet_t packet);

  void
  close();

  void
  start();

  [[nodiscard]] bool
  is_opened() const
  {
    return this->_tcp_socket.is_open();
  }

  TEST_IFACE void
  enqueue(packet_t packet);

  const tx_config&
  get_config()
  {
    return this->_config;
  }

private:
  void
  start_io_workers();

  void
  udp_io_context_runner_loop()
  {
    using work_guard_type =
      boost::asio::executor_work_guard<boost::asio::io_context::executor_type>;

    work_guard_type work_guard(this->_udp_io_context.get_executor());

    this->_udp_io_context.restart();
    this->_udp_io_context.run();
  }

  void
  tcp_io_context_runner_loop()
  {
    using work_guard_type =
      boost::asio::executor_work_guard<boost::asio::io_context::executor_type>;

    work_guard_type work_guard(this->_tcp_io_context.get_executor());

    this->_tcp_io_context.restart();
    this->_tcp_io_context.run();
  }

private:
  boost::asio::io_context _tcp_io_context;
  boost::asio::io_context _udp_io_context;

  boost::asio::ip::udp::endpoint _udp_endpoint;

  tcp_socket_t _tcp_socket;

  // TODO: provide mutexless packet queue?
  guarded<udp_socket_t, mutex_t> _udp_socket;

  std::shared_ptr<network_packet_queue> _udp_queue;
  tx_config _config;

  suspendable_thread _udp_io_context_runner;
  suspendable_thread _tcp_io_context_runner;
};

// use tamplete to enable testing
// TODO change template to use wrapped sockets
// so we will be able to test the class with mocked socket
// implemetnation
template<typename Conn, size_t buffer_size>
struct tx_context
{
  tx_context()
    : raw_packet(packet_buffer.data(), packet_buffer.size())
    , forward_builder(
        piwo::forward_w_cid_builder::make_forward_w_cid_builder(raw_packet)
          .value())
  {
    static_assert(buffer_size >=
                  (piwo::forward_w_cid_without_any_packet_length +
                   piwo::lightshow_buffer_minimum_length));
  }

  std::array<piwo::net_byte_t, buffer_size> packet_buffer{};
  piwo::raw_packet raw_packet;
  piwo::forward_w_cid_builder forward_builder;

  Conn connection;

  // function creates and stores the lightshow packet
  // or sends buffer when the packet is larger than buffer
  // and then it conitunes creating the packet
  void
  add_or_send_lightshow(const lightshow_config& config)
  {
    constexpr uint8_t ttf = lightshow_config::DEFAULT_TTF;

    for (size_t pixel_offset = 0u;;)
    {
      std::optional buff_slot_opt = this->forward_builder.get_next_slot();
      // no more memory send packet and continue
      if (!buff_slot_opt.has_value()) [[unlikely]]
      {
        forward_packet_and_reset_builder();
        continue;
      }

      auto buff_slot = this->trim_packet(buff_slot_opt.value(),
                                         piwo::lightshow_buffer_maximum_length);

      std::optional ls_packet_opt =
        this->prepare_lightshow_packet(config, buff_slot, pixel_offset, ttf);

      if (!ls_packet_opt.has_value()) [[unlikely]]
      {
        this->forward_packet_and_reset_builder();
        continue;
      }

      auto& ls_packet = *ls_packet_opt;

      pixel_offset += ls_packet.get_colors_count();

      if (ls_packet.get_colors_count() == 0)
        break;

      this->forward_builder.commit(ls_packet, config.id.radio);
    }
  }

  void
  send_rssi(const rssi_config& config)
  {
    std::optional buff_slot_opt = this->forward_builder.get_next_slot();

    if (!buff_slot_opt.has_value()) [[unlikely]]
    {
      forward_packet_and_reset_builder();
      return;
    }

    auto buff_slot =
      this->trim_packet(buff_slot_opt.value(), piwo::ping_length);

    auto ping_builder_opt = piwo::ping_builder::make_ping_builder(buff_slot);

    if (!ping_builder_opt.has_value()) [[unlikely]]
    {
      spdlog::critical(
        "Failed to create ping builder. This should never happen");
      flush();
      return;
    }

    auto ping_builder = ping_builder_opt.value();

    // Rssi is send via UPD
    ping_builder.set_seq(0);

    // Broadcast uid
    std::optional uid_opt =
      piwo::uid::from_ascii("FF:FF:FF:FF:FF:FF:FF:FF:FF:FF:FF:FF");

    if (!uid_opt.has_value()) [[unlikely]]
    {
      spdlog::critical(
        "Failed to create broadcast uid. This should never happen");
      flush();
      return;
    }

    auto uid = uid_opt.value();

    ping_builder.set_uid(uid);

    auto ping_packet = piwo::ping(ping_builder);

    this->forward_builder.commit(ping_packet, config.radio);
    this->flush();
  }

  void
  flush()
  {
    this->forward_packet_and_reset_builder();
  }

private:
  void
  forward_packet_and_reset_builder()
  {
    auto forward = piwo::forward_w_cid(this->forward_builder);
    if (forward.size() != piwo::forward_w_cid_without_any_packet_length)
    {
      this->connection.enqueue(
        piwo::raw_packet(forward.data(), forward.size()));
    }
    std::optional forward_builder_opt =
      piwo::forward_w_cid_builder::make_forward_w_cid_builder(this->raw_packet);

    this->forward_builder = forward_builder_opt.value();
  }

  piwo::raw_packet
  trim_packet(piwo::raw_packet p, size_t s)
  {
    return piwo::raw_packet(p.data(), std::min(s, p.size()));
  }

  std::optional<piwo::lightshow>
  prepare_lightshow_packet(const lightshow_config& config,
                           piwo::raw_packet raw_packet,
                           const size_t pixel_offset,
                           const uint8_t ttf)
  {
    std::optional opt_ls_builder =
      piwo::lightshow_builder::make_lightshow_builder(raw_packet);

    if (!opt_ls_builder.has_value()) [[unlikely]]
    {
      spdlog::error(
        "Unexpected internal error: make_lightshow_builder with the "
        "local packet buffer failed.");
      return std::nullopt;
    }

    auto& ls_builder = opt_ls_builder.value();
    ls_builder.set_ttf(ttf);
    ls_builder.set_first_la(config.device_offset + pixel_offset);

    for (size_t i = pixel_offset;; ++i)
    {
      std::optional pixel_info = config.get_nth(i);

      if (!pixel_info.has_value())
        break;

      const piwo::color& color = pixel_info.value().color;
      piwo::packed_color packed_color(color.r, color.g, color.b);
      if (!ls_builder.add_color(packed_color))
        break;
    }

    return piwo::lightshow(ls_builder);
  }
};

using tx_context_t = tx_context<tx_eth_connection, FORAWRDING_BUFFER_SIZE>;

class ls_sending_task
{
public:
  using ls_config_array = std::vector<lightshow_config>;
  using ms_t = std::chrono::milliseconds;
  static constexpr ms_t DEFAULT_FRAME_DURATION{ 1000 };

  void
  stop();

  bool
  stop_and_configure(std::shared_ptr<frame_provider> p,
                     ls_config_array configs);

  bool
  is_configured()
  {
    if (!this->_provider)
      return false;
    return true;
  }

  bool
  run()
  {
    if (!is_configured())
      return false;
    this->_ls_send_task.resume();
    return true;
  }

private:
  void
  ls_send_loop();

private:
  ls_config_array _configs;
  std::shared_ptr<frame_provider> _provider;
  ms_t _frame_duration{ DEFAULT_FRAME_DURATION };

  suspendable_thread _ls_send_task{ [this] { this->ls_send_loop(); } };
};

class rssi_sending_task
{
public:
  using ms_t = std::chrono::milliseconds;

  void
  stop();

  void
  stop_and_configure(rssi_config config);

  void
  run()
  {
    this->_rssi_send_task.resume();
  }

private:
  void
  rssi_send_loop();

private:
  rssi_config _config;

  suspendable_thread _rssi_send_task{ [this] { this->rssi_send_loop(); } };
};

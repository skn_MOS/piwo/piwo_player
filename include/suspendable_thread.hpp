#pragma once

#include <condition_variable>
#include <mutex>
#include <thread>

#include <spdlog/spdlog.h>

#ifdef __WIN32
#include <processthreadsapi.h>
#elif __linux__
#include <signal.h>
#endif

#include "guarded.hpp"

class synch_states
{
private:
  using event_t = std::condition_variable;
  using mutex_t = std::mutex;

  // Event that tells synchronized object to go to sleep
  event_t wake_up_event;

  // Event that tells user of synchronizer that object is sleeping
  event_t going_sleep_event;

  // Event that tells user of synchronizer that object is waking up
  event_t waking_up_event;

  bool stop_requested{ true };
  bool stop_done{ false };
  bool alive{ true };

public:
  void
  stop()
  {
    this->stop_requested = true;
  }

  void
  stop_and_wait(std::unique_lock<mutex_t>& lck)
  {
    // Mark stop request
    this->stop();

    // Wait for thread to go sleep
    this->going_sleep_event.wait(lck,
                                 [this]() -> bool { return this->stop_done; });
  }

  void
  resume()
  {
    this->stop_requested = false;
    this->wake_up_event.notify_all();
  }

  void
  resume_and_wait(std::unique_lock<mutex_t>& lck)
  {
    this->resume();

    this->waking_up_event.wait(lck, [this] { return !this->stop_done; });
  }

  bool
  is_stopped()
  {
    return this->stop_done;
  }

  void
  wait(std::unique_lock<mutex_t>& lck)
  {
    if (this->stop_requested)
    {
      // Mark sleep state
      this->stop_done = true;

      // Notify all that is waiting for this thread to sleep
      this->going_sleep_event.notify_all();

      // Wait until someone wakes this thread, wait will also
      // release event mutex
      this->wake_up_event.wait(lck, [this] { return !this->stop_requested; });

      this->stop_done = false;
      this->waking_up_event.notify_all();
    }
  }

  bool
  is_stop_requested()
  {
    return this->stop_requested;
  }

  void
  kill()
  {
    this->alive = false;
    this->resume();
  }

  bool
  is_alive()
  {
    return this->alive;
  }

  void
  on_thread_exit()
  {
    this->stop_done = true;
    this->going_sleep_event.notify_all();
  }
};

class synchronizable
{
private:
  using mutex_t = std::mutex;

  guarded<synch_states, mutex_t> event;

public:
  void
  stop()
  {
    this->event.locked_scope([](synch_states& state) { state.stop(); });
  }

  void
  stop_and_wait()
  {

    auto [lock, state] = this->event.get_guarded();
    state.stop_and_wait(lock);
  }

  void
  resume()
  {
    this->event.locked_scope([](synch_states& state) { state.resume(); });
  }

  void
  resume_and_wait()
  {
    auto [lock, state] = this->event.get_guarded();
    state.resume_and_wait(lock);
  }

  bool
  is_stopped()
  {
    auto [lock, state] = this->event.get_guarded();
    return state.is_stopped();
  }

  void
  wait()
  {
    auto [lock, state] = this->event.get_guarded();
    state.wait(lock);
  }

  bool
  is_stop_requested()
  {
    auto [lock, state] = this->event.get_guarded();
    return state.is_stop_requested();
  }

  void
  kill()
  {
    this->event.locked_scope([](synch_states& state) { state.kill(); });
  }

  bool
  is_alive()
  {
    auto [lock, state] = this->event.get_guarded();
    return state.is_alive();
  }

  // base class is responsible for
  // invoking this function in worker thread
  // on thread exit
  void
  on_thread_exit()
  {
    this->event.locked_scope([](synch_states& state)
                             { state.on_thread_exit(); });
  }
};

class suspendable_thread : public synchronizable
{
private:
  std::thread worker;

public:
  suspendable_thread() = default;

  template<class T, class... Args>
  suspendable_thread(T&& callable, Args&&... args)
  {
    this->worker = std::thread(
      [this](T&& callable, Args&&... args)
      {
        while (1)
        {
          this->wait();

          if (!this->is_alive())
            break;

          std::invoke(std::forward<T>(callable), std::forward<Args>(args)...);
        }
        this->on_thread_exit();
      },
      std::forward<T>(callable),
      std::forward<Args>(args)...);
  }

  suspendable_thread&
  operator=(const suspendable_thread& thread) = delete;
  suspendable_thread&
  operator=(suspendable_thread&& thread) = delete;

  ~suspendable_thread()
  {
    this->kill();
    if (this->worker.joinable())
      this->worker.join();
  }

  void
  join() noexcept
  {
    worker.join();
  }

  auto
  native_handle() noexcept
  {
    return this->worker.native_handle();
  }

  void
  _terminate() noexcept
  {
#ifdef __WIN32
    ::TerminateThread(pthread_gethandle(this->worker.native_handle()), 1);
#elif __linux__
    ::pthread_kill(this->worker.native_handle(), SIGUSR1);
#endif
    this->worker.detach();
  }
};

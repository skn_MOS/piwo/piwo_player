#pragma once

#include "common_types.hpp"
#include "typeutils.hpp"
#include <piwo/protodef.h>

#include <spdlog/spdlog.h>

#include "network.hpp"
#include "pipeline_config.hpp"

#ifdef _WIN32
#undef GetObject
#endif

enum class application_state_t;

void
start_ui(int argc, char** argv);

/* General global config interface */

void
config_module_added(const piwo::uid uid);

void
config_module_removed(const piwo::uid uid);

void
config_module_data_changed(const piwo::uid uid);

void
modules_config_container_rearanged();

void
config_resolution_changed();

void
application_state_changed(const application_state_t state);

/* Player main display interface */

void
display_animation_changed();

void
playlist_changed();

void
playlist_animation_added(const animation_id_t id);

void
playlist_animation_removed(const animation_id_t id);

void
playlist_animation_index_changed(const animation_id_t id);

void
light_show_started();

void
light_show_stopped();

void
rssi_started();

void
rssi_stopped();

void
animation_started();

void
animation_paused();

void
animation_stopped();

void
audio_in_list_changed(const audio_list devices);

void
audio_out_list_changed(const audio_list devices);

void
audio_out_connection_status_changed(bool state);

void
midi_list_changed(const midi_list devices);

void
midi_connection_status_changed(bool state);

void
render_engine_state_chnaged(bool);

void
render_engine_configured(const pipeline_config config);

void
render_engine_step_removed(step_id id);

void
render_engine_step_state_changed(step_id id, bool state);

/* Ethernet configuration */
void
eth_state_changed(bool new_state);

void
rssi_config_changed(rssi_config config);
